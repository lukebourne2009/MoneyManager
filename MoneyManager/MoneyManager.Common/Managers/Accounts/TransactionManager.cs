﻿using System.Collections.Generic;
using MoneyManager.Common.Transformers;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Models.Accounts;

namespace MoneyManager.Common.Managers.Accounts
{
    public class TransactionManager : IManager<TransactionModel>
    {
        private readonly IAccess<TransactionEntity> _transAccess;

        public TransactionManager(IAccess<TransactionEntity> transAccess)
        {
            _transAccess = transAccess;
        }
        public TransactionModel Create(TransactionModel obj)
        {
            var entity = _transAccess.Insert(Transform.ToTransactionEntity(obj));
            return Transform.ToTransactionModel(entity);
        }

        public IEnumerable<TransactionModel> Get()
        {
            throw new System.NotImplementedException();
        }

        public TransactionModel Update(TransactionModel obj)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}