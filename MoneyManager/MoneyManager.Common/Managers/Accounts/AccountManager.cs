﻿using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Models.Accounts;
using MoneyManager.Core.Transformers;

namespace MoneyManager.Common.Managers.Accounts
{
    public class AccountManager : BaseManager<AccountModel, AccountEntity>
    {
	    public AccountManager(IAccess<AccountEntity> transAccess,
		    ITransformer<AccountModel, AccountEntity> accountModelTransformer,
		    ITransformer<AccountEntity, AccountModel> accountEntityTransformer)
			:base(transAccess, accountEntityTransformer, accountModelTransformer)
	    {}
    }
}