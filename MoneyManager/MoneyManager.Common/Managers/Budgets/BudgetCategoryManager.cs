﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Mappers;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Transformers;

namespace MoneyManager.Common.Managers.Budgets
{
	public class BudgetCategoryManager : IBudgetCategoryManager
	{
		private readonly IAccess<BudgetCategoryEntity> _budgetCatAccess;
		private readonly IMapper<BudgetCategoryModel, BudgetCategoryEntity, DateTime> _catMapper;
		private readonly ITransformer<BudgetCategoryEntity, BudgetCategoryModel> _catTransformer;

		public BudgetCategoryManager(IAccess<BudgetCategoryEntity> budgetCatAccess,
			IMapper<BudgetCategoryModel, BudgetCategoryEntity, DateTime> catMapper,
			ITransformer<BudgetCategoryEntity, BudgetCategoryModel> catTransformer)
		{
			_budgetCatAccess = budgetCatAccess;
			_catMapper = catMapper;
			_catTransformer = catTransformer;
		}

		public BudgetCategoryModel Create(BudgetCategoryModel budgetCat)
		{
			if (budgetCat == null) throw new ArgumentNullException(nameof(budgetCat));

			var budgetEntity = _catTransformer.Transform(budgetCat);
			budgetEntity = _budgetCatAccess.Insert(budgetEntity);
			return _catMapper.Map(budgetEntity, DateTime.Now);
		}

		public void Delete(int id)
		{
			_budgetCatAccess.Delete(id);
		}

		public IEnumerable<BudgetCategoryModel> Get()
		{
			var entities = _budgetCatAccess.GetAll(x => x.Transactions.Select(y => y.Value));
			return _catMapper.Map(entities, DateTime.Now);
		}

		public BudgetCategoryModel Update(BudgetCategoryModel model)
		{
			if (model == null) throw new ArgumentNullException(nameof(model));

			var budgetEntity = _catTransformer.Transform(model);
			budgetEntity = _budgetCatAccess.Update(budgetEntity);
			return _catMapper.Map(budgetEntity, DateTime.Now);
		}

		public BudgetCategoryModel UpdateProperty(int id, Action<BudgetCategoryEntity> action)
		{
			if (action == null) throw new ArgumentNullException(nameof(action));

			var budgetEntity = _budgetCatAccess.Update(id, action);
			return _catMapper.Map(budgetEntity, DateTime.Now);
		}

		public BudgetCategoryModel UpdateCategoryOrder(int id, int newOrder)
		{
			if (newOrder < 0)
				throw new ArgumentException("The new Category sorting order cannot be less than 0", nameof(newOrder));

			return UpdateProperty(id, x => x.CategoryOrder = newOrder);
		}
	}
}
