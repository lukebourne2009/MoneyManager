﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MoneyManager.Common.Transformers;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Transformers;

namespace MoneyManager.Common.Managers.Budgets
{
    public class BudgetManager : BaseManager<BudgetModel, BudgetEntity>, IBudgetManager
	{
	    public BudgetManager(IAccess<BudgetEntity> access, 
			ITransformer<BudgetEntity, BudgetModel> entityTransformer,
		    ITransformer<BudgetModel, BudgetEntity> modelTransformer)
			: base(access, entityTransformer, modelTransformer)
	    {}

		public override BudgetModel Create(BudgetModel budget)
        {
	        if (budget == null) throw new ArgumentNullException(nameof(budget));

	        var budgetEntity = EntityTransformer.Transform(budget);
            budgetEntity.LastUsed = DateTime.Now;
            budgetEntity = Access.Insert(budgetEntity);

            return ModelTransformer.Transform(budgetEntity);
        }

        public IEnumerable<BudgetModel> Get(int? amount = null)
        {
            var budgetEntities = amount == null ? Access.GetAll() : Access.GetN((int)amount);
            var budgetModels = ModelTransformer.Transform(budgetEntities);
            return budgetModels;
        }

		/*x => x.Collection, x => x.Collection.Select(y => y.Collection)*/
		//ModelTransformer.Transform(budgetEntity)
		public BudgetModel GetBudget(int id)
        {
            var budget = Access.GetById(id,
		            x => x.Collection,
		            x => x.Collection.Select(y => y.Collection))
	            .Select(ModelTransformer.Transform)
	            .FirstOrDefault();

			if (budget == null) throw new ArgumentNullException(nameof(budget));

	        return budget;
        }

        public BudgetModel GetLastUsedBudget()
        {
            var budget = Access.GetHighest(x => x.LastUsed, 
				x => x.Collection, 
				x => x.Collection.Select(y => y.Collection))
				.Select(ModelTransformer.Transform)
				.FirstOrDefault();

	        if (budget == null) throw new ArgumentNullException(nameof(budget));

			return budget;
        }

        public int BudgetCount()
        {
            return Access.Count();
        }
	}
}