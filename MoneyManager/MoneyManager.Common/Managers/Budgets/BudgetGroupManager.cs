﻿using System;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Mappers;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Transformers;

namespace MoneyManager.Common.Managers.Budgets
{
    public class BudgetGroupManager : IBudgetGroupManager
    {
        private readonly IAccess<BudgetGroupEntity> _budgetGroupAccess;
        private readonly IMapper<BudgetGroupModel, BudgetGroupEntity, DateTime> _groupMapper;
	    private readonly ITransformer<BudgetGroupEntity, BudgetGroupModel> _budgetGroupTransformer;

	    public BudgetGroupManager(IAccess<BudgetGroupEntity> budgetGroupAccess,
	        IMapper<BudgetGroupModel, BudgetGroupEntity, DateTime> groupMapper,
			ITransformer<BudgetGroupEntity, BudgetGroupModel> budgetGroupTransformer)
        {
            _budgetGroupAccess = budgetGroupAccess;
            _groupMapper = groupMapper;
	        _budgetGroupTransformer = budgetGroupTransformer;
        }

        public BudgetGroupModel CreateBudgetGroup(BudgetGroupModel budgetGroup)
        {
	        if (budgetGroup == null) throw new ArgumentNullException(nameof(budgetGroup));

	        var budgetEntity = _budgetGroupTransformer.Transform(budgetGroup);
            budgetEntity = _budgetGroupAccess.Insert(budgetEntity);
            return _groupMapper.Map(budgetEntity, DateTime.Now);
        }

        public IEnumerable<BudgetGroupModel> GetBudgetGroups(DateTime? month = null)
        {
	        var budgetGroupModels = 
				_budgetGroupAccess.GetAll(x => x.Collection.Select(y => y.MonthlyCategories),
			        x => x.Collection.Select(y => y.Transactions))
		        .Select(x => _groupMapper.Map(x, month ?? DateTime.Now));

            return budgetGroupModels;
        }
    }
}