﻿using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Transformers;
using MoneyManager.Core.ViewModels;

namespace MoneyManager.Common.Managers
{
    public class MonthlyManager : BaseManager<MonthlyCategoryViewModel, MonthlyCategoryEntity>
    {
        public MonthlyManager(IAccess<MonthlyCategoryEntity> access, 
            ITransformer<MonthlyCategoryEntity, MonthlyCategoryViewModel> monthlyCategoryEntityTransformer,
            ITransformer<MonthlyCategoryViewModel, MonthlyCategoryEntity> monthlyCategoryModelTransformer)
			: base(access, monthlyCategoryEntityTransformer, monthlyCategoryModelTransformer)
        {}
    }
}