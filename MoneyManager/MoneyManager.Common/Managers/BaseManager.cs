﻿using System;
using System.Collections.Generic;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Transformers;

namespace MoneyManager.Common.Managers
{
	public class BaseManager<TModel, TEntity> : IManager<TModel>
	{
		protected readonly IAccess<TEntity> Access;
		protected readonly ITransformer<TEntity, TModel> EntityTransformer;
		protected readonly ITransformer<TModel, TEntity> ModelTransformer;

		public BaseManager(IAccess<TEntity> access,
			ITransformer<TEntity, TModel> entityTransformer,
			ITransformer<TModel, TEntity> modelTransformer)
		{
			Access = access;
			EntityTransformer = entityTransformer;
			ModelTransformer = modelTransformer;
		}

		public virtual TModel Create(TModel obj)
		{
			if (obj == null) throw new ArgumentNullException(nameof(obj));

			var entity = Access.Insert(EntityTransformer.Transform(obj));
			return ModelTransformer.Transform(entity);
		}

		public virtual IEnumerable<TModel> Get()
		{
			throw new NotImplementedException();
		}

		public virtual TModel Update(TModel obj)
		{
			throw new NotImplementedException();
		}

		public virtual TModel Update(int id, Action<TEntity> func)
		{
			var update = Access.Update(id, func);
			return ModelTransformer.Transform(update);
		}

		public virtual void Delete(int id)
		{
			Access.Delete(id);
		}


	}
}