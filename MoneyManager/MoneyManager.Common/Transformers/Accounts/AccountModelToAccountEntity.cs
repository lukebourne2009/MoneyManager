﻿using System;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Models.Accounts;

namespace MoneyManager.Common.Transformers.Accounts
{
    public class AccountModelToAccountEntity : BaseTransformer<AccountEntity,AccountModel>
    {
        public override AccountEntity Transform(AccountModel source)
        {
	        if (source == null) throw new ArgumentNullException(nameof(source));
	        return new AccountEntity
            {
                Id = source.Id,
                ParentId = source.BudgetId
            };
        }
    }
}