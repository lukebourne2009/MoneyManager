﻿using System;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Models.Accounts;

namespace MoneyManager.Common.Transformers.Accounts
{
    public class TransactionModelToTransactionEntity : BaseTransformer<TransactionEntity, TransactionModel>
    {
        public override TransactionEntity Transform(TransactionModel source)
        {
	        if (source == null) throw new ArgumentNullException(nameof(source));

	        return new TransactionEntity
            {
                Id = source.Id,
                ParentId = source.AccountId,
                Value = source.Value,
                CategoryId = source.CategoryId
            };
        }
    }
}