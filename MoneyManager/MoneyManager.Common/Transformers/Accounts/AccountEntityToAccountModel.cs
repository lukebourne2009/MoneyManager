﻿using System;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Models.Accounts;

namespace MoneyManager.Common.Transformers.Accounts
{
    public class AccountEntityToAccountModel : BaseTransformer<AccountModel,AccountEntity>
    {
        public override AccountModel Transform(AccountEntity source)
        {
	        if (source == null) throw new ArgumentNullException(nameof(source));

	        return new AccountModel
            {
                Id = source.Id,
                BudgetId = source.ParentId
            };
        }
    }
}