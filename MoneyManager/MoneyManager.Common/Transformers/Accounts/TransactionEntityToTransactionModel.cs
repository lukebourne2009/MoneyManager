﻿using System;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Models.Accounts;

namespace MoneyManager.Common.Transformers.Accounts
{
    public class TransactionEntityToTransactionModel : BaseTransformer<TransactionModel, TransactionEntity>
    {
        public override TransactionModel Transform(TransactionEntity source)
        {
	        if (source == null) throw new ArgumentNullException(nameof(source));

	        return new TransactionModel
            {
                Id = source.Id,
                AccountId = source.ParentId,
                Value = source.Value,
                CategoryId = source.CategoryId
            };
        }
    }
}