﻿using System;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Formatters;
using MoneyManager.Core.ViewModels;

namespace MoneyManager.Common.Transformers
{
    public class MonthlyCategoryEntityToMonthlyCategoryViewModel : BaseTransformer<MonthlyCategoryViewModel, MonthlyCategoryEntity>
    {
        private readonly IFormatter<string> _formatter;

        public MonthlyCategoryEntityToMonthlyCategoryViewModel(IFormatter<string> formatter)
        {
            _formatter = formatter;
        }

        public override MonthlyCategoryViewModel Transform(MonthlyCategoryEntity source)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));

            return new MonthlyCategoryViewModel
            {
                BudgetedValue = source.BudgetedValue,
                BudgetCategoryId = source.ParentId,
                Id = source.Id,
                Date = _formatter.Format(source.Date)
            };
        }
    }
}