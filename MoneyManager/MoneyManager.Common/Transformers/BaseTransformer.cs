﻿using MoneyManager.Core.Transformers;
using System.Collections.Generic;
using System.Linq;

namespace MoneyManager.Common.Transformers
{
    public abstract class BaseTransformer<TTarget, TSource> : ITransformer<TTarget, TSource>
    {
        public virtual IEnumerable<TTarget> Transform(IEnumerable<TSource> sources)
        {
            var targets = new List<TTarget>();
            targets.AddRange(sources.Select(Transform));
            return targets;
        }

        public abstract TTarget Transform(TSource source);
    }
}
