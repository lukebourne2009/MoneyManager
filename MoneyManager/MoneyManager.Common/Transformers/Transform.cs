﻿using System.Collections.Generic;
using MoneyManager.Common.Transformers.Accounts;
using MoneyManager.Common.Transformers.Budgets;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Models.Accounts;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Transformers;

namespace MoneyManager.Common.Transformers
{
    public static class Transform
    {
        private static readonly ITransformer<BudgetModel, BudgetEntity> _fromBudgetEntityToBudgetModel = new BudgetEntityToBudgetModel(null);
        private static readonly ITransformer<BudgetEntity, BudgetModel> _fromBudgetModelToBudgetEntity = new BudgetModelToBudgetEntity();
        private static readonly ITransformer<BudgetGroupEntity, BudgetGroupModel> _fromBudgetGroupModelToBudgetGroupEntity = new BudgetGroupModelToBudgetGroupEntity();
        private static readonly ITransformer<BudgetCategoryEntity, BudgetCategoryModel> _fromBudgetCategoryModelToBudgetCategoryEntity = new BudgetCategoryModelToBudgetCategoryEntity();
        private static readonly ITransformer<AccountModel, AccountEntity> _FromAccountEntityToAccountModel = new AccountEntityToAccountModel();
        private static readonly ITransformer<AccountEntity, AccountModel> _FromAccountModelToAccountEntity = new AccountModelToAccountEntity();
        private static readonly ITransformer<TransactionEntity, TransactionModel> _FromTransactionModelToTransactionEntity = new TransactionModelToTransactionEntity();
        private static readonly ITransformer<TransactionModel, TransactionEntity> _FromTransactionEntityToTransactionModel = new TransactionEntityToTransactionModel();

        // Budgets
        public static BudgetModel ToBudgetModel(BudgetEntity budgetEntity)
        {
            return _fromBudgetEntityToBudgetModel.Transform(budgetEntity);
        }
        public static IEnumerable<BudgetModel> ToBudgetModels(IEnumerable<BudgetEntity> budgetEntity)
        {
            return _fromBudgetEntityToBudgetModel.Transform(budgetEntity);
        }
        public static BudgetEntity ToBudgetEntity(BudgetModel budgetModel)
        {
            return _fromBudgetModelToBudgetEntity.Transform(budgetModel);
        }
        public static IEnumerable<BudgetEntity> ToBudgetEntities(IEnumerable<BudgetModel> budgetModel)
        {
            return _fromBudgetModelToBudgetEntity.Transform(budgetModel);
        }

        // Budget Groups
        public static BudgetGroupEntity ToBudgetGroupEntity(BudgetGroupModel budgetGroupModel)
        {
            return _fromBudgetGroupModelToBudgetGroupEntity.Transform(budgetGroupModel);
        }

        // Budget Categories
        public static BudgetCategoryEntity ToBudgetCategoryEntity(BudgetCategoryModel budgetCatModel)
        {
            return _fromBudgetCategoryModelToBudgetCategoryEntity.Transform(budgetCatModel);
        }

        // Accounts
        public static IEnumerable<AccountModel> ToAccountModels(IEnumerable<AccountEntity> groups)
        {
            return _FromAccountEntityToAccountModel.Transform(groups);
        }
        public static AccountEntity ToAccountEntity(AccountModel model)
        {
            return _FromAccountModelToAccountEntity.Transform(model);
        }
        public static AccountModel ToAccountModel(AccountEntity entity)
        {
            return _FromAccountEntityToAccountModel.Transform(entity);
        }

        // Transactions
        public static TransactionEntity ToTransactionEntity(TransactionModel model)
        {
            return _FromTransactionModelToTransactionEntity.Transform(model);
        }
        public static TransactionModel ToTransactionModel(TransactionEntity entity)
        {
            return _FromTransactionEntityToTransactionModel.Transform(entity);
        }
        public static IEnumerable<TransactionModel> ToTransactionModels(IEnumerable<TransactionEntity> entites)
        {
            return _FromTransactionEntityToTransactionModel.Transform(entites);
        }
    }
}
