﻿using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Models.Budgets;

namespace MoneyManager.Common.Transformers.Budgets
{
    public class BudgetGroupModelToBudgetGroupEntity : BaseTransformer<BudgetGroupEntity, BudgetGroupModel>
    {
        public override BudgetGroupEntity Transform(BudgetGroupModel source)
        {
            var budgetGroup = new BudgetGroupEntity
            {
                Id = source.Id,
                Name = source.Name,
				ParentId = source.BudgetId
            };

            return budgetGroup;
        }
    }
}
