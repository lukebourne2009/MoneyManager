﻿using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Models.Budgets;

namespace MoneyManager.Common.Transformers.Budgets
{
    public class BudgetModelToBudgetEntity : BaseTransformer<BudgetEntity, BudgetModel>
    {
        public override BudgetEntity Transform(BudgetModel source)
        {
            var budgetModel = new BudgetEntity()
            {
                Id = source.Id,
                Name = source.Name,
                User_Id = source.UserId,
                LastUsed = source.LastUsed
            };

            return budgetModel;
        }
    }
}