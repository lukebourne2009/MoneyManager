﻿using System;
using System.Collections.Generic;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Transformers;

namespace MoneyManager.Common.Transformers.Budgets
{
    public class BudgetEntityToBudgetModel : BaseTransformer<BudgetModel, BudgetEntity>
    {
        private readonly ITransformer<BudgetGroupModel, BudgetGroupEntity> _groupTrans;

        public BudgetEntityToBudgetModel(ITransformer<BudgetGroupModel, BudgetGroupEntity> groupTrans)
        {
            _groupTrans = groupTrans;
        }

        public override BudgetModel Transform(BudgetEntity source)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));

            var budgetModel = new BudgetModel
            {
                Id = source.Id,
                Name = source.Name,
                UserId = source.User_Id,
                LastUsed = source.LastUsed,
                BudgetGroups = source.Collection == null ? new List<BudgetGroupModel>() : _groupTrans.Transform(source.Collection)
            };

            return budgetModel;
        }
    }
}
