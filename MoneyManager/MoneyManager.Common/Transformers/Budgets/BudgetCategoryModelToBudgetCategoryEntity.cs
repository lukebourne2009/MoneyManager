﻿using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Models.Budgets;

namespace MoneyManager.Common.Transformers.Budgets
{
    public class BudgetCategoryModelToBudgetCategoryEntity : BaseTransformer<BudgetCategoryEntity, BudgetCategoryModel>
    {
        public override BudgetCategoryEntity Transform(BudgetCategoryModel source)
        {
            var category = new BudgetCategoryEntity
            {
                Id = source.Id,
                Name = source.Name,
                CategoryOrder = source.CategoryOrder,
                ParentId = source.BudgetGroupId
            };
            return category;
        }
    }
}