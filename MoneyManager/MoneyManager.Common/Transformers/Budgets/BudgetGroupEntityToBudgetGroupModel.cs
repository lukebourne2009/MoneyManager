﻿using System;
using System.Collections.Generic;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Mappers;
using MoneyManager.Core.Models.Budgets;

namespace MoneyManager.Common.Transformers.Budgets
{
    public class BudgetGroupEntityToBudgetGroupModel : BaseTransformer<BudgetGroupModel, BudgetGroupEntity>
    {
        private readonly IMapper<BudgetCategoryModel, BudgetCategoryEntity, DateTime> _mapper;

        public BudgetGroupEntityToBudgetGroupModel(IMapper<BudgetCategoryModel, BudgetCategoryEntity, DateTime> mapper)
        {
            _mapper = mapper;
        }

        public override BudgetGroupModel Transform(BudgetGroupEntity source)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));

            var model = new BudgetGroupModel
            {
                Id = source.Id,
                Name = source.Name,
                BudgetId = source.ParentId,
                BudgetCategories = source.Collection == null 
                ? new List<BudgetCategoryModel>() 
                : _mapper.Map(source.Collection, DateTime.Now)
            };
            return model;
        }
    }
}
