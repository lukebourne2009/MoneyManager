﻿using System;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Formatters;
using MoneyManager.Core.ViewModels;

namespace MoneyManager.Common.Transformers
{
    public class MonthlyCategoryViewModelToMonthyCategoryEntity : BaseTransformer<MonthlyCategoryEntity, MonthlyCategoryViewModel>
    {
        private readonly IFormatter<string> _formatter;

        public MonthlyCategoryViewModelToMonthyCategoryEntity(IFormatter<string> formatter)
        {
            _formatter = formatter;
        }

        public override MonthlyCategoryEntity Transform(MonthlyCategoryViewModel source)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));

            return new MonthlyCategoryEntity
            {
                BudgetedValue = source.BudgetedValue,
                Id = source.Id,
                ParentId = source.BudgetCategoryId,
                Date = _formatter.Format(source.Date)
            };
        }
    }
}