﻿using System.Threading.Tasks;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Requests;

namespace MoneyManager.Common.Requests.BudgetCategoryRequests
{
	public class CategoryCreateRequest : IRequest<BudgetCategoryModel, BudgetCategoryModel>
	{
		private readonly IFactory<IBudgetCategoryManager, int> _catManFactory;

		public CategoryCreateRequest(IFactory<IBudgetCategoryManager, int> catManFactory)
		{
			_catManFactory = catManFactory;
		}
		public async Task<BudgetCategoryModel> ExecuteAsync(BudgetCategoryModel model)
		{
			return _catManFactory.Make(model.BudgetGroupId).Create(model);
		}
	}
}