﻿using System.Threading.Tasks;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Models;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Requests;

namespace MoneyManager.Common.Requests
{
	public class BudgetCreateRequest : IRequest<BudgetModel, BudgetModel, Task<ApplicationUser>>
	{
		private readonly IFactory<IBudgetManager, UserEntity> _budgetFactory;

		public BudgetCreateRequest(IFactory<IBudgetManager, UserEntity> budgetFactory)
		{
			_budgetFactory = budgetFactory;
		}

		public async Task<BudgetModel> ExecuteAsync(BudgetModel budgetModel, Task<ApplicationUser> user)
		{
			var appUser = await user;
			var model = _budgetFactory.Make(appUser.User).Create(budgetModel);
			return model;
		}
	}
}