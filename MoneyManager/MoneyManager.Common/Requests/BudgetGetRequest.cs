﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Models;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Requests;

namespace MoneyManager.Common
{
	public class BudgetGetRequest : IRequest<BudgetModel, int?, Task<ApplicationUser>>
	{
		private readonly IFactory<IBudgetManager, UserEntity> _budgetFactory;

		public BudgetGetRequest(IFactory<IBudgetManager, UserEntity> budgetFactory)
		{
			_budgetFactory = budgetFactory;
		}

		public async Task<BudgetModel> ExecuteAsync(int? id, Task<ApplicationUser> user)
		{
			// Get Last Used Budget
			var appUser = await user;
			var budget = id == null
				? _budgetFactory.Make(appUser.User).GetLastUsedBudget()
				: _budgetFactory.Make(appUser.User).GetBudget((int)id);

			return budget;
		}
	}
}