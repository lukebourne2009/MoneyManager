﻿using System.Threading.Tasks;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Models;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Requests;

namespace MoneyManager.Common.Requests
{
	public class BudgetUpdateNameRequest : IRequest<BudgetModel, int, string, Task<ApplicationUser>>
	{
		private readonly IFactory<IBudgetManager, UserEntity> _budgetfactory;

		public BudgetUpdateNameRequest(IFactory<IBudgetManager, UserEntity> budgetfactory)
		{
			_budgetfactory = budgetfactory;
		}
		public async Task<BudgetModel> ExecuteAsync(int id, string name, Task<ApplicationUser> appUser)
		{
			var user = await appUser;
			var model = _budgetfactory.Make(user.User).Update(id, x => x.Name = name);
			return model;
		}
	}
}