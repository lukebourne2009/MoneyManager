﻿using System.Threading.Tasks;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Models;
using MoneyManager.Core.Requests;

namespace MoneyManager.Common.Requests
{
	public class BudgetDeleteRequest : IRequest<bool, int, Task<ApplicationUser>>
	{
		private readonly IFactory<IBudgetManager, UserEntity> _budgetFactory;

		public BudgetDeleteRequest(IFactory<IBudgetManager, UserEntity> budgetFactory)
		{
			_budgetFactory = budgetFactory;
		}

		public async Task<bool> ExecuteAsync(int id, Task<ApplicationUser> user)
		{
			_budgetFactory.Make((await user).User).Delete(id);

			return true;
		}
	}
}