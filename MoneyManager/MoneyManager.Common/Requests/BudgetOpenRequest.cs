﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Interfaces.Service;
using MoneyManager.Core.Models;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Requests;

namespace MoneyManager.Common.Requests
{
	public class BudgetOpenRequest : IRequest<IEnumerable<BudgetModel>, int, Task<ApplicationUser>>
	{
		private readonly IFactory<IBudgetManager, UserEntity> _budgetFactory;
		private readonly IService<IEnumerable<BudgetModel>, IEnumerable<BudgetModel>> _budgetSortingService;

		public BudgetOpenRequest(IFactory<IBudgetManager, UserEntity> budgetFactory, 
			IService<IEnumerable<BudgetModel>, IEnumerable<BudgetModel>> budgetSortingService)
		{
			_budgetFactory = budgetFactory;
			_budgetSortingService = budgetSortingService;
		}

		public async Task<IEnumerable<BudgetModel>> ExecuteAsync(int nBudgets, Task<ApplicationUser> user)
		{
			var appUser = await user;
			var budgetMan = _budgetFactory.Make(appUser.User);
			var budgets = budgetMan.Get(nBudgets);
			budgets = _budgetSortingService.Excecute(budgets);

			return budgets;
		}
	}
}