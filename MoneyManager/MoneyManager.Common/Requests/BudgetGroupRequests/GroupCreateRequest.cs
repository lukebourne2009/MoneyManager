﻿using System.Threading.Tasks;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Requests;

namespace MoneyManager.Common.Requests.BudgetGroupRequests
{
	public class GroupCreateRequest : IRequest<BudgetGroupModel, BudgetGroupModel>
	{
		private readonly IFactory<IBudgetGroupManager, int> _groupManFactory;

		public GroupCreateRequest(IFactory<IBudgetGroupManager, int> groupManFactory)
		{
			_groupManFactory = groupManFactory;
		}
		public async Task<BudgetGroupModel> ExecuteAsync(BudgetGroupModel model)
		{
			return _groupManFactory.Make(model.BudgetId).CreateBudgetGroup(model);
		}
	}
}