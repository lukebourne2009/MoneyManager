﻿using System;
using MoneyManager.Common.Formatter;
using MoneyManager.Common.Managers;
using MoneyManager.Common.Transformers;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.ViewModels;
using MoneyManager.Data.Database;
using MoneyManager.Data.Repositories;

namespace MoneyManager.Common.Factories
{
    public class MonthlyManagerFactory : IFactory<IManager<MonthlyCategoryViewModel>, int>
    {
        private readonly MoneyManagerDbContext _repo;

        public MonthlyManagerFactory(MoneyManagerDbContext repo)
        {
            if (repo == null) throw new ArgumentNullException(nameof(repo));

            _repo = repo;
        }

        public IManager<MonthlyCategoryViewModel> Make(int input)
        {
            return new MonthlyManager(new MonthlyCategoryAccess(input, _repo),
                                      new MonthlyCategoryViewModelToMonthyCategoryEntity(new ShortMonthAndYearFormatter()),
                                      new MonthlyCategoryEntityToMonthlyCategoryViewModel(new LongMonthAndYearFormatter()));
        }
    }
}