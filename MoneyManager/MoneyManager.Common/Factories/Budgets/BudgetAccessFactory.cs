﻿using System;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Data.Database;
using MoneyManager.Data.Repositories;

namespace MoneyManager.Common.Factories.Budgets
{
    public class BudgetAccessFactory : IFactory<IAccess<BudgetEntity>,UserEntity>
    {
        private readonly MoneyManagerDbContext _repo;

        public BudgetAccessFactory(MoneyManagerDbContext repo)
        {
	        _repo = repo;
        }

        public IAccess<BudgetEntity> Make(UserEntity input)
        {
	        if (input == null) throw new ArgumentNullException(nameof(input));

	        return new BudgetAccess(input, _repo);
        }
    }
}
