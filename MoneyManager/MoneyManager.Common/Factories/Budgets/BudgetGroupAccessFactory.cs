﻿using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Data.Database;
using MoneyManager.Data.Repositories;

namespace MoneyManager.Common.Factories.Budgets
{
    public class BudgetGroupAccessFactory : IFactory<IAccess<BudgetGroupEntity>, int>
    {
        private MoneyManagerDbContext _repo;

        public BudgetGroupAccessFactory()
        {
            
        }
        public BudgetGroupAccessFactory(MoneyManagerDbContext repo)
        {
	        _repo = repo;
        }

        public IAccess<BudgetGroupEntity> Make(int input)
        {
            return new BudgetGroupAccess(input, _repo);
        }
    }
}