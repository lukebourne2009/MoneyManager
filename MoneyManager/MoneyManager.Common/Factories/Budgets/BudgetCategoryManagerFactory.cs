﻿using System;
using MoneyManager.Common.Managers.Budgets;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Mappers;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Transformers;

namespace MoneyManager.Common.Factories.Budgets
{
	public class BudgetCategoryManagerFactory : IFactory<BudgetCategoryManager, int>
	{
		private readonly IFactory<IAccess<BudgetCategoryEntity>, int> _bceFactory;
		private readonly IMapper<BudgetCategoryModel, BudgetCategoryEntity, DateTime> _catMapper;
		private readonly ITransformer<BudgetCategoryEntity, BudgetCategoryModel> _catTransformer;

		public BudgetCategoryManagerFactory(IFactory<IAccess<BudgetCategoryEntity>, int> bceFactory,
			IMapper<BudgetCategoryModel, BudgetCategoryEntity, DateTime> catMapper,
			ITransformer<BudgetCategoryEntity, BudgetCategoryModel> catTransformer)
		{
			_bceFactory = bceFactory;
			_catMapper = catMapper;
			_catTransformer = catTransformer;
		}

		public BudgetCategoryManager Make(int input)
		{
			return new BudgetCategoryManager(_bceFactory.Make(input), _catMapper, _catTransformer);
		}
	}
}