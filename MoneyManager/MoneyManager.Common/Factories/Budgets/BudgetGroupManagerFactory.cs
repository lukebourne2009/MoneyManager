﻿using System;
using MoneyManager.Common.Managers.Budgets;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Mappers;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Transformers;

namespace MoneyManager.Common.Factories.Budgets
{
    public class BudgetGroupManagerFactory : IFactory<IBudgetGroupManager, int>
    {
        private readonly IFactory<IAccess<BudgetGroupEntity>, int> _bgeFactory;
        private readonly IMapper<BudgetGroupModel, BudgetGroupEntity, DateTime> _groupMapper;
	    private readonly ITransformer<BudgetGroupEntity, BudgetGroupModel> _budgetGroupTransformer;

	    public BudgetGroupManagerFactory(IFactory<IAccess<BudgetGroupEntity>, int> bgeFactory,
	        IMapper<BudgetGroupModel, BudgetGroupEntity, DateTime> groupMapper,
	        ITransformer<BudgetGroupEntity, BudgetGroupModel> budgetGroupTransformer)
        {
            _bgeFactory = bgeFactory;
            _groupMapper = groupMapper;
	        _budgetGroupTransformer = budgetGroupTransformer;
        }

        public IBudgetGroupManager Make(int input)
        {
            var access = _bgeFactory.Make(input);
            return new BudgetGroupManager(access, _groupMapper, _budgetGroupTransformer);
        }
    }
}