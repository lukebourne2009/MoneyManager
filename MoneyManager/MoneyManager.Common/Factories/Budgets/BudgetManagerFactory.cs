﻿using System;
using MoneyManager.Common.Managers.Budgets;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Transformers;

namespace MoneyManager.Common.Factories.Budgets
{
    public class BudgetManagerFactory : IFactory<IBudgetManager, UserEntity>
    {
        private readonly IFactory<IAccess<BudgetEntity>, UserEntity> _accessFactory;
	    private readonly ITransformer<BudgetModel, BudgetEntity> _modelTransformer;
	    private readonly ITransformer<BudgetEntity, BudgetModel> _entityTransformer;

	    public BudgetManagerFactory(IFactory<IAccess<BudgetEntity>, UserEntity> accessFactory, 
			ITransformer<BudgetModel, BudgetEntity> modelTransformer,
			ITransformer<BudgetEntity, BudgetModel> entityTransformer)
	    {
		    _accessFactory = accessFactory;
		    _modelTransformer = modelTransformer;
		    _entityTransformer = entityTransformer;
	    }

        public IBudgetManager Make(UserEntity input)
        {
            if(input == null)
                throw new ArgumentNullException(nameof(input));

            var access = _accessFactory.Make(input);
            return new BudgetManager(access, _entityTransformer, _modelTransformer);
        }
    }
}
