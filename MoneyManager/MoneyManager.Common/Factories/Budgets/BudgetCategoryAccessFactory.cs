﻿using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Data.Database;
using MoneyManager.Data.Repositories;

namespace MoneyManager.Common.Factories.Budgets
{
    public class BudgetCategoryAccessFactory : IFactory<IAccess<BudgetCategoryEntity>, int>
    {
        private MoneyManagerDbContext _repo;

       public BudgetCategoryAccessFactory(MoneyManagerDbContext repo)
        {
            _repo = repo;
        }

        public IAccess<BudgetCategoryEntity> Make(int input)
        {
            return new BudgetCategoryAccess(input, _repo);
        }
    }
}