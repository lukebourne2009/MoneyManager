﻿using System;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Models;
using MoneyManager.Data.Database;
using MoneyManager.Data.Repositories;

namespace MoneyManager.Common.Factories
{
    public class UserAccessFactory : IFactory<ISingleAccess<UserEntity>,ApplicationUser>
    {
        private MoneyManagerDbContext _repo;

        public UserAccessFactory()
        {}

        public UserAccessFactory(MoneyManagerDbContext repo)
        {
	        _repo = repo;
        }

        public ISingleAccess<UserEntity> Make(ApplicationUser input)
        {
	        if (input == null) throw new ArgumentNullException(nameof(input));

            return new UserAccess(input, _repo);
        }
    }
}