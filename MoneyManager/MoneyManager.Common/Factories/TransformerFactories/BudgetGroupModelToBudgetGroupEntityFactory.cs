﻿using System;
using MoneyManager.Common.Mappers;
using MoneyManager.Common.Services.Budgets;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Services;

namespace MoneyManager.Common.Factories.TransformerFactories
{
    public class BudgetGroupEntityMapperFactory : IFactory<BudgetGroupEntityMapper, DateTime>
    {
        private readonly ITransactionSummingService _transactionCountingService;

        public BudgetGroupEntityMapperFactory(
            ITransactionSummingService transactionCountingService)
        {
            _transactionCountingService = transactionCountingService;
        }

        public BudgetGroupEntityMapper Make(DateTime monthRequested)
        {
            return new BudgetGroupEntityMapper(new BudgetCategoryEntityMapper(_transactionCountingService, new MonthlyCategoryChooserService()));
        }
    }
}