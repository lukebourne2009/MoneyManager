﻿using System;
using MoneyManager.Core.Formatters;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.WebPages;

namespace MoneyManager.Common.Formatter
{
    public class ShortMonthAndYearFormatter : IFormatter<string>
    {
        private static readonly IDictionary<string, string> Dates = new Dictionary<string, string>
        {
            {  "January"  ,"01-"  },
            {  "February" ,"02-"    },
            {  "March"    ,"03-"   },
            {  "April"    ,"04-"   },
            {  "May"      ,"05-"  },
            {  "June"     ,"06-" },
            {  "July"     ,"07-"  },
            {  "August"   ,"08-"   },
            {  "September","09-"      },
            {  "October"  ,"10-"   },
            {  "November" ,"11-"   },
            {  "December" ,"12-"    },
        };

        public string Format(string obj)
        {
            var match = Regex.Match(obj, @"([a-zA-Z]+) (\d{4})");
            if(obj.IsEmpty() || match.Value != obj)
                throw new FormatException();

            return Dates[match.Groups[1].Value] + match.Groups[2].Value;
        }
    }
}