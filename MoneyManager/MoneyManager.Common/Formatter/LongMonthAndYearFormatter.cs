﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.WebPages;
using MoneyManager.Core.Formatters;

namespace MoneyManager.Common.Formatter
{
    public class LongMonthAndYearFormatter : IFormatter<string>
    {
        private static readonly IDictionary<string, string> Dates = new Dictionary<string, string>
        {
            { "01", "January "},
            { "02", "February "},
            { "03", "March "},
            { "04", "April "},
            { "05", "May "},
            { "06", "June "},
            { "07", "July "},
            { "08", "August "},
            { "09", "September "},
            { "10", "October "},
            { "11", "November "},
            { "12", "December "},
        };

        public string Format(string obj)
        {
            var match = Regex.Match(obj, @"((0[1-9])|(1[0-2]))-(\d{4})");
            if (obj.IsEmpty() || match.Value != obj)
                throw new FormatException();

            var month = match.Groups[1].Value;
            return Dates[month] + match.Groups[4].Value;
        }
    }
}