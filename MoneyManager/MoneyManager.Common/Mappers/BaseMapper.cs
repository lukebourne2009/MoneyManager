﻿using System.Collections.Generic;
using System.Linq;
using MoneyManager.Core.Mappers;

namespace MoneyManager.Common.Mappers
{
    public abstract class BaseMapper<TTarget, TSource, TArg> : IMapper<TTarget, TSource, TArg>
    {
        public abstract TTarget Map(TSource source, TArg arg);

        public virtual IEnumerable<TTarget> Map(IEnumerable<TSource> sources, TArg arg)
        {
            var targets = new List<TTarget>();
            targets.AddRange(sources.Select(x => Map(x, arg)));
            return targets;
        }
    }
}