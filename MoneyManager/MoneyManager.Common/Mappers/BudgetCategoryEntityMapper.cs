﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Service;
using MoneyManager.Core.Models.Budgets;

namespace MoneyManager.Common.Mappers
{
    public class BudgetCategoryEntityMapper : BaseMapper<BudgetCategoryModel, BudgetCategoryEntity, DateTime>
    {
        private readonly IService<int, IEnumerable<TransactionEntity>> _transactionCountingService;
        private readonly IService<Func<MonthlyCategoryEntity, bool>, DateTime> _monthlyCatChooserService;

        public BudgetCategoryEntityMapper(IService<int, IEnumerable<TransactionEntity>> transactionCountingService,
            IService<Func<MonthlyCategoryEntity, bool>, DateTime> monthlyCatChooserService)
        {
            _transactionCountingService = transactionCountingService;
            _monthlyCatChooserService = monthlyCatChooserService;
        }

        public override BudgetCategoryModel Map(BudgetCategoryEntity source, DateTime dateToPick)
        {
	        if (source == null) throw new ArgumentNullException(nameof(source));

	        var model = new BudgetCategoryModel
            {
                Id = source.Id,
                Name = source.Name,
                BudgetGroupId = source.ParentId,
                BudgetedValue = source.MonthlyCategories?.FirstOrDefault(_monthlyCatChooserService.Excecute(dateToPick))?.BudgetedValue ?? 0,
                CategoryOrder = source.CategoryOrder,
                ActivityValue = source.Transactions == null ? 0 : _transactionCountingService.Excecute(source.Transactions)
            };
            model.Available = model.BudgetedValue + model.ActivityValue;
            model.PercentageAvailable = CalculatePercentageSpent(model);
            return model;
        }

        private static int CalculatePercentageSpent(BudgetCategoryModel model)
        {
            if (model.BudgetedValue == 0)
                return 0;
            return Math.Max(0, Math.Min(100, (model.Available * 100) / model.BudgetedValue));
        }
    }
}