﻿using System;
using System.Collections.Generic;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Mappers;
using MoneyManager.Core.Models.Budgets;

namespace MoneyManager.Common.Mappers
{
    public class BudgetGroupEntityMapper : BaseMapper<BudgetGroupModel, BudgetGroupEntity, DateTime>, IBudgetGroupEntityMapper
    {
        private readonly IMapper<BudgetCategoryModel, BudgetCategoryEntity, DateTime> _mapper;

        public BudgetGroupEntityMapper(IMapper<BudgetCategoryModel, BudgetCategoryEntity, DateTime> mapper)
        {
            _mapper = mapper;
        }
        public override BudgetGroupModel Map(BudgetGroupEntity source, DateTime date)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));

            var model = new BudgetGroupModel
            {
                Id = source.Id,
                Name = source.Name,
                BudgetId = source.ParentId,
                BudgetCategories = source.Collection == null
                    ? new List<BudgetCategoryModel>()
                    : _mapper.Map(source.Collection, date)
            };
            return model;
        }
    }
}