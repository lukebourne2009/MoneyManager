﻿using System;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Service;

namespace MoneyManager.Common.Services.Budgets
{
    public class MonthlyCategoryChooserService : IService<Func<MonthlyCategoryEntity, bool>, DateTime>
    {
        public Func<MonthlyCategoryEntity, bool> Excecute(DateTime date)
        {
            string month = date.Month < 10 ? "0" + date.Month : date.Month.ToString();
            return x => x.Date == $"{month}-{date.Year.ToString()}";
        }
    }
}