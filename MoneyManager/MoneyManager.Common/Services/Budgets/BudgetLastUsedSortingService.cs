﻿using System.Collections.Generic;
using System.Linq;
using MoneyManager.Core.Interfaces.Service;
using MoneyManager.Core.Models.Budgets;

namespace MoneyManager.Common.Services.Budgets
{
    public class BudgetLastUsedSortingService : IService<IEnumerable<BudgetModel>, IEnumerable<BudgetModel>>
	{
        public IEnumerable<BudgetModel> Excecute(IEnumerable<BudgetModel> budgets)
        {
            return budgets.OrderByDescending(x => x.LastUsed);
        }
    }
}
