﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Interfaces.Service;

namespace MoneyManager.Common.Services.Budgets
{
    public class TransactionSummingService : IService<int, IEnumerable<TransactionEntity>>
	{
        public int Excecute(IEnumerable<TransactionEntity> transactions)
        {
            if (transactions == null) throw new ArgumentNullException(nameof(transactions));

            return transactions.Sum(x => x.Value);
        }
    }
}