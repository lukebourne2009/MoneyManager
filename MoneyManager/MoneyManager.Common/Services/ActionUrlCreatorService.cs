﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using MoneyManager.Core.Interfaces.Service;

namespace MoneyManager.Common.Services
{
    public class ActionUrlCreatorService : IActionUrlCreatorService
    {
        private readonly RequestContext _context;

        public ActionUrlCreatorService(RequestContext context)
        {
            _context = context;
        }
        public string Excecute(string in1, string in2, IDictionary<string, object> routeValues)
        {
            var request = _context.HttpContext.Request;
            return $"{request.Url.Scheme}://{request.Url.Authority}{request.ApplicationPath.TrimEnd('/')}" +
                   $"{new UrlHelper(_context).Action(in1, in2, new RouteValueDictionary(routeValues))}";
        }
        public string Excecute(string in1, string in2)
        {
            var request = _context.HttpContext.Request;
            return $"{request.Url.Scheme}://{request.Url.Authority}{request.ApplicationPath.TrimEnd('/')}" +
                   $"{new UrlHelper(_context).Action(in1, in2)}";
        }
    }
}