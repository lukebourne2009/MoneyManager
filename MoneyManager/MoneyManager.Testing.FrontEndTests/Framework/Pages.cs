﻿using AutomatedTestFramework.Framework.Common;
using System.Collections.Generic;
using System.Linq;
using AutomatedTestFramework.Framework;
using AutomatedTestFramework.Framework.Core;
using MoneyManager.Testing.FrontEndTests.Framework.SitePages;

namespace MoneyManager.Testing.FrontEndTests.Framework
{
    public class Pages
    {
        public static BudgetPage BudgetPage => new BudgetPage();
        public static LoginPage LoginPage => new LoginPage();
        public static OpenBudgetPage OpenBudgetPage => new OpenBudgetPage();

        public static IDictionary<string, IElementQuery> Elements { get; } = GenerateElements();
        private static Dictionary<string, IElementQuery> GenerateElements()
        {
            var dictionary =  new Dictionary<string, IElementQuery>
            {
                { "Edit Budget Button", new ElementQuery("#budget-edit", DriverContext.Driver) },
                { "Delete Budget Button", new ElementQuery("#budget-delete", DriverContext.Driver) },
                { "Body", new ElementQuery("body", DriverContext.Driver) }
            }.ToList();

            dictionary.AddRange(EditBudgetForm());
            dictionary.AddRange(BudgetTable());
            dictionary.AddRange(DeleteBudgetForm());
            dictionary.AddRange(SideMenu());
            dictionary.AddRange(CreateBudgetForm());
            dictionary.AddRange(CreateGroupForm());

            return dictionary.ToDictionary(x => x.Key, x => x.Value);
        }
        private static List<KeyValuePair<string, IElementQuery>> EditBudgetForm()
        {
            var form = new ElementQuery("#edit-budget-menu",DriverContext.Driver);
            return new Dictionary<string, IElementQuery>
            {
                {"Edit Budget Menu", form},
                {"Edit Budget Form Cancel", new ElementQuery(form, " .cancel")},
                {"Edit Budget Form Submit", new ElementQuery(form, " .submit")},
                {"Edit Budget Name Text Field", new ElementQuery(form, " input#budget-name-text-input")},
                {"Edit Budget Form Validation Error", new ElementQuery(form, " .validation-error") }
            }.ToList();
        }
        private static List<KeyValuePair<string, IElementQuery>> DeleteBudgetForm()
        {
            var form = new ElementQuery("#delete-budget-form", DriverContext.Driver);
            return new Dictionary<string, IElementQuery>
            {
                {"Delete Budget Form", form},
                {"Delete Budget Form Cancel", new ElementQuery(form, " .cancel")},
                {"Delete Budget Form Submit", new ElementQuery(form, " .submit")},
                {"Delete Budget Form Confirmation Message", new ElementQuery(form, " .message") }
            }.ToList();
        }
        private static List<KeyValuePair<string, IElementQuery>> CreateBudgetForm()
        {
            var form = new ElementQuery("#create-budget-form", DriverContext.Driver);
            return new Dictionary<string, IElementQuery>
            {
                {"Create Budget Form", form},
                {"Create Budget Form Cancel", new ElementQuery(form, " .cancel")},
                {"Create Budget Form Submit", new ElementQuery(form, " .submit")},
                {"Create Budget Form Header", new ElementQuery(form, " .modal-header h5")},
                {"Create Budget Form Name Text Input", new ElementQuery(form, " input[name='Name']")},
                {"Create Budget Form Validation Error", new ElementQuery(form, " .validation-error") }
            }.ToList();
        }
        private static List<KeyValuePair<string, IElementQuery>> CreateGroupForm()
        {
            var form = new ElementQuery("#create-group-form", DriverContext.Driver);
            return new Dictionary<string, IElementQuery>
            {
                {"Create Group Form", form},
                {"Create Group Form Cancel", new ElementQuery(form, " .cancel")},
                {"Create Group Form Submit", new ElementQuery(form, " .submit")},
                {"Create Group Form Header", new ElementQuery(form, " .modal-header h5")},
                {"Create Group Form Name Text Input", new ElementQuery(form, " input[name='Name']")},
                {"Create Group Form Validation Error", new ElementQuery(form, " .validation-error") }
            }.ToList();
        }
        private static List<KeyValuePair<string, IElementQuery>> CreateCategoryForm()
        {
            var form = new ElementQuery("#create-category-form", DriverContext.Driver);
            return new Dictionary<string, IElementQuery>
            {
                {"Create Category Form", form},
                {"Create Category Form Cancel", new ElementQuery(form, " .cancel")},
                {"Create Category Form Submit", new ElementQuery(form, " .submit")},
                {"Create Category Form Header", new ElementQuery(form, " .modal-header h5")},
                {"Create Category Form Name Text Input", new ElementQuery(form, " input[name='Name']")},
                {"Create Category Form Validation Error", new ElementQuery(form, " .validation-error") }
            }.ToList();
        }
        private static List<KeyValuePair<string, IElementQuery>> BudgetTable()
        {
            return new Dictionary<string, IElementQuery>
            {
                {"Budget Table", new ElementQuery("#budget-table", DriverContext.Driver)},
                {"Budget Name", new ElementQuery("#budget-name", DriverContext.Driver)},
                {"Create Group Button", new ElementQuery("#group-create", DriverContext.Driver)},
                {"Create Category Button", new ElementQuery(".category-create", DriverContext.Driver)},
                {"Group Row Name", new ElementQuery(".budget-group-row .budget-group-name", DriverContext.Driver)},
                {"Group Row", new ElementQuery(".budget-group-row", DriverContext.Driver)}
            }.ToList();
        }
        private static List<KeyValuePair<string, IElementQuery>> SideMenu()
        {
            var sideMenu = new ElementQuery("#sidemenu", DriverContext.Driver);
            return new Dictionary<string, IElementQuery>
            {
                { "Side Menu", sideMenu},
                { "Brand Name", new ElementQuery(sideMenu, " #brand-name") },
                { "Accounts Tab", new ElementQuery(sideMenu, " #accounts-tab") },
                { "Accounts Tab Icon", new ElementQuery(sideMenu, " #accounts-tab .fa-credit-card") },
                { "Budgets Menu Tab", new ElementQuery(sideMenu, " #budgets-options-menu-tab") },
                { "Budgets Menu Tab Icon", new ElementQuery(sideMenu, " #budgets-options-menu-tab .fa-money") },
                { "Create Budget Button", new ElementQuery(sideMenu, " #budget-create")}
            }.ToList();
        }
    }
}
