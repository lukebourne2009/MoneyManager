﻿using AutomatedTestFramework.Framework.Common;

namespace MoneyManager.Testing.FrontEndTests.Framework.SitePages
{
    public class LoginPage : Page
    {
        public LoginPage() 
            : base("/Account/Login")
        {
        }

        public void Login(string user, string password)
        {
            UserNameField.EnterText(user);
            PasswordField.EnterText(password);
            LoginButton.Click();
        }

        public ElementQuery UserNameField => "#loginForm input#Email";
        public ElementQuery PasswordField => "#loginForm input#Password";
        public ElementQuery LoginButton => "#loginForm [type='submit']";
    }
}
