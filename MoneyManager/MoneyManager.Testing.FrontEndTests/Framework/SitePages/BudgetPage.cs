﻿using AutomatedTestFramework.Framework.Common;
using MoneyManager.Testing.FrontEndTests.Framework.Elements;

namespace MoneyManager.Testing.FrontEndTests.Framework.SitePages
{
    public class BudgetPage : Page
    {
        public BudgetPage() 
            : base("/Budget/Index")
        {}

        public ElementQuery BudgetName => ".budget-name";
        public BudgetOptionsMenu BudgetOptionsMenu => ".budget-options-menu";
        public CreateBudgetForm CreateBudgetForm => ".budget-create-form";
        public BudgetToolbar BudgetToolbar => ".budget-toolbar";
        public BudgetGroupRow BudgetGroupRow => ".budget-group-row";
        public CreateGroupForm CreateGroupForm => "#group-create-form";
        public BudgetCategoryRow BudgetCategoryRow => ".budget-category-row";
        public BudgetTable BudgetTable => "#budget-table";
        public ElementQuery BudgetHeadingRow => "#budget-heading-row";
    }
}