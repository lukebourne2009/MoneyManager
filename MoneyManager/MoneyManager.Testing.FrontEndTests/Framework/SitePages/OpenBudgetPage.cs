﻿using AutomatedTestFramework.Framework.Common;
using MoneyManager.Testing.FrontEndTests.Framework.Elements;

namespace MoneyManager.Testing.FrontEndTests.Framework.SitePages
{
    public class OpenBudgetPage : Page
    {
        public OpenBudgetPage() 
            : base("/Budget/Open")
        {}

        public ElementQuery Title => "#page-title";
        public ElementQuery CreateBudget => "#create-budget";
        public ElementQuery ClickableIcons => ".icon";
        public OpenBudgetIcon OpenBudgetIcons => ".open-budget-icon";
        public ElementQuery DropDownArrow => "#budget-load-bar";
        public CreateBudgetForm CreateBudgetForm => ".budget-create-form";
    }
}
