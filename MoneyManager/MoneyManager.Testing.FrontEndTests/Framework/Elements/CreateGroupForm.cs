﻿using AutomatedTestFramework.Framework.Common;
using AutomatedTestFramework.Framework.Core.ElementGroups;
using OpenQA.Selenium;

namespace MoneyManager.Testing.FrontEndTests.Framework.Elements
{
    public class CreateGroupForm : ElementQuery, IForm
    {
        public CreateGroupForm(string selector, IWebDriver driver) : base(selector, driver)
        {}
        public CreateGroupForm(IWebElement webElement, string selector) : base(webElement, selector)
        {}
        public static implicit operator CreateGroupForm(string selector)
        {
            CreateGroupForm element = new CreateGroupForm(selector,null);
            return element;
        }

        public ElementQuery PositiveButton => Selector + " .submit";
        public ElementQuery NegativeButton => Selector + " .cancel";
        public ElementQuery GroupNameField => Selector + " #group-name-field";
        public ElementQuery GroupNameFieldError => Selector + " [data-valmsg-for='Name'";
    }
}