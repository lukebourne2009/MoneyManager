﻿using AutomatedTestFramework.Framework.Common;
using OpenQA.Selenium;

namespace MoneyManager.Testing.FrontEndTests.Framework.Elements
{
    public class BudgetTable : ElementQuery
    {
        public BudgetTable(string selector) : base(selector)
        {}
        public BudgetTable(string selector, IWebDriver driver) : base(selector, driver)
        {}
        public BudgetTable(IWebElement webElement, string selector) : base(webElement, selector)
        {}
        public static implicit operator BudgetTable(string selector)
        {
            BudgetTable element = new BudgetTable(selector, null);
            return element;
        }
        public static string Checkbox => " .table-checkbox input";
        public ElementQuery Headings => Selector + " .budget-table-heading";
    }
}