﻿using AutomatedTestFramework.Framework.Common;
using OpenQA.Selenium;

namespace MoneyManager.Testing.FrontEndTests.Framework.Elements
{
    public class BudgetCategoryRow : ElementQuery
    {
        public BudgetCategoryRow(string selector) : base(selector)
        {}
        public BudgetCategoryRow(string selector, IWebDriver driver) : base(selector, driver)
        {}
        public BudgetCategoryRow(IWebElement webElement, string selector) : base(webElement, selector)
        {}
        public static implicit operator BudgetCategoryRow(string selector)
        {
            BudgetCategoryRow element = new BudgetCategoryRow(selector);
            return element;
        }

        public static string BudgetedValueColumn => " .column-budgeted";
    }
}