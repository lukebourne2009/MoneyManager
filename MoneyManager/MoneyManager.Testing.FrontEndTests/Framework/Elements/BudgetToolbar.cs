﻿using AutomatedTestFramework.Framework.Common;
using OpenQA.Selenium;

namespace MoneyManager.Testing.FrontEndTests.Framework.Elements
{
    public class BudgetToolbar : ElementQuery
    {
        public BudgetToolbar(string selector, IWebDriver driver) : base(selector, driver)
        {
        }

        public static implicit operator BudgetToolbar(string selector)
        {
            BudgetToolbar element = new BudgetToolbar(selector,null);
            return element;
        }

        public ElementQuery AddGroupButton => "#group-create";
    }
}