﻿using AutomatedTestFramework.Framework.Common;
using OpenQA.Selenium;

namespace MoneyManager.Testing.FrontEndTests.Framework.Elements
{
    public class BudgetGroupRow : ElementQuery
    {
        private BudgetGroupRow(string selector) : base(selector)
        {}

        public BudgetGroupRow(string selector, IWebDriver driver) : base(selector, driver)
        {}

        public static implicit operator BudgetGroupRow(string selector)
        {
            BudgetGroupRow element = new BudgetGroupRow(selector);
            return element;
        }

        public ElementQuery Columns => Selector + " td";
        public ElementQuery BudgetName => Selector + " .budget-group-name";

        public static string Dropper => ".budget-group-dropper";
    }
}