﻿namespace MoneyManager.Testing.FrontEndTests.Framework.Elements.Enums
{
    public enum Direction
    {
        Right,
        Down
    }
}