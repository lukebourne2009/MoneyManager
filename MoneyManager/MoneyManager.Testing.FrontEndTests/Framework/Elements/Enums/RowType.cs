﻿namespace MoneyManager.Testing.FrontEndTests.Framework.Elements.Enums
{
    public enum RowType
    {
        Category,
        All,
        Group,
        Heading
    }
}