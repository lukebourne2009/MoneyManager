﻿using AutomatedTestFramework.Framework.Common;
using OpenQA.Selenium;

namespace MoneyManager.Testing.FrontEndTests.Framework.Elements
{
    public class OpenBudgetIcon : ElementQuery
    {
        public OpenBudgetIcon(string selector) : base(selector)
        {
        }

        public OpenBudgetIcon(string selector, IWebDriver driver) : base(selector, driver)
        {
        }

        public static implicit operator OpenBudgetIcon(string selector)
        {
            OpenBudgetIcon element = new OpenBudgetIcon(selector);
            return element;
        }

        public ElementQuery DeleteBudget => Selector + " .delete-budget";
    }
}
