﻿using AutomatedTestFramework.Framework.Common;
using AutomatedTestFramework.Framework.Core.ElementGroups;

namespace MoneyManager.Testing.FrontEndTests.Framework.Elements
{
    public class CreateBudgetForm : ElementQuery, IForm
    {
        public CreateBudgetForm(string selector) : base(selector)
        {
        }

        public static implicit operator CreateBudgetForm(string selector)
        {
            CreateBudgetForm element = new CreateBudgetForm(selector);
            return element;
        }

        public ElementQuery Heading => Selector + " .form-heading";
        public ElementQuery BudgetNameFieldInput => Selector + " .budget-name-field input";
        public ElementQuery BudgetNameField => Selector + " .budget-name-field";
        public ElementQuery BudgetNameFieldError => Selector + " .field-validation-error";
        public ElementQuery PositiveButton => Selector + " .submit";
        public ElementQuery NegativeButton => Selector + " .cancel";

    }
}