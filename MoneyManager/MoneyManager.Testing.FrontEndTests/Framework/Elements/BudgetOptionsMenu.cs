﻿using AutomatedTestFramework.Framework.Common;
using OpenQA.Selenium;

namespace MoneyManager.Testing.FrontEndTests.Framework.Elements
{
    public class BudgetOptionsMenu : ElementQuery
    {
        private BudgetOptionsMenu(string selector) : base(selector)
        {
        }

        public BudgetOptionsMenu(string selector, IWebDriver driver) : base(selector, driver)
        {
        }

        public static implicit operator BudgetOptionsMenu(string selector)
        {
            BudgetOptionsMenu element = new BudgetOptionsMenu(selector);
            return element;
        }

        public ElementQuery CreateBudget => Selector + " .budget-create";
        public ElementQuery OpenBudget => Selector + " .budget-open";
    }
}