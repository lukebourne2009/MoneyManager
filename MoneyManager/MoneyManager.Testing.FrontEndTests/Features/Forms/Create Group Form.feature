﻿Feature: Create Group Form
	In order to
	As a
	I want to

Background: The Create Group Form is Open
	Given user "Test@test.co.uk" is logged in
	And the user has 1 Budget(s)
	And the user is on the "/Budget/Index" page
	And the user has clicked the "Create Group Button" element
	And the "Create Group Form" element is displayed

Scenario: The Form has a header
	Then the "Create Group Form Header" element is displayed

Scenario: The Form has a name field
	Then the "Create Group Form Name Text Input" element is displayed

Scenario: The Form has a Positive button
	Then the "Create Group Form Submit" element is displayed
	And says "Create Group"

Scenario: The Form has a Negative button
	Then the "Create Group Form Cancel" element is displayed
	And says "Cancel"

Scenario: The Negative button closes the Create Group Form
	When the user clicks the "Create Group Form Cancel" element
	Then the "Create Group Form" element is not displayed

Scenario: A validation error is displayed if no characters are entered into the name field
	Given the user has entered "" into the "Create Group Form Name Text Input" element
	When the user clicks the "Create Group Form Submit" element
	Then the "Create Group Form Validation Error" element is displayed
	And says "You can't have a name with no letters, can you?"

Scenario: A validation error is displayed if no characters are entered more than 128 characters into the name field
	Given the user has entered "12345678901234567890123456789012345678901234567890123456789012345678901234567890123789012345678901234567890123456789012345678901234567890" into the "Create Group Form Name Text Input" element
	When the user clicks the "Create Group Form Submit" element
	Then the "Create Group Form Validation Error" element is displayed
	And says "Don't be greedy with yer letters. Keep it below 128 letters please!"

Scenario: A Valid form creates a Budget Group in the Database
	Given the user has entered "My Group" into the "Create Group Form Name Text Input" element
	When the user clicks the "Create Group Form Submit" element
	Then "Budget" 1 contains 1 "Budget Groups"
	And "Budget Group" 1's "Name" is "My Group"

Scenario: A Valid form appends the Group to the budget table
	Given the user has entered "My Group" into the "Create Group Form Name Text Input" element
	When the user clicks the "Create Group Form Submit" element
	Then 1 "Group Row Name" element is displayed
	And element 1 says "My Group"