﻿Feature: Create Budget Form
	In order to
	As a
	I want to

Background: The Budget Options Menu is Open
	Given user "Test@test.co.uk" is logged in
	And the user has 1 Budget(s)
	And the user is on the "/Budget/Index" page
	And the user has clicked the "Create Budget Button" element
	And the "Create Budget Form" element is displayed

Scenario: The Form has a header
	Then the "Create Budget Form Header" element is displayed

Scenario: The Form has a name field
	Then the "Create Budget Form Name Text Input" element is displayed

Scenario: The Form has a Positive button
	Then the "Create Budget Form Submit" element is displayed
	And says "Create Budget"

Scenario: The Form has a Negative button
	Then the "Create Budget Form Cancel" element is displayed
	And says "Cancel"

Scenario: The Negative button closes the Budget Options Menu
	When the user clicks the "Create Budget Form Cancel" element
	Then the "Create Budget Form" element is not displayed

Scenario: A validation error is displayed if no characters are entered into the name field
	Given the user has entered "" into the "Create Budget Form Name Text Input" element
	When the user clicks the "Create Budget Form Submit" element
	Then the "Create Budget Form Validation Error" element is displayed
	And says "You can't have a name with no letters, can you?"

Scenario: A validation error is displayed if no characters are entered more than 128 characters into the name field
	Given the user has entered "12345678901234567890123456789012345678901234567890123456789012345678901234567890123789012345678901234567890123456789012345678901234567890" into the "Create Budget Form Name Text Input" element
	When the user clicks the "Create Budget Form Submit" element
	Then the "Create Budget Form Validation Error" element is displayed
	And says "Don't be greedy with yer letters. Keep it below 128 letters please!"

Scenario: If form is valid, The Positive button redirects to Budget/Index
	Given the user has entered "My Budget" into the "Create Budget Form Name Text Input" element
	When the user clicks the "Create Budget Form Submit" element
	Then the user is redirected to "/Budget/Index"
	And the "Budget Name" element contains the text "My Budget"
	