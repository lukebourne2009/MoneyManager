﻿Feature: Edit Budget Form
	In order to
	As a 
	I want to 


Background:	Setup
	Given user "Test@test.co.uk" is logged in
	And the user has 1 Budget(s)
	And the user is on the "/Budget/Index" page
	And the user has clicked the "Edit Budget Button" element
	And the "Edit Budget Menu" element is displayed

Scenario: A Text Input is displayed for the Budget Name
	Then the "Edit Budget Name Text Field" element is displayed

Scenario: A Submit button is displayed
	Then the "Edit Budget Form Submit" element is displayed

Scenario: A Cancel button is displayed
	Then the "Edit Budget Form Cancel" element is displayed

Scenario: A validation error is displayed if no characters are entered into the name field
	Given the user has entered "" into the "Edit Budget Name Text Field" element
	When the user clicks the "Edit Budget Form Submit" element
	Then the "Edit Budget Form Validation Error" element is displayed
	And says "You can't have a name with no letters, can you?"

Scenario: A validation error is displayed if no characters are entered more than 128 characters into the name field
	Given the user has entered "12345678901234567890123456789012345678901234567890123456789012345678901234567890123789012345678901234567890123456789012345678901234567890" into the "Edit Budget Name Text Field" element
	When the user clicks the "Edit Budget Form Submit" element
	Then the "Edit Budget Form Validation Error" element is displayed
	And says "Don't be greedy with yer letters. Keep it below 128 letters please!"

Scenario: The Budget Name is changed when the user submits a valid name
	Given the user has entered "New Name" into the "Edit Budget Name Text Field" element
	When the user clicks the "Edit Budget Form Submit" element
	Then "Budget" 1's "Name" is "New Name"
	Then the "Budget Name" element contains the text "New Name"

Scenario: The Cancel button closes the form
	When the user clicks the "Edit Budget Form Cancel" element
	Then the "Edit Budget Menu" element is not displayed

Scenario: The Cancel button resets the validation
	Given the user has entered "" into the "Edit Budget Name Text Field" element
	And the user has clicked the "Edit Budget Form Submit" element
	And the "Edit Budget Form Validation Error" element is displayed
	And the user has clicked the "Edit Budget Form Cancel" element
	When the user clicks the "Edit Budget Button" element
	Then the "Edit Budget Form Validation Error" element is not displayed