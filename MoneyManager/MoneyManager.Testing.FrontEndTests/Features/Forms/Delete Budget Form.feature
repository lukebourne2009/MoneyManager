﻿Feature: Delete Budget Form
	In order to
	As a
	I want to

Background:	Setup
	Given user "Test@test.co.uk" is logged in
	And the user has 1 Budget(s)
	And the user is on the "/Budget/Index" page
	And the user has clicked the "Delete Budget Button" element
	And the "Delete Budget Form" element is displayed

Scenario: A Submit button is displayed
	Then the "Delete Budget Form Submit" element is displayed

Scenario: A Cancel button is displayed
	Then the "Delete Budget Form Cancel" element is displayed

Scenario: Clicking the Cancel button closes the form
	When the user clicks the "Delete Budget Form Cancel" element
	Then the "Delete Budget Form" element is not displayed

Scenario: Clicking the Submit button deletes the budget
	When the user clicks the "Delete Budget Form Submit" element
	Then "Budget" 1 is deleted

Scenario: Clicking the Submit button send you to the Open Budget page
	When the user clicks the "Delete Budget Form Submit" element
	Then the user is redirected to "/Budget/Open"

Scenario: A Message is displayed
	Then the "Delete Budget Form Confirmation Message" element contains the text "Are you sure you want to delete this budget?\r\nThis cannot be reversed.\r\nLike, even if you ask nicely."