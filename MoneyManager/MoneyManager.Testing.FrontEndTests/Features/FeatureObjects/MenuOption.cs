﻿using System.Diagnostics.CodeAnalysis;

namespace MoneyManager.Testing.FrontEndTests.Features.FeatureObjects
{
    [ExcludeFromCodeCoverage]
    public class MenuOption
    {
        public string Text { get; set; }
    }
}