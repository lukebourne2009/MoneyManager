﻿Feature: Open Budget Page
	In order to
	As a
	I want to

Background: user is logged in
	Given user "Test@test.co.uk" is logged in

Scenario: My Budgets Title is displayed
	When the user lands on the "/Budget/Open" page
	Then the Title is displayed
	And says "My Budgets"

Scenario: Create Budget button is displayed
	When the user lands on the "/Budget/Open" page
	Then the Create Budget button is displayed

Scenario: When the user has 1 budget the budget icon is shown
	Given the user has 1 Budget(s)
	When the user lands on the "/Budget/Open" page
	Then budget 1 is displayed as icon 1
	And the Create Budget button is displayed as icon 2

Scenario: The Budgets are shown in order of last used
	Given the user has 5 Budget(s)
	And the budgets have been used in the following order:
	| 1 | 2 | 3 | 4 | 5 |
	| 5 | 2 | 4 | 1 | 3 |
	When the user lands on the "/Budget/Open" page
	Then the budgets are displayed in the that order

Scenario: When the user has 1 budget then the correct name is displayed
	Given the user has 1 Budget(s) named "My Lovely Budget"
	When the user lands on the "/Budget/Open" page
	Then icon 1 has a name "My Lovely Budget"

Scenario: When the user has more than 10 budget, only the first 10 are displayed
	Given the user has 11 Budget(s)
	When the user lands on the "/Budget/Open" page
	Then 10 budget icons are displayed

Scenario: When the user has more than 10 budget, an arrow is displayed to load more
	Given the user has 11 Budget(s)
	When the user lands on the "/Budget/Open" page
	Then a drop down arrow is displayed

@ignore
Scenario: Clicking the drop down arrow loads up to 10 more budgets
	Given the user has 21 Budget(s)
	And the user is on the "/Budget/Open" page
	When the user clicks the drop down arrow
	Then 20 budget icons are displayed
	And a drop down arrow is displayed

Scenario: When the user has less than 10 budget, the arrow is hidden
	Given the user has 9 Budget(s)
	When the user lands on the "/Budget/Open" page
	Then a drop down arrow is not displayed

Scenario: Clicking the budget-icon redirects the user to the budget page
	Given the user has 1 Budget(s) named "My Lovely Budget"
	And the user is on the "/Budget/Open" page
	When the user clicks on budget 1
	Then the user is redirected to "/Budget/Index/.*"
	And the Budget Name is "My Lovely Budget"

Scenario: Clicking the delete-icon deletes the budget removes it
	Given the user has 3 Budget(s)
	And the user is on the "/Budget/Open" page
	When the user clicks on the delete-icon for budget 2
	Then budget 2 is deleted
	And 2 budget icons are displayed

Scenario: Clicking the Create Budget icon opens the Create Budget Form
	Given the user is on the "/Budget/Open" page
	When the user clicks the Create Budget icon
	Then the Create a Budget form is displayed

Scenario: Creating a Budget redirects you to the created Budget page
	Given the user is on the "/Budget/Open" page
	And the user has clicked the Create Budget icon
	And the user has entered "budgety poo" into the Budget Name field
	When the user clicks the Positive Button
	Then the user is redirected to "/Budget/Index/.*"
	And the Budget Name is "budgety poo"