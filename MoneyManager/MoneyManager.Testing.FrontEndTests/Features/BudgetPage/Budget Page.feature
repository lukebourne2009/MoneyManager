﻿Feature: Budget Page
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

Background: User is logged in
	Given user "Test@test.co.uk" is logged in

Scenario: A Budget Table is displayed in the middle of the page
	Given the user has 1 Budget(s)
	When the user lands on the "/Budget/Index" page
	Then the "Budget Table" element is displayed
