﻿Feature: Budget Table Title
	In order to 
	As a
	I want to

Background:	Setup
	Given user "Test@test.co.uk" is logged in
	And the user has 1 Budget(s)

Scenario: An Edit button will show next to the Budget name
	When the user lands on the "/Budget/Index" page
	Then the "Edit Budget Button" element is displayed

Scenario: When the user clicks the edit button the Edit Budget Menu appears
	Given the user is on the "/Budget/Index" page
	When the user clicks the "Edit Budget Button" element
	Then the "Edit Budget Menu" element is displayed

Scenario Outline: The user can close the Edit Budget Menu 
	Given the user is on the "/Budget/Index" page
	And the user has clicked the "Edit Budget Button" element
	And the "Edit Budget Menu" element is displayed
	When the user clicks the "<Element To Click>" element
	Then the "Edit Budget Menu" element is not displayed

Examples: 
	| Element To Click   |
	| Edit Budget Button |
	| Body               |

Scenario: A Delete button will show next to the Budget name
	When the user lands on the "/Budget/Index" page
	Then the "Delete Budget Button" element is displayed

Scenario: When the user clicks the delete button the Delete Budget Menu appears
	Given the user is on the "/Budget/Index" page
	When the user clicks the "Delete Budget Button" element
	Then the "Delete Budget Form" element is displayed

Scenario Outline: The user can close the Delete Budget Menu 
	Given the user is on the "/Budget/Index" page
	And the user has clicked the "Delete Budget Button" element
	And the "Delete Budget Form" element is displayed
	When the user clicks the "<Element To Click>" element
	Then the "Delete Budget Form" element is not displayed

Examples: 
	| Element To Click     |
	| Delete Budget Button |
	| Body                 |

