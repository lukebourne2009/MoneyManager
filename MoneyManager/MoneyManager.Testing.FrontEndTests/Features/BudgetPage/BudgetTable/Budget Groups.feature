﻿Feature: Budget Groups
	In order to
	As a
	I want to

Background: Log in with Default budget
	Given user "Test@test.co.uk" is logged in
	And the user has 1 Budget(s)

Scenario Outline: Given the user has X Budget Groups then X Budget Group rows are displayed
	Given the user has <Group Count> Budget Group(s)
	When the user lands on the "/Budget/Index" page
	Then <Group Count> Budget Group row are displayed

Examples: 
	| Group Count |
	| 1           |
	| 5           |