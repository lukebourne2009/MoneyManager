﻿Feature: Budget Table Layout
	In order to
	As a
	I want to

Background: Log in
	Given user "Test@test.co.uk" is logged in
	And the user has 1 Budget(s)

Scenario: Budget Table has 4 columns
	When the user lands on the "/Budget/Index" page
	Then the Budget Table has the following columns:
	| Column Name |
	| CATEGORY    |
	| BUDGETED    |
	| CASH FLOW   |
	| AVAILABLE   |

Scenario: A Dropper is displayed to the left of each Budget Group name
	Given the user has 1 Budget Group(s) named "My Budget Group 0"
	When the user lands on the "/Budget/Index" page
	Then Budget Group row column 2 has a dropper
	And it is displayed on the left of the Budget Group name
	
Scenario: The Dropper for each Budget Group is displayed pointing down by default
	Given the user has 1 Budget Group(s)
	When the user lands on the "/Budget/Index" page
	Then the Dropper for Budget Group 1 is displayed pointing "Down"

Scenario: Clicking the Dropper once turns it to point right
	Given the user has 1 Budget Group(s)
	And the user is on the "/Budget/Index" page
	When the user clicks on the Dropper for Group 1
	Then the Dropper for Budget Group 1 is displayed pointing "Right"

Scenario: all Budget Categories associated with the Budget Group are shown by default, below
	Given the user has 1 Budget Group(s)
	And the user has 4 Budget Categories for Group 1
	When the user lands on the "/Budget/Index" page
	Then the Budget Categories are displayed

Scenario: Clicking the Dropper hides all Budget Categories associated with the Budget Group
	Given the user has 1 Budget Group(s)
	And the user has 4 Budget Categories for Group 1
	And the user is on the "/Budget/Index" page
	When the user clicks on the Dropper for Group 1
	Then the Budget Categories are not displayed

Scenario Outline: Clicking the Select Checkbox on a Category highlights that Category
	Given the user has 3 Budget Group(s)
	And the user has 4 Budget Categories for Group 1
	And the user has 2 Budget Categories for Group 2
	And the user has 3 Budget Categories for Group 3
	And the user is on the "/Budget/Index" page
	When the user clicks the Select Checkbox on row number <nRow>
	Then row number <Row Number Highlighted> is highlighted

Examples:
	| nRow | Row Number Highlighted |
	| 3    | 3                      |
	| 7    | 7,8,9                  |
	| 1    | All                    |

Scenario Outline: Clicking the Select Checkbox on a Category selects the appropriate child rows below
	Given the user has 3 Budget Group(s)
	And the user has 4 Budget Categories for Group 1
	And the user has 2 Budget Categories for Group 2
	And the user has 3 Budget Categories for Group 3
	And the user is on the "/Budget/Index" page
	When the user clicks the Select Checkbox on row number <nRow>
	Then row number <Row Number Checked> is checked

Examples:
	 | nRow  | Row Number Checked |
	 | 3     | 3                  |
	 | 7     | 7,8,9              |
	 | 1     | All                |
	 | 3,4,2 | 2,3,4,5,6          |
	 | 2,3,1 | All                | 

Scenario: A currency is displayed for each row under columns: Budgeted, Cash Flow and Available
	Given the user has 1 Budget Group(s)
	And the user has 1 Budget Categories for Group 1
	When the user lands on the "/Budget/Index" page
	Then a currency is displayed for each row under columns: Budgeted, Cash Flow and Available

Scenario: A Plus Icon Button is displayed by the Category column header
	When the user lands on the "/Budget/Index" page
	Then the "Create Group Button" element is displayed

Scenario: Clicking the Create Group Button opens the Create Group Form
	And the user is on the "/Budget/Index" page
	When the user clicks the "Create Group Button" element
	Then the "Create Group Form" element is displayed

Scenario: Each Group row has a Create Category Button
	And "Budget" 1 has 3 "Budget Group"
	When the user lands on the "/Budget/Index" page
	Then the "Create Category Button" element is displayed on "Group Row" 1
	And the "Create Category Button" element is displayed on "Group Row" 2
	And the "Create Category Button" element is displayed on "Group Row" 3

Scenario: Clicking the Create Category Button opens the Create Category Form
	And "Budget" 1 has 1 "Budget Group"
	And the user is on the "/Budget/Index" page
	When the user clicks the "Create Category Button" element on "Group Row" 1
	Then the "Create Category Form" element is displayed