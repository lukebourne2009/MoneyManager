﻿Feature: Budget Categories
	In order to
	As a 
	I want to 

Background: Log in and provide data
	Given user "Test@test.co.uk" is logged in
	And the user has 1 Budget(s)
	And the user has 1 Budget Group(s)

Scenario Outline: The Available column on a Category row is the addition of the Budgeted column and the Activity column
	 Given the user has 1 Budget Categories for Group 1
	 And the Budgeted amount is <Budgeted>
	 And the Activity amount is <Activity>
	 When the user lands on the "/Budget/Index" page
	 Then the Available column displays <Available>

Examples:
	| Budgeted | Activity | Available |
	| £0.00    | £0.00    | £0.00     |
	| £10.00   | £0.00    | £10.00    |
	| £10.00   | £-10.00  | £0.00     |
	| £20.00   | £-25.00  | £-5.00    |
	| £20.00   | £20.00   | £40.00    |

Scenario Outline: The Activity column on a Category row is populated
	 Given the user has 1 Budget Categories for Group 1
	 And the Activity amount is <Activity>
	 When the user lands on the "/Budget/Index" page
	 Then the Activity column displays <Activity>

Examples:
	| Activity |
	| £-20.00  |
	| £20.00   |

Scenario Outline: The Budgeted column on a Category row is populated
	 Given the user has 1 Budget Categories for Group 1
	 And the Budgeted amount is <Budgeted>
	 When the user lands on the "/Budget/Index" page
	 Then the Budgeted column displays <Budgeted>

Examples:
	| Budgeted |
	| £-20.00  |
	| £20.00   |

Scenario: A Percentage Bar is displayed behind the Available data
	Given the user has 1 Budget Categories for Group 1
	And the Budgeted amount is £40.00
	And the Activity amount is £-10.00
	When the user lands on the "/Budget/Index" page
	Then the Available Percentage bar is 75% filled

Scenario Outline: A coloured background is displayed around the available column-data for each Category row
	Given the user has 1 Budget Categories for Group 1
	And Budget Category 1's available column-data is "<Sign>"
	When the user lands on the "/Budget/Index" page
	Then Budget Category 1's available column-data is displayed with a "<Colour>" background

Examples:
	| Sign     | Colour |
	| Negative | Red    |
	| Positive | Green  |

Scenario: The Budgeted Value input box displays when the mouse hovers over it
	Given the user has 1 Budget Categories for Group 1
	And the user is on the "/Budget/Index" page
	When the user hovers over Budget Category 1's Budgeted Value
	Then a text input box for Budget Category 1's Budgeted Value is displayed

Scenario: The Budgeted Value input box does not display when the mouse is not hovering over it
	Given the user has 1 Budget Categories for Group 1
	When the user lands on the "/Budget/Index" page
	Then the text input for Budget Category 1's Budgeted Value is not displayed

Scenario: The Budgeted Value input box sticks when the user clicks in it
	Given the user has 1 Budget Categories for Group 1
	And the user is on the "/Budget/Index" page
	And the user has hovered over Budget Category 1's Budgeted Value
	And the user has clicked on Budget Category 1's Budgeted Value input
	When the user moves the mouse out of Budget Category 1's Budgeted Value
	Then a text input box for Budget Category 1's Budgeted Value is displayed
