﻿Feature: Side Menu
	In order to navigate my website
	As a customer
	I want to

Background: 
	Given user "Test@test.co.uk" is logged in

Scenario: A SideMenu is displayed across the left of the page
	When the user lands on the "/Budget/Index" page
	Then the "Side Menu" element is displayed
	
Scenario: The Main Toolbar has a Brand name
	When the user lands on the "/Budget/Index" page
	Then the "Brand Name" element is displayed
	And the "Brand Name" element contains the text "Money Manager"

Scenario: The Main Toolbar has an Accounts Tab
	When the user lands on the "/Budget/Index" page
	Then the "Accounts Tab" element is displayed
	And the "Accounts Tab" element contains the text "Accounts"

Scenario: The Accounts Tab has an account icon
	When the user lands on the "/Budget/Index" page
	Then the "Accounts Tab Icon" element is displayed inside the "Accounts Tab"

Scenario: The Main Toolbar has a Budgets Menu Tab
	When the user lands on the "/Budget/Index" page
	Then the "Budgets Menu Tab" element is displayed
	And the "Budgets Menu Tab" element contains the text "Budgets"

Scenario: The Budgets Menu Tab has an icon
	When the user lands on the "/Budget/Index" page
	Then the "Budgets Menu Tab Icon" element is displayed inside the "Budgets Menu Tab"

Scenario: The Budgets Menu Tab has a Create Budget button Next to it
	When the user lands on the "/Budget/Index" page
	Then the "Create Budget Button" element is displayed
	
Scenario: Clicking the Plus icon on the Budgets Menu Tab opens the Create Budget Form
	And the user is on the "/Budget/Index" page
	When the user clicks the "Create Budget Button" element
	Then the "Create Budget Form" element is displayed