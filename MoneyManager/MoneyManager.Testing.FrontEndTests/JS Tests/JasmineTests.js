﻿/// <reference path="../../MoneyManager.Web/Scripts/jquery-3.1.1.js" />
/// <reference path="../../MoneyManager.Web/Scripts/Site/site-lib.js" />
/// <reference path="../bower_components/jasmine-standalone-2.6.4/lib/jasmine-2.6.4/jasmine.js"/>
/// <reference path="../../MoneyManager.Web/Scripts/Site/Validation/Validate.js" />

describe("Commenter", function() {
    it("Can be created",
        function() {
            var commenter = new Commenter("test");
            expect(commenter).toBeDefined();
        });
    it("Can be Passed a css selector of a container",
        function() {
            var commenter = new Commenter(".Selector");
            expect(commenter.Selector).toBe(".Selector");
        });

    describe("Uncomment",
        function() {
            it("Can remove comments on all elements under the selector container",
                function() {
                    // Given
                    $(".test-setup").append("<div class='selector'></div>");
                    $(".selector").append("<!--<div class='commented'></div>-->");
                    var commenter = new Commenter(".selector");

                    // Act
                    commenter.Uncomment();

                    // Assert
                    var uncommented = $(".commented");
                    expect(uncommented.length).toBeGreaterThan(0);
                });
        });

    describe("Comment",
        function() {
            it("Can add comments to all elements under the selector container",
                function() {
                    // Given
                    $(".test-setup").append("<div class='selector'></div>");
                    $(".selector").append("<div class='commented'></div>");
                    var commenter = new Commenter(".selector");
                    commenter.UncommentedElement = $(".commented");

                    // Act
                    commenter.Comment(".commented");

                    // Assert
                    var uncommented = $(".commented");
                    expect(uncommented.length).toBe(0);
                });
        });

    describe("IsCommented",
        function() {
            it("Can return true when element under selector is commented",
                function() {
                    // Given
                    $(".test-setup").append("<div class='selector'></div>");
                    $(".selector").append("<!--<div class='commented'></div>-->");
                    var commenter = new Commenter(".selector");

                    // Act
                    var result = commenter.IsCommented();

                    // Assert
                    expect(result).toBe(true);
                });

            it("Can return false when element under selector is uncommented",
                function () {
                    // Given
                    $(".test-setup").append("<div class='selector'></div>");
                    $(".selector").append("<!--<div class='commented'></div>-->");
                    var commenter = new Commenter(".selector");
                    commenter.Uncomment();

                    // Act
                    var result = commenter.IsCommented();

                    // Assert
                    expect(result).toBe(false);
                });
        });

    describe("ToggleComment",
        function() {
            it("Comments Something out if it is uncommented",
                function() {
                    // Given
                    $(".test-setup").append("<div class='selector'></div>");
                    $(".selector").append("<div class='commented'></div>");
                    var commenter = new Commenter(".selector");

                    // Act
                    commenter.ToggleComment(".commented");

                    // Assert
                    var result = $(".commented");
                    expect(result.length).toBe(0);
                });

            it("Uncomments Something if it is Commented",
                function () {
                    // Given
                    $(".test-setup").append("<div class='selector'></div>");
                    $(".selector").append("<!--<div class='commented'></div>-->");
                    var commenter = new Commenter(".selector");

                    // Act
                    commenter.ToggleComment(".commented");

                    // Assert
                    var result = $(".commented");
                    expect(result.length).toBe(1);
                });
        });
});

describe("ClickIsOutsideElement", function () {

        it("returns true when clicked element is not child of container",
            function() {
                // Arrange
                $(".test-setup").append("<div class='event'></div>" +
                    "<div class='selector-parent'>" +
                    "<div class='selector'></div>" +
                    "</div>");

                var event = { target: $(".event") };
                var selector = ".selector";

                // Act
                var result = ClickIsOutsideElement(event, selector);

                // Assert
                expect(result).toBe(true);
            });

        it("returns false when clicked element is a child of container",
            function () {
                // Arrange
                $(".test-setup").append(
                    "<div class='selector-parent'>" +
                    "<div class='selector'>" +
                    "<div class='event'></div>" +
                    "</div>" +
                    "</div>");

                var event = { target: $(".event") };
                var selector = ".selector";

                // Act
                var result = ClickIsOutsideElement(event, selector);

                // Assert
                expect(result).toBe(false);
            });

        it("returns false when clicked element is the container",
            function () {
                // Arrange
                $(".test-setup").append(
                    "<div class='container-parent'>" +
                    "<div class='container'>" +
                    "<div class='event'></div>" +
                    "</div>" +
                    "</div>");

                var event = { target: $(".container") };
                var selector = ".container";
  

                // Act
                var result = ClickIsOutsideElement(event, selector);

                // Assert
                expect(result).toBe(false);
            });
    });

describe("Hiders", function() {
        it("Hide attaches display: none to the object",
            function() {
                // Arrange
                $(".test-setup").append("<div class='selector'></div>");
                var selector = ".selector";

                // Act
                Hide(selector);

                // Assert
                expect($(selector).is(":visible")).toBe(false);
            });

        it("Show returns display to intital on the object",
            function() {
                // Arrange
                $(".test-setup").append("<div class='selector'></div>");
                var selector = ".selector";

                // Act
                Show(selector);

                // Assert
                expect($(selector).is(":visible")).toBe(true);
            });

        it("IsHidden returns true when display of object is none",
            function() {
                // Arrange
                $(".test-setup").append("<div class='selector' style='display: none'></div>");
                var selector = ".selector";

                // Act
                var result = IsHidden(selector);

                // Assert
                expect(result).toBe(true);
            });

        it("IsHidden returns false when display of object is not none",
            function () {
                // Arrange
                $(".test-setup").append("<div class='selector'></div>");
                var selector = ".selector";

                // Act
                var result = IsHidden(selector);

                // Assert
                expect(result).toBe(false);
            });
    });

/*describe("Form", function() {
        describe("Open",
            function() {
                it("GivenAHiddenForm_FormIsShownWhenCallingOpen",
                    function() {
                        // Arrange
                        $(".test-setup").append("<div class='form' style='display: hidden'></div>");
                        var selector = ".form";
                        var form = new Form(selector);

                        // Act
                        form.Open();

                        // Assert
                        expect(IsHidden(selector)).toBe(false);
                    });
                it("GivenAShownForm_FormIsStillShownWhenCallingOpen",
                    function () {
                        // Arrange
                        $(".test-setup").append("<div class='form'></div>");
                        var selector = ".form";
                        var form = new Form(selector);

                        // Act
                        form.Open();

                        // Assert
                        expect(IsHidden(selector)).toBe(false);
                    });
                it("Open_GivenAForm_SetsAnyEventBindings",
                    function () {
                        // Arrange
                        $(".test-setup").append("<div class='form'>" +
                            "<div class='positive-button'></div>" +
                            "</div>");
                        var selector = ".form";
                        var form = new Form(selector);
                        var result = false;
                        var func = function () {
                            result = true;
                        }
                        form.BindEvent(".positive-button", "click", func);

                        // Act
                        form.Open();

                        // Assert
                        $(".positive-button").trigger("click");
                        expect(result).toBe(true);
                    });
                it("Open_GivenAForm_SetsMultipleBindings",
                    function () {
                        // Arrange
                        $(".test-setup").append("<div class='form'>" +
                            "<div class='positive-button'></div>" +
                            "</div>");
                        var selector = ".form";
                        var form = new Form(selector);
                        var result1 = false;
                        var result2 = false;
                        var func1 = function () {
                            result1 = true;
                        }
                        var func2 = function () {
                            result2 = true;
                        }
                        form.BindEvent(".positive-button", "click", func1);
                        form.BindEvent(".positive-button", "mouseenter", func2);

                        // Act
                        form.Open();

                        // Assert
                        $(".positive-button").trigger("click");
                        $(".positive-button").trigger("mouseenter");
                        expect(result1).toBe(true);
                        expect(result2).toBe(true);
                    });
            });
        describe("Close",
            function() {
                it("Close_GivenAShownForm_FormIsHiddenWhenCallingClose",
                    function() {
                        // Arrange
                        $(".test-setup").append("<div class='form'></div>");
                        var selector = ".form";
                        var form = new Form(selector);

                        // Act
                        form.Close();

                        // Assert
                        expect(IsHidden(selector)).toBe(true);
                    });
                it("Close_GivenAForm_RemovesAllEventBindings",
                    function () {
                        // Arrange
                        $(".test-setup").append("<div class='form'>" +
                            "<div class='positive-button'></div>" +
                            "</div>");
                        var selector = ".form";
                        var form = new Form(selector);
                        var result = false;
                        var func = function () {
                            result = true;
                        }
                        form.BindEvent(".positive-button", "click", func);

                        // Act
                        form.Close();

                        // Assert
                        $(".positive-button").trigger("click");
                        expect(result).toBe(false);
                    });
            });
        describe("BindEvent",
            function() {
                it("BindEvent_GivenAHiddenForm_FunctionsCanBeBoundToEventsOnSpecificElements",
                    function() {
                        // Arrange
                        $(".test-setup").append("<div class='form'>" +
                            "<div class='positive-button'></div>" +
                            "</div>");
                        var selector = ".form";
                        var form = new Form(selector);
                        var result = false;
                        var func = function() {
                            result = true;
                        }

                        // Act
                        form.BindEvent(".positive-button", "click", func);

                        // Assert
                        expect(form._actionElements.length).toBe(1);
                    });
                it("BindEvent_GivenAHiddenForm_MultipleFunctionsCanBeBound",
                    function () {
                        // Arrange
                        $(".test-setup").append("<div class='form'>" +
                            "<div class='positive-button'></div>" +
                            "</div>");
                        var selector = ".form";
                        var form = new Form(selector);
                        var result = false;
                        var func = function () {
                            result = true;
                        }

                        // Act
                        form.BindEvent(".positive-button", "click", func);
                        form.BindEvent(".positive-button", "mouseenter", func);
                        form.BindEvent(".positive-button", "mouseleave", func);

                        // Assert
                        expect(form._actionElements.length).toBe(3);
                    });
            });
    });

describe("OpenForm", function() {
        it("GivenAHider_HiderIsDisplayed",
            function() {
                // Arrange
                $(".test-setup").append("<div class='form-hider' style='display: none'>" +
                    "<div class='form'></div>" +
                    "</div>");
                var selector = ".form-hider";

                // Act
                OpenForm(".form");

                // Assert
                expect(IsHidden(selector)).toBe(false);
            });
        it("GivenAnElementHasdata-formAttr_AndElementIsDisplayed_ThenOpenFormIsCalled",
            function() {
                // Arrange
                $(".test-setup").append("<div class='click' data-form='form'></div>" +
                    "<div class='form-hider' style='display: hidden'>" +
                    "<div class='form'></div>" +
                    "</div>");
                var selector = ".form-hider";
                BindForms();

                // Act
                $(".click").trigger("click");

                // Assert
                expect(IsHidden(selector)).toBe(false);
            });
        it("GivenAnElementHasdata-formAttr_AndElementIsNotDisplayed_ThenOpenFormIsNotCalled",
            function () {
                // Arrange
                $(".test-setup").append("<div style='display: none' class='click' data-form='form'></div>" +
                    "<div class='form-hider' style='display: none'>" +
                    "<div class='form'></div>" +
                    "</div>");
                var selector = ".form-hider";
                BindForms();

                // Act
                $(".click").trigger("click");

                // Assert
                expect(IsHidden(selector)).toBe(true);
            });
        it("GivenAFormIsOpened_AndTheFormContainsdata-formAttrs_ThenTheyAreBinded",
            function () {
                // Arrange
                $(".test-setup").append("<div class='click' data-form='.form'></div>" +
                    "<div class='form-hider' style='display: none'>" +
                        "<div class='form'>" +
                            "<div data-form='.form2'></div>" +
                        "</div>" +
                    "</div>" +
                    "<div class='form2-hider' style='display: none'>" +
                        "<div class='form2'></div>" +
                    "</div>");
                var selector = ".form-hider";
                BindForms();

                // Act
                $(".click").trigger("click");
                $(".form div").trigger("click");

                // Assert
                expect(IsHidden(".form2-hider")).toBe(false);
            });
        it("OpeningAFormRemovesOpenClickListener_AndAttachesCloseClickListener",
            function () {
                // Arrange
                $(".test-setup").append("<div class='click' data-form='.form'></div>" +
                    "<div class='form-hider' style='display: none'>" +
                    "<div class='form'></div>" +
                    "</div>");
                var selector = ".form-hider";
                BindForms();

                // Act
                $(".click").trigger("click");

                // Assert
                var events = $._data($(".click").get(0), "events").click;

                var found = false;

                for (var i = 0; i < events.length; i++) {
                    var clickHandler = events[i].handler;
                    expect(clickHandler).not.toBe(BindOpenForm);
                    if (clickHandler === BindCloseForm)
                        found = true;
                }

                expect(found).toBe(true);
            });
        it("OpeningAFormASecondTime_RemovesOpenClickListener_AndAttachesCloseClickListener",
            function () {
                // Arrange
                $(".test-setup").append("<div class='click' data-form='.form'></div>" +
                    "<div class='form-hider' style='display: none'>" +
                    "<div class='form'></div>" +
                    "</div>");
                var selector = ".form-hider";
                BindForms();

                // Act
                $(".click").trigger("click");
                $(".click").trigger("click");
                $(".click").trigger("click");

                // Assert
                var events = $._data($(".click").get(0), "events").click;

                var found = false;

                for (var i = 0; i < events.length; i++) {
                    var clickHandler = events[i].handler;
                    expect(clickHandler).not.toBe(BindOpenForm);
                    if (clickHandler === BindCloseForm)
                        found = true;
                }

                expect(found).toBe(true);
            });
        it("OpeningAForm_AddOpenListenersToChildFormButtons",
            function () {
                // Arrange
                $(".test-setup").append("<div class='click' data-form='.form'></div>" +
                                        "<div class='form-hider' style='display: none'>" +
                                            "<div class='form'>" +
                                                "<div data-form='.form2'></div>" +
                                            "</div>" +
                                        "</div>" +
                                        "<div class='form2-hider' style='display: none'>" +
                                        "   <div class='form2'></div>" +
                                        "</div>");
                BindForms();

                // Act
                $(".click").trigger("click");

                // Assert
                var events = $._data($(".form div").get(0), "events").click;

                var found = false;

                for (var i = 0; i < events.length; i++) {
                    var clickHandler = events[i].handler;
                    expect(clickHandler).not.toBe(BindCloseForm);
                    if (clickHandler === BindOpenForm)
                        found = true;
                }

                expect(found).toBe(true);
            });
        it("OpeningAFormASecondTime_AddOpenListenersToChildFormButtons",
            function () {
                // Arrange
                $(".test-setup").append("<div class='click' data-form='.form'></div>" +
                    "<div class='form-hider' style='display: none'>" +
                    "<div class='form'>" +
                    "<div data-form='.form2'></div>" +
                    "</div>" +
                    "</div>" +
                    "<div class='form2-hider' style='display: none'>" +
                    "   <div class='form2'></div>" +
                    "</div>");
                BindForms();

                // Act
                $(".click").trigger("click");
                $(".click").trigger("click");
                $(".click").trigger("click");

                // Assert
                var events = $._data($(".form div").get(0), "events").click;

                var found = false;

                for (var i = 0; i < events.length; i++) {
                    var clickHandler = events[i].handler;
                    expect(clickHandler).not.toBe(BindCloseForm);
                    if (clickHandler === BindOpenForm)
                        found = true;
                }

                expect(found).toBe(true);
            });
});

describe("CloseForm", function () {
        it("GivenAHider_HiderIsHidden",
            function () {
                // Arrange
                $(".test-setup").append("<div class='form-hider'>" +
                    "<div class='form'></div>" +
                    "</div>");
                var selector = ".form-hider";

                // Act
                CloseForm(".form");

                // Assert
                expect(IsHidden(selector)).toBe(true);
            });
        it("ByDefault_OpenButtonAlsoClosesForm",
            function() {
                // Arrange
                $(".test-setup").append("<div class='click' data-form='.form'></div>" +
                    "<div class='form-hider' style='display: none'>" +
                    "<div class='form'></div>" +
                    "</div>");
                var selector = ".form-hider";
                BindForms();

                // Act
                $(".click").trigger("click");
                $(".click").trigger("click");

                // Assert
                expect(IsHidden(selector)).toBe(true);
            });
        it("CanOpenAndCloseAFormMultipleTimes",
            function () {
                // Arrange
                $(".test-setup").append("<div class='click' data-form='.form'></div>" +
                    "<div class='form-hider' style='display: none'>" +
                    "<div class='form'></div>" +
                    "</div>");
                var selector = ".form-hider";
                BindForms();

                // Act
                $(".click").trigger("click");
                expect(IsHidden(selector)).toBe(false);
                $(".click").trigger("click");
                expect(IsHidden(selector)).toBe(true);
                $(".click").trigger("click");
                expect(IsHidden(selector)).toBe(false);
                $(".click").trigger("click");
                expect(IsHidden(selector)).toBe(true);
            });
        it("ClosingAFormRemovesCloseClickListener_AndAttachesOpenClickListener",
            function () {
                // Arrange
                $(".test-setup").append("<div class='click' data-form='.form'></div>" +
                    "<div class='form-hider' style='display: none'>" +
                    "<div class='form'></div>" +
                    "</div>");
                var selector = ".form-hider";
                BindForms();

                // Act
                $(".click").trigger("click");
                $(".click").trigger("click");

                // Assert
                var events = $._data($(".click").get(0), "events").click;

                var found = false;

                for (var i = 0; i < events.length; i++) {
                    var clickHandler = events[i].handler;
                    expect(clickHandler).not.toBe(BindCloseForm);
                    if (clickHandler === BindOpenForm)
                        found = true;
                }

                expect(found).toBe(true);
            });
        it("ClosingAFormASecondTime_RemovesCloseClickListener_AndAttachesOpenClickListener",
            function () {
                // Arrange
                $(".test-setup").append("<div class='click' data-form='.form'></div>" +
                    "<div class='form-hider' style='display: none'>" +
                    "<div class='form'></div>" +
                    "</div>");
                var selector = ".form-hider";
                BindForms();

                // Act
                $(".click").trigger("click");
                $(".click").trigger("click");
                $(".click").trigger("click");
                $(".click").trigger("click");

                // Assert
                var events = $._data($(".click").get(0), "events").click;

                var found = false;

                for (var i = 0; i < events.length; i++) {
                    var clickHandler = events[i].handler;
                    expect(clickHandler).not.toBe(BindCloseForm);
                    if (clickHandler === BindOpenForm)
                        found = true;
                }

                expect(found).toBe(true);
            });
        it("ClosingAForm_RemovesAllClickListenersFromChildButtons",
            function () {
                // Arrange
                $(".test-setup").append("<div class='click' data-form='.form'></div>" +
                    "<div class='form-hider' style='display: none'>" +
                    "<div class='form'>" +
                    "<div data-form='.form2'></div>" +
                    "</div>" +
                    "</div>" +
                    "<div class='form2-hider' style='display: none'>" +
                    "   <div class='form2'></div>" +
                    "</div>");
                BindForms();

                // Act
                $(".click").trigger("click");
                $(".click").trigger("click");

                // Assert
                var events = $._data($(".form div").get(0), "events");
                expect(events).toBe(undefined);
            });
        it("ClosingAFormASecondTime_RemovesAllClickListenersFromChildButtons",
            function () {
                // Arrange
                $(".test-setup").append("<div class='click' data-form='.form'></div>" +
                    "<div class='form-hider' style='display: none'>" +
                    "<div class='form'>" +
                    "<div data-form='.form2'></div>" +
                    "</div>" +
                    "</div>" +
                    "<div class='form2-hider' style='display: none'>" +
                    "   <div class='form2'></div>" +
                    "</div>");
                BindForms();

                // Act
                $(".click").trigger("click");
                $(".click").trigger("click");
                $(".click").trigger("click");
                $(".click").trigger("click");

                // Assert
                var events = $._data($(".form div").get(0), "events");
                expect(events).toBe(undefined);
            });
});*/

describe("Validate", function() {
    it("GivenNoValidation_RunSuccessFunction", function() {
        // Arrange
        $(".test-setup").append("<div class='input'>" +
                                    "<input class='form-control' data-bind='value: groupName' id='group-name-field' name='Name' placeholder='Group Name' type='text' value=''>" +
                                    "<div>" +
                                        "<span class='field-validation-valid text-danger' data-valmsg-for='Name' data-valmsg-replace='true'></span>" +
                                    "</div>" +
                                "</div>");

        // Act
        var success = false;
        Validate(".input", function () {
            success = true;
        });

        // Assert
        expect(success).toBe(true);
    });
    it("Given1Validation_DontRunSuccessFunction", function () {
        // Arrange
        $(".test-setup").append("<div class='input'>" +
            "<input class='form-control' data-val='true' data-val-length='The field Name must be a string with a maximum length of 128.' " +
            "data-val-length-max='128' data-val-required='The Name field is required.' " +
            "id='Name' name='Name' type='text' value=''>" +
            "<div>" +
            "<span class='field-validation-valid text-danger' data-valmsg-for='Name' data-valmsg-replace='true'></span>" +
            "</div>" +
            "</div>");

        // Act
        var success = false;
        Validate(".input", function () {
            success = true;
        });

        // Assert
        expect(success).toBe(false);
    });
    it("Given1Validation_ShowErrorMessage", function () {
        // Arrange
        $(".test-setup").append("<div class='input'>" +
                                    "<input class='form-control' data-val='true' data-val-length='The field Name must be a string with a maximum length of 128.' " +
                                    "data-val-length-max='128' data-val-required='The Name field is required.' " +
                                    "id='Name' name='Name' type='text' value=''>" +
                                    "<div>" +
                                        "<span class='field-validation-valid text-danger' data-valmsg-for='Name' data-valmsg-replace='true'></span>" +
                                    "</div>" +
                                "</div>");

        // Act
        var success = false;
        Validate(".input", null);
        var hasClass = $(".input span").hasClass('field-validation-error');
        var text = $(".input span").text();

        // Assert
        expect(hasClass).toBe(true);
        expect(text).toBe('The Name field is required.');
    });
});

// Tear down
afterEach(function () {
    $(".test-setup").empty();
});