﻿using System.Diagnostics.CodeAnalysis;
using AutomatedTestFramework.Framework;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using TechTalk.SpecFlow;
using MoneyManager.Core.Models;
using MoneyManager.Testing.FrontEndTests.Framework;
using MoneyManager.Testing.TestHelper;
using MoneyManager.Testing.TestHelper.Mocks.Databases;
using MoneyManager.Testing.TestHelper.PropertyHelpers;

namespace MoneyManager.Testing.FrontEndTests.Tests.Global
{
    [Binding]
    [ExcludeFromCodeCoverage]
    public class TestSetup : DataGenerationHelper
    {
        [BeforeScenario]
        public static void Setup()
        {
            // Clean DB
            var repo = new TestDbContext();
            repo.Database.ExecuteSqlCommand("UPDATE Users SET DefaultBudget_Id = NULL; " +
                                            "DELETE FROM Budgets;" +
                                            "DELETE FROM Transactions;" +
                                            "DELETE FROM Accounts;");
        }

        [AfterTestRun]
        public static void GlobalTearDown()
        {
            DriverContext.CloseDriver();
        }

        [Given(@"user ""(.*)"" is logged in")]
        public void GivenUserIsLoggedIn(string user)
        {
            Pages.LoginPage.GoTo();
            Pages.LoginPage.Login(user, "Password123!");

            var userMan = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(DbHelper.Repo));
            UserHelper.ExpectedModel = userMan.FindByEmail("Test@test.co.uk");
        }
    }
}
