﻿using System;
using AutomatedTestFramework.Framework;
using MoneyManager.Testing.FrontEndTests.Framework;
using MoneyManager.Testing.TestHelper;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace MoneyManager.Testing.FrontEndTests.Tests.Global
{
    [Binding]
    public class General : PageHelper
    {
        #region Click

        [Given(@"the user has clicked the ""(.*)"" element")]
        public void GivenTheUserHasClickedTheElement(string p0)
        {
            WhenTheUserClicksTheElement(p0);
        }

        [When(@"the user clicks the ""(.*)"" element")]
        public void WhenTheUserClicksTheElement(string p0)
        {
            Pages.Elements[p0].Click();
        }

        [When(@"the user clicks the ""(.*)"" element on ""(.*)"" (.*)")]
        public void WhenTheUserClicksTheElementOn(string p0, string p1, int p2)
        {
            var element = QueriedElement = Pages.Elements[p1].On(Pages.Elements[p1][p2 - 1]);
            element.Click();
        }

        #endregion

        #region Displayed

        [Given(@"the ""(.*)"" element is displayed")]
        public void GivenTheElementIsDisplayed(string p0)
        {
            ThenTheElementIsDisplayed(p0);
        }

        [Then(@"the ""(.*)"" element is displayed")]
        public void ThenTheElementIsDisplayed(string p0)
        {
            var element = QueriedElement = Pages.Elements[p0];
            new WebDriverWait(DriverContext.Driver, TimeSpan.FromMilliseconds(3000))
                .Until(x => element.IsDisplayed());
            Assert.IsTrue(element.IsDisplayed());
        }

        [Then(@"the ""(.*)"" element is not displayed")]
        public void ThenTheElementIsNotDisplayed(string p0)
        {
            var element = QueriedElement = Pages.Elements[p0];
            new WebDriverWait(DriverContext.Driver, TimeSpan.FromMilliseconds(3000))
                .Until(x => !element.IsDisplayed());
            Assert.IsFalse(element.IsDisplayed());
        }

        [Then(@"the ""(.*)"" element is displayed inside the ""(.*)""")]
        public void ThenTheElementIsDisplayedInsideThe(string p0, string p1)
        {
            var elementLocation = Pages.Elements[p0].GetElement().Location;
            var elementSize = Pages.Elements[p0].GetElement().Size;

            var containerLocation = Pages.Elements[p1].GetElement().Location;
            var containerSize = Pages.Elements[p1].GetElement().Size;

            Assert.GreaterOrEqual(elementLocation.X, containerLocation.X);
            Assert.GreaterOrEqual(elementLocation.Y, containerLocation.Y);
            Assert.LessOrEqual(elementLocation.X + elementSize.Width, containerLocation.X + containerSize.Width);
            Assert.LessOrEqual(elementLocation.Y + elementSize.Height, containerLocation.Y + containerSize.Height);
        }

        [Then(@"(.*) ""(.*)"" element is displayed")]
        public void ThenElementIsDisplayed(int p0, string p1)
        {
            var element = QueriedElement = Pages.Elements[p1];
            new WebDriverWait(DriverContext.Driver, TimeSpan.FromMilliseconds(3000))
                .Until(x => element.IsDisplayed());

            Assert.IsTrue(element.IsDisplayed());

            Assert.AreEqual(p0, element.Elements.Count);
        }

        [Then(@"the ""(.*)"" element is displayed on ""(.*)"" (.*)")]
        public void ThenTheElementIsDisplayedOn(string @object, string parent, int parentN)
        {
            var element = QueriedElement = Pages.Elements[@object].On(Pages.Elements[parent][parentN-1]);
            new WebDriverWait(DriverContext.Driver, TimeSpan.FromMilliseconds(3000))
                .Until(x => element.IsDisplayed());

            Assert.IsTrue(element.IsDisplayed());
        }

        #endregion

        [Then(@"the ""(.*)"" element has a background-image of ""(.*)""")]
        public void ThenTheElementHasABackground_ColorOf(string element, string colour)
        {
            var backgroundColour = Pages.Elements[element].GetElement().GetCssValue("background-image");
            Assert.AreEqual(colour, backgroundColour);
        }

        #region Text

        [Given(@"the user has entered ""(.*)"" into the ""(.*)"" element")]
        public void GivenTheUserHasEnteredIntoTheElement(string text, string element)
        {
            Pages.Elements[element].EnterText(text);
        }

        [Then(@"the ""(.*)"" element contains the text ""(.*)""")]
        public void ThenTheElementContainsTheText(string element, string expectedText)
        {
            var webElement = Pages.Elements[element].GetElement();
            expectedText = expectedText.Replace("\\r\\n", "\r\n");
            new WebDriverWait(DriverContext.Driver, TimeSpan.FromMilliseconds(3000))
                .Until(x => expectedText == webElement.Text);
            Assert.AreEqual(expectedText, webElement.Text);
        }

        [Then(@"says ""(.*)""")]
        public void ThenSays(string text)
        {
            Assert.AreEqual(text, QueriedElement.GetElement().Text);
        }

        [Then(@"element (.*) says ""(.*)""")]
        public void ThenElementSays(int p0, string p1)
        {
            Assert.AreEqual(p1, QueriedElement.Elements[p0-1].Text);
        }

        #endregion New Region
    }
}
