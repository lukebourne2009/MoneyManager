﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using MoneyManager.Common.Managers.Accounts;
using MoneyManager.Common.Transformers;
using MoneyManager.Common.Transformers.Accounts;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Data.Repositories;
using MoneyManager.Testing.TestHelper;
using MoneyManager.Testing.TestHelper.Mocks.Databases;
using NUnit.Framework;
using TechTalk.SpecFlow;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Models.Accounts;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Data.Database;
using MoneyManager.Data.Repositories.Accounts;
using MoneyManager.Testing.TestHelper.Base;
using MoneyManager.Testing.TestHelper.Exceptions;
using MoneyManager.Testing.TestHelper.PropertyHelpers;
using OpenQA.Selenium;
using PropertyHelper = MoneyManager.Testing.TestHelper.Base.PropertyHelper;

namespace MoneyManager.Testing.FrontEndTests.Tests.Global
{
    [Binding]
    public class DataGeneration : DataGenerationHelper
    {
        #region Givens

        #region Specifics

        [Given(@"the user has (.*) Budget\(s\)")]
        public void GivenTheUserHasBudgetS(int nBudgets)
        {
            var budgetHelper = new PropertyHelper<BudgetModel, UserEntity>();
            budgetHelper.ExpectedModels = CreateBudgets(nBudgets, GetUser("Test@test.co.uk").User).ToList();
        }

        [Given(@"the user has (.*) Budget Group\(s\)")]
        public void GivenTheUserHasBudgetGroupS(int p0)
        {
            new PropertyHelper<BudgetGroupModel,BudgetModel>().ExpectedModels =
                CreateBudgetGroups(p0, BudgetGroupHelper.ParentModel(), DbHelper.Repo).ToList();
        }

        [Given(@"the user has (.*) Budget Group\(s\) named ""(.*)""")]
        public void GivenTheUserHasBudgetGroupSNamed(int p0, string p1)
        {
            var data = new List<BudgetGroupModel>();
            for (int i = 0; i < p0; i++)
            {
                data.Add(new BudgetGroupModel
                {
                    Name = p1
                });
            }

            BudgetGroupHelper.ExpectedModels =
                CreateBudgetGroups(data, BudgetGroupHelper.ParentModel(), DbHelper.Repo).ToList();
        }

        [Given(@"the user has (.*) Budget Categories for Group (.*)")]
        public void GivenTheUserHasBudgetCategoriesForGroup(int p0, int p1)
        {
            BudgetGroupModel group = new PropertyHelper<BudgetCategoryModel,BudgetGroupModel>().ParentModel(p1 - 1);
            DataGenerationHelper dgHelper = new DataGenerationHelper();
            List<BudgetCategoryModel> categories = dgHelper.CreateBudgetCategories(p0, group, DbHelper.Repo).ToList();
            new PropertyHelper<BudgetCategoryModel,BudgetGroupModel>().ExpectedModels = categories.ToList();
        }

        [Given(@"Budget Category (.*)'s available column-data is ""(.*)""")]
        public void GivenBudgetCategorySAvailableColumn_DataIs(int p0, string p1)
        {
            var accountHelper = new PropertyHelper<AccountModel, BudgetModel>();
            IAccess<AccountEntity> accAccess = new AccountAccess(accountHelper.ParentModel().Id, DbHelper.Repo);
            IManager<AccountModel> accManager = new AccountManager(accAccess, new AccountEntityToAccountModel(), new AccountModelToAccountEntity());

            var accModel = accManager.Create(new AccountModel());

            IAccess<TransactionEntity> transAccess = new TransactionAccess(accModel.Id, DbHelper.Repo);
            IManager<TransactionModel> transManager = new TransactionManager(transAccess);

            var catHelper = new PropertyHelper<BudgetCategoryModel, BudgetGroupModel>();

            DbHelper.Repo.MonthlyCategories.Add(new MonthlyCategoryEntity
            {
                Date = NowDateConversion,
                ParentId = catHelper.ExpectedModel.Id
            });
            DbHelper.Repo.SaveChanges();

            int? val = null;
            if (p1 == "Negative") val = -1000;
            else if (p1 == "Positive") val = 1000;
            else if (p1 == "Neutral") val = 0;

            transManager.Create(new TransactionModel
            {
                CategoryId = catHelper.ExpectedModels[p0-1].Id,
                Value = (int)val
            });
        }

        [Given(@"the Budgeted amount is £(.*)")]
        public void GivenTheBudgetedAmountIs(Decimal p0)
        {
            var catHelper = new PropertyHelper<BudgetCategoryModel, BudgetGroupModel>();

            DbHelper.Repo.MonthlyCategories.Add(new MonthlyCategoryEntity
            {
                BudgetedValue = (int) (p0 * 100),
                Date = NowDateConversion,
                ParentId = catHelper.ExpectedModels[0].Id
            });
            DbHelper.Repo.SaveChanges();

            var entity = new BudgetCategoryAccess(catHelper.ParentModel().Id, DbHelper.Repo).GetById(catHelper.ExpectedModels[0].Id,
                nameof(BudgetCategoryEntity.Transactions));

            // TODO
            catHelper.ExpectedModels[0] = null;//Transform.ToBudgetCategoryModel(entity);
        }

        [Given(@"the Activity amount is £(.*)")]
        public void GivenTheActivityAmountIs(Decimal p0)
        {
            var accountHelper = new PropertyHelper<AccountModel, BudgetModel>();
            var account = Transform.ToAccountModel(new AccountAccess(accountHelper.ParentModel().Id, DbHelper.Repo)
                .Insert(new AccountEntity()));
            var transHelper = new PropertyHelper<TransactionModel, AccountModel>();
            new TransactionAccess(account.Id, DbHelper.Repo).Insert(new TransactionEntity
            {
                CategoryId = new PropertyHelper<BudgetCategoryModel, BudgetGroupModel>().ExpectedModels[0].Id,
                Value = (int) p0 * 100
            });
        }

        [Given(@"the user has (.*) Budget\(s\) named ""(.*)""")]
        public void GivenTheUserHasBudgetSNamed(int nBudgets, string name)
        {
            // Arrange
            var budgetHelper = new PropertyHelper<BudgetModel, UserEntity>();
            var data = new List<BudgetModel>();
            for (int i = 0; i < nBudgets; i++)
            {
                data.Add(new BudgetModel { Name = name });
            }
            budgetHelper.ExpectedModels = CreateBudgets(data,GetUser("Test@test.co.uk").User).ToList();
        }

        [Given(@"the budgets have been used in the following order:")]
        public void GivenTheBudgetsHaveBeenUsedInTheFollowingOrder(Table table)
        {


            var order = table.Rows[0].Values.Select(x => Convert.ToInt32(x)).ToList();

            var budgets = Transform.ToBudgetEntities(GetBudgets(GetUser("Test@test.co.uk"))).ToList();
            for (int i = 0; i < budgets.Count; i++)
            {
                budgets[order[i]-1].LastUsed = budgets[order[i] - 1].LastUsed.AddDays(i);
            }
            var access = new BudgetAccess(GetUser("Test@test.co.uk").User, DbHelper.Repo);
            foreach (var budget in budgets)
            {
                //access.Update(budget);
                var budgetInDb = DbHelper.Repo.Budgets.Find(budget.Id);
                DbHelper.Repo.Entry(budgetInDb).CurrentValues.SetValues(budget);
                DbHelper.Repo.Entry(budgetInDb).State = EntityState.Modified;
                DbHelper.Repo.SaveChanges();
            }

            new PropertyHelper<BudgetModel, UserEntity>().ExpectedModels 
                = GetBudgets(GetUser("Test@test.co.uk")).ToList();
        }

        #endregion New Region

        [Given(@"""(.*)"" (.*) has (.*) ""(.*)""")]
        public void GivenHas(string parent, int parentN, int objectN, string @object)
        {
            @object = @object.Replace(" ", "").ToLower();
            parent = parent.Replace(" ", "").ToLower();

            var id = GetObjectId(parent, parentN);
            var repo = new TestDbContext();
            for (int i = 0; i < objectN; i++)
            {
                dynamic obj = _newObjects[@object]();
                obj.ParentId = (int)id;
                DbSets[@object](repo).Add(obj);
            }
            repo.SaveChanges();
        }


        #endregion

        #region Thens

        [Then(@"budget (.*) is deleted")]
        public void ThenBudgetIsDeleted(int p0)
        {
            Thread.Sleep(500);

            var id = new PropertyHelper<BudgetModel, UserEntity>().ExpectedModels[p0-1].Id;
            DbHelper.Repo = new TestDbContext();
            var result = GetBudgets(GetUser("Test@test.co.uk")).Any(x => x.Id == id);
            Assert.IsFalse(result);
        }

        [Then(@"""(.*)"" (.*)'s ""(.*)"" is ""(.*)""")]
        public void ThenSIs(string objectName, int p1, string p2, string p3)
        {
            objectName = objectName.Replace(" ", "");
            object id;
            try
            {
                id = GetObjectId(objectName, p1);
            }
            catch (KeyNotFoundException)
            {
                id = GetObjectIdFromRepo(objectName, p1);
            }

            var repo = new TestDbContext();
            var prop = DbSets[objectName](repo).Find(id).GetType().GetProperty(p2);
            var value = prop.GetValue(DbSets[objectName](repo).Find(id));
            Assert.AreEqual(p3, value);
        }

        [Then(@"""(.*)"" (.*) contains (.*) ""(.*)""")]
        public void ThenContains(string parent, int nParent, int childCount, string child)
        {
            _currentQuieredDbSetKey = parent;
            _currentQuieredObjectId = GetObjectId(parent, nParent);
            var childObjs = GetChildCollection(parent, _currentQuieredObjectId, child);

            int count = 0;
            var enumerator = childObjs.GetEnumerator();
            while (enumerator.MoveNext())
            {
                count++;
            }
            Assert.AreEqual(childCount, count);
        }
        [Then(@"""(.*)"" (.*) is deleted")]
        public void ThenIsDeleted(string p0, int p1)
        {
            var id = GetObjectId(p0, p1);
            
            var entity = DbSets[p0](new TestDbContext()).Find(id);
            Assert.IsNull(entity);
        }

        #endregion

        #region Privates

        private PropertyHelper<BudgetGroupModel, BudgetModel> BudgetGroupHelper { get; }
            = new PropertyHelper<BudgetGroupModel, BudgetModel>();
        private string NowDateConversion
            => $"{(DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month : DateTime.Now.Month.ToString())}-{DateTime.Now.Year}";

        private static object _currentQuieredObjectId;
        private static string _currentQuieredDbSetKey;

        private static Dictionary<string, Func<object>> _newObjects
            = new Dictionary<string, Func<object>>
            {
                {"budgetgroup", () => new BudgetGroupEntity {Name = "Name"}}
            };

        private static readonly Dictionary<string, Type> ObjectModelTypes
            = new Dictionary<string, Type>
            {
                {"budget", typeof(BudgetModel) },
                {"budgetgroup", typeof(BudgetGroupModel) }
            };

        private static readonly Dictionary<string, Func<MoneyManagerDbContext, DbSet>> DbSets
            = new Dictionary<string, Func<MoneyManagerDbContext,DbSet>>
            {
                {"budget", x => x.Budgets },
                {"budgetgroup", x => x.BudgetGroups }
            };

        private static IEnumerable GetChildCollection(string parent, object id, string child)
        {
            var type = DbSets[parent](new TestDbContext()).Find(id).GetType();
            var property = type.GetProperties()
                .Where(x => x.CustomAttributes.Any(y =>
                    y.AttributeType == typeof(ColumnAttribute) &&
                    y.ConstructorArguments.Count(z => ((string)z.Value).Contains(child.Replace(" ", ""))) == 1))
                .ToList();

            if (property.Count == 0)
                throw new NotFoundException($"Could not find property named{child.Replace(" ", "")}");
            if (property.Count > 1)
                throw new TooManyException("Too many properties found");

            var parentTask = DbSets[parent](new TestDbContext()).Include(property[0].Name).ToListAsync();
            parentTask.Wait();
            var parentObj = parentTask.Result.FirstOrDefault(x => (int) GetObjectId(x) == (int) id);

            IEnumerable childObjs = property[0].GetValue(parentObj) as IEnumerable;
            return childObjs;
        }
        private static object GetObjectIdFromRepo(string child, int nChild)
        {
            var childs = GetChildCollection(_currentQuieredDbSetKey, _currentQuieredObjectId, child);

            var enumerator = childs.GetEnumerator();
            for (int i = 0; i < nChild; i++)
            {
                enumerator.MoveNext();
            }

            return GetObjectId(enumerator.Current);
        }
        private static object GetObjectId(string @object, int objectN)
        {
            dynamic dynamicModel = new PropertyHelper(ObjectModelTypes[@object.Replace(" ", "")]).ExpectedModels;
            IList model = dynamicModel as IList;
            var id = ObjectModelTypes[@object.Replace(" ", "")].GetProperty("Id").GetValue(model[objectN - 1]);
            return id;
        }
        private static object GetObjectId(object obj)
        {
            var id = obj.GetType().GetProperty("Id").GetValue(obj);
            return id;
        }

        #endregion
    }
}
