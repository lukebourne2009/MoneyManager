﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using AutomatedTestFramework.Framework;
using AutomatedTestFramework.Framework.Common;
using MoneyManager.Testing.FrontEndTests.Framework;
using MoneyManager.Testing.FrontEndTests.Framework.Elements;
using MoneyManager.Testing.FrontEndTests.Framework.Elements.Enums;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace MoneyManager.Testing.FrontEndTests.Tests.Budget.BudgetTable
{
    [Binding]
    public class BudgetTableLayoutSteps
    {
        #region Givens

        [Given(@"the user has hovered over Budget Category (.*)'s Budgeted Value")]
        public void GivenTheUserHasHoveredOverBudgetCategorySBudgetedValue(int p0)
        {
            WhenTheUserHoversOverBudgetCategorySBudgetedValue(p0);
        }

        [Given(@"the user has clicked on Budget Category (.*)'s Budgeted Value input")]
        public void GivenTheUserHasClickedOnBudgetCategorySBudgetedValueInput(int p0)
        {
            Thread.Sleep(2000);
            Pages.BudgetPage.BudgetCategoryRow.Combine(BudgetCategoryRow.BudgetedValueColumn)[p0 - 1].Click();
        }

        #endregion Givens

        #region Whens
        [When(@"the user moves the mouse out of Budget Category (.*)'s Budgeted Value")]
        public void WhenTheUserMovesTheMouseOutOfBudgetCategorySBudgetedValue(int p0)
        {
            Pages.BudgetPage.BudgetCategoryRow.Combine(BudgetCategoryRow.BudgetedValueColumn)
                .MouseLeave(p0-1);
        }


        [When(@"the user clicks on the Dropper for Group (.*)")]
        public void WhenTheUserClicksOnTheDropperForGroup(int p0)
        {
            var element = Pages.BudgetPage.BudgetGroupRow[p0-1];
            new ElementQuery(element, BudgetGroupRow.Dropper).Click();
        }
        
        [When(@"the user clicks the Select Checkbox on row number (.*)")]
        public void WhenTheUserClicksTheSelectCheckboxOnRowNumber(List<string> p0)
        {
            foreach (var number in p0)
            {
                int num = Convert.ToInt32(number);
                IWebElement element = Pages.BudgetPage.BudgetTable.Find("tr[row-data]").ElementAt(num - 1);

                new ElementQuery(element, Framework.Elements.BudgetTable.Checkbox).Click();
            }
        }

        [When(@"the user hovers over Budget Category (.*)'s Budgeted Value")]
        public void WhenTheUserHoversOverBudgetCategorySBudgetedValue(int p0)
        {
            var row = Pages.BudgetPage.BudgetCategoryRow;
            new ElementQuery(row, BudgetCategoryRow.BudgetedValueColumn)
                .MouseOver(p0 - 1);
        }

        #endregion

        #region Thens

        [Then(@"row number (.*) is checked")]
        public void ThenRowNumberIsChecked(List<string> p0)
        {
            AssertOnEachRow(p0, x => Assert.IsTrue(x.Selected));
        }

        [Then(@"row number (.*) is highlighted")]
        public void ThenRowNumberIsHighlighted(List<string> p0)
        {
            Action<IWebElement> func = x =>
            {
                if (x.GetAttribute("class").Contains("row-selected"))
                {
                    Assert.Fail();
                }
            };

            AssertOnEachRow(p0, func);
        }

        [Then(@"the Budget Table has the following columns:")]
        public void ThenTheBudgetTableHasTheFollowingColumns(Table table)
        {
            for (var i = 0; i < table.RowCount; i++)
            {
                var actualText = Pages.BudgetPage.BudgetTable.Headings[i].Text;
                var expectedText = table.Rows[i]["Column Name"];
                Assert.AreEqual(expectedText,actualText);
            }
        }

        [Then(@"the Dropper for Budget Group (.*) is displayed pointing ""(.*)""")]
        public void ThenTheDropperForBudgetGroupIsDisplayedPointing(int p0, string p1)
        {
            Direction dir = (Direction)Enum.Parse(typeof(Direction), p1);
            var element = Pages.BudgetPage.BudgetGroupRow[p0 - 1];
            var dropped = new ElementQuery(element, BudgetGroupRow.Dropper);

            if (dir == Direction.Down)
                Assert.IsTrue(dropped[0].GetAttribute("class").Contains("fa-rotate-90"));
            else if(dir == Direction.Right)
                Assert.IsFalse(dropped[0].GetAttribute("class").Contains("fa-rotate"));
        }
        
        [Then(@"the Budget Categories are displayed")]
        public void ThenTheBudgetCategoriesAreDisplayed()
        {
            bool displayed = Pages.BudgetPage.BudgetCategoryRow.AreDisplayed();
            Assert.IsTrue(displayed);
        }

        [Then(@"the Budget Categories are not displayed")]
        public void ThenTheBudgetCategoriesAreNotDisplayed()
        {
            bool displayed = Pages.BudgetPage.BudgetCategoryRow.AreDisplayed();
            Assert.IsFalse(displayed);
        }

        [Then(@"Budget Category (.*)'s border thickness increases")]
        public void ThenBudgetCategorySBorderThicknessIncreases(int p0)
        {
            bool border = Pages.BudgetPage.BudgetCategoryRow[p0 - 1]
                .GetAttribute("class").Contains("thick-border");

            Assert.IsTrue(border);
        }
        
        [Then(@"Budget Category (.*)'s Budgeted data displays a border around it")]
        public void ThenBudgetCategorySBudgetedDataDisplaysABorderAroundIt(int p0)
        {
            var budgetedBorder = Pages.BudgetPage.BudgetCategoryRow[p0 - 1].FindElement(By.CssSelector(".column-budgeted div"))
                .GetAttribute("class").Contains("budgeted-border");

            Assert.IsTrue(budgetedBorder);
        }
        
        [Then(@"a currency is displayed for each row under columns: Budgeted, Cash Flow and Available")]
        public void ThenACurrencyIsDisplayedForEachRowUnderColumnsBudgetedCashFlowAndAvailable()
        {
            var budgeted = Pages.BudgetPage.BudgetCategoryRow[0].FindElement(By.CssSelector(".column-budgeted")).Text;
            var activity = Pages.BudgetPage.BudgetCategoryRow[0].FindElement(By.CssSelector(".column-cash-flow")).Text;
            var available = Pages.BudgetPage.BudgetCategoryRow[0].FindElement(By.CssSelector(".column-available")).Text;
            var currencyReg = new Regex(@"[£$]\d+.\d\d");
            Assert.IsTrue(currencyReg.IsMatch(budgeted));
            Assert.IsTrue(currencyReg.IsMatch(activity));
            Assert.IsTrue(currencyReg.IsMatch(available));
        }
        [Then(@"Budget Category (.*)'s available column-data is displayed with a ""(.*)"" background")]
        public void ThenBudgetCategorySAvailableColumn_DataIsDisplayedWithABackground(int p0, string colour)
        {
            switch (colour)
            {
                case "Red":
                    var backgroundColourRed = DriverContext.Driver.FindElements(By.CssSelector(".budget-category-row + tr .percentage-bar div"))[p0 - 1]
                        .GetCssValue("background-color");
                    Assert.AreEqual("rgba(255, 0, 0, 1)", backgroundColourRed);
                    break;
                case "Green":
                    var backgroundColourGreen = DriverContext.Driver.FindElements(By.CssSelector(".budget-category-row + tr .percentage-bar div"))[p0 - 1]
                        .GetCssValue("background-color");
                    Assert.AreEqual("rgba(0, 128, 0, 1)", backgroundColourGreen);
                    break;
            }
        }

        [Then(@"a text input box for Budget Category (.*)'s Budgeted Value is displayed")]
        public void ThenATextInputBoxForBudgetCategorySBudgetedValueIsDisplayed(int p0)
        {
            var displayed = new ElementQuery(Pages.BudgetPage.BudgetCategoryRow[p0 - 1], BudgetCategoryRow.BudgetedValueColumn + " input")
                .IsDisplayed(100);
            Assert.IsTrue(displayed);
        }
        [Then(@"the text input for Budget Category (.*)'s Budgeted Value is not displayed")]
        public void ThenTheTextInputForBudgetCategorySBudgetedValueIsNotDisplayed(int p0)
        {
            var displayed = new ElementQuery(Pages.BudgetPage.BudgetCategoryRow[p0 - 1], BudgetCategoryRow.BudgetedValueColumn + " input")
                .IsDisplayed(100);
            Assert.IsFalse(displayed);
        }


        #endregion
        [StepArgumentTransformation]
        public List<string> TransformToListOfString(string commaSeparatedList)
        {
            return commaSeparatedList.Split(',').ToList();
        }
        private static void AssertOnEachRow(List<string> p0, Action<IWebElement> assert)
        {
            foreach (var number in p0)
            {
                if (number == "All")
                {
                    var allRows = Pages.BudgetPage.BudgetTable.Find("tr[row-data]");
                    foreach (var webElement in allRows)
                    {
                        var selected = new ElementQuery(webElement, Framework.Elements.BudgetTable.Checkbox).Element;
                        assert(selected);
                    }
                }
                else
                {
                    int num = Convert.ToInt32(number);
                    IWebElement element = Pages.BudgetPage.BudgetTable.Find("tr[row-data]").ElementAt(num - 1);

                    var value = new ElementQuery(element, Framework.Elements.BudgetTable.Checkbox).Element;
                    assert(value);
                }
            }
        }
    }
}
