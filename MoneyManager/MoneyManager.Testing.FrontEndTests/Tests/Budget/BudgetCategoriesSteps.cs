﻿using System;
using System.Globalization;
using System.Linq;
using AutomatedTestFramework.Framework;
using MoneyManager.Testing.FrontEndTests.Framework;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace MoneyManager.Testing.FrontEndTests.Tests.Budget
{
    [Binding]
    public class BudgetCategoriesSteps
    {
        [Then(@"the Available column displays £(.*)")]
        public void ThenTheAvailableColumnDisplays(decimal p0)
        {
            var text = Pages.BudgetPage.BudgetCategoryRow.Find(".column-available").ElementAt(0).Text;
            var expected = "£" + Convert.ToString(p0, CultureInfo.CurrentCulture);
            Assert.AreEqual(expected, text);
        }

        [Then(@"the Activity column displays £(.*)")]
        public void ThenTheActivityColumnDisplays(decimal p0)
        {
            var text = Pages.BudgetPage.BudgetCategoryRow.Find(".column-cash-flow").ElementAt(0).Text;
            var expected = "£" + Convert.ToString(p0, CultureInfo.CurrentCulture);
            Assert.AreEqual(expected, text);
        }

        [Then(@"the Budgeted column displays £(.*)")]
        public void ThenTheBudgetedColumnDisplays(Decimal p0)
        {
            var text = Pages.BudgetPage.BudgetCategoryRow.Find(".column-budgeted").ElementAt(0).Text;
            var expected = "£" + Convert.ToString(p0, CultureInfo.CurrentCulture);
            Assert.AreEqual(expected, text);
        }

        [Then(@"the Available Percentage bar is (.*)% filled")]
        public void ThenTheAvailablePercentageBarIsFilled(int p0)
        {
            var parentWidth = DriverContext.Driver.FindElement(By.CssSelector(".percentage-bar")).GetCssValue("width");
            var width = DriverContext.Driver.FindElement(By.CssSelector(".percentage-bar div")).GetCssValue("width");
            parentWidth = parentWidth.Replace("px", "");
            width = width.Replace("px", "");

            var percent = (int)(Convert.ToDecimal(width) / Convert.ToDecimal(parentWidth) * 100);
            Assert.IsTrue(percent >= p0-1 && percent <= p0+1);
        }

    }
}
