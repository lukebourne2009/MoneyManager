﻿using System;
using System.Diagnostics.CodeAnalysis;
using AutomatedTestFramework.Framework;
using MoneyManager.Testing.FrontEndTests.Framework;
using MoneyManager.Testing.TestHelper;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace MoneyManager.Testing.FrontEndTests.Tests.Budget
{
    [Binding]
    [ExcludeFromCodeCoverage]
    public class CreateBudgetFormSteps
    {
        [Given(@"the user has entered ""(.*)"" into the Budget Name field")]
        public void GivenTheUserHasEnteredIntoTheBudgetNameField(string text)
        {
            WhenTheUserEntersIntoTheBudgetNameField(text);
        }

        [Given(@"the user has clicked the Negative button")]
        public void GivenTheUserHasClickedTheNegativeButton()
        {
            WhenTheUserClicksTheNegativeButton();
        }

        [Given(@"the user has clicked the Positive Button")]
        public void GivenTheUserHasClickedThePositiveButton()
        {
            WhenTheUserClicksThePositiveButton();
        }

        [When(@"the user clicks the Negative button")]
        public void WhenTheUserClicksTheNegativeButton()
        {
            PageHelper pageHelper = new PageHelper();
            pageHelper.Form.NegativeButton.Click();
        }

        [When(@"the user enters ""(.*)"" into the Budget Name field")]
        public void WhenTheUserEntersIntoTheBudgetNameField(string text)
        {
            Pages.BudgetPage.CreateBudgetForm.BudgetNameFieldInput
                .EnterText(text);
        }

        [When(@"the user clicks the Positive Button")]
        public void WhenTheUserClicksThePositiveButton()
        {
            PageHelper pageHelper = new PageHelper();
            pageHelper.Form.PositiveButton.Click();
        }

        [Then(@"the form has the heading: ""(.*)""")]
        public void ThenTheFormHasTheHeading(string expectedText)
        {
            var actualText = Pages.BudgetPage.CreateBudgetForm.Heading.GetElement().Text;
            Assert.AreEqual(expectedText, actualText);
        }
        
        [Then(@"the form has a field named: ""(.*)""")]
        public void ThenTheFormHasAFieldNamed(string expectedText)
        {
            var actualText = Pages.BudgetPage.CreateBudgetForm.BudgetNameField.GetElement().Text;
            Assert.AreEqual(expectedText, actualText);
        }
        
        [Then(@"the form has a Positive button with text: ""(.*)""")]
        public void ThenTheFormHasAPositiveButtonWithText(string text)
        {
            var actualText = Pages.BudgetPage.CreateBudgetForm.PositiveButton.GetElement().GetAttribute("value");
            Assert.AreEqual(text, actualText);
        }
        
        [Then(@"the form has a Negative button with text: ""(.*)""")]
        public void ThenTheFormHasANegativeButtonWithText(string text)
        {
            var actualText = Pages.BudgetPage.CreateBudgetForm.NegativeButton.GetElement().GetAttribute("value");
            Assert.AreEqual(text, actualText);
        }
        
        [Then(@"an error is displayed")]
        public void ThenAnErrorIsDisplayed()
        {
            var element = Pages.BudgetPage.CreateBudgetForm.BudgetNameFieldError;
            Assert.IsNotNull(element);
        }

        [Then(@"the user is redirected to ""(.*)""")]
        public void ThenTheUserIsRedirectedTo(string url)
        {
            new WebDriverWait(DriverContext.Driver, TimeSpan.FromMilliseconds(3000))
                .Until(x => DriverContext.IsUrl(url));
        }
    }
}
