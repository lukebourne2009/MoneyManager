﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using AutomatedTestFramework.Framework;
using AutomatedTestFramework.Framework.Common;
using MoneyManager.Testing.FrontEndTests.Features.FeatureObjects;
using MoneyManager.Testing.FrontEndTests.Framework;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace MoneyManager.Testing.FrontEndTests.Tests.Budget
{
    [Binding]
    [ExcludeFromCodeCoverage]
    public class BudgetHomePageSteps
    {
        #region Givens

        [Given(@"the user is on the ""(.*)"" page")]
        public void GivenTheUserIsOnThePage(string url)
        {
            WhenTheUserLandsOnThePage(url);
        }

        [Given(@"the user has clicked on the Budget Name button")]
        public void GivenTheUserHasClickedOnTheBudgetNameButton()
        {
            WhenTheUserClicksTheButton();
        }

        [Given(@"the Budget Options menu is open")]
        public void GivenTheBudgetOptionsMenuIsOpen()
        {
            WhenTheBudgetOptionsMenuIsOpen();
        }

        #endregion

        #region Whens

        [When(@"the user clicks the Budget Name button")]
        public void WhenTheUserClicksTheButton()
        {
            Pages.BudgetPage.BudgetName.Click();
        }

        [When(@"the user clicks the Add Category Group button")]
        public void WhenTheUserClicksTheAddCategoryGroupButton()
        {
            Pages.BudgetPage.BudgetToolbar.AddGroupButton.Click();
        }

        [When(@"the user lands on the ""(.*)"" page")]
        public void WhenTheUserLandsOnThePage(string url)
        {
            Page.GoTo(url);
        }

        [When(@"the Budget Options menu is open")]
        public void WhenTheBudgetOptionsMenuIsOpen()
        {
            if (!Pages.BudgetPage.BudgetOptionsMenu.IsDisplayed())
            {
                Pages.BudgetPage.BudgetName.Click();
            }
        }

        #endregion

        #region Thens

        [Then(@"the Budget Options menu displays the following links:")]
        public void ThenTheBudgetOptionsMenuDisplaysTheFollowingLinks(Table table)
        {
            var links = table.CreateSet<MenuOption>();
            var elements = Pages.BudgetPage.BudgetOptionsMenu.Find("li span");
            for (var i = 0; i < links.Count(); i++)
            {
                var ele = elements.ElementAt(i);
                var link = links.ElementAt(i);
                Assert.AreEqual(link.Text,ele.Text);
            }
        }

        [Then(@"the Budget Name is displayed")]
        public void ThenTheBudgetNameIsDisplayed()
        {
            var result = Pages.BudgetPage.BudgetName.IsDisplayed();
            Assert.IsTrue(result);
        }

        [Then(@"the Budget Options menu is opened")]
        public void ThenTheBudgetOptionsMenuIsOpened()
        {
            var result = Pages.BudgetPage.BudgetOptionsMenu.IsDisplayed();
            Assert.IsTrue(result);
        }

        [Then(@"the Create Group Form is displayed")]
        public void ThenTheCreateGroupFormIsDisplayed()
        {
            var displayed = Pages.BudgetPage.CreateGroupForm.IsDisplayed();
            Assert.IsTrue(displayed);
        }

        [Then(@"the Budget Options menu is not displayed")]
        public void ThenTheBudgetOptionsMenuIsNotDisplayed()
        {
            var result = Pages.BudgetPage.BudgetOptionsMenu.IsDisplayed();
            Assert.IsFalse(result);
        }

        [Then(@"the Budget Toolbar is displayed")]
        public void ThenTheBudgetToolbarIsDisplayed()
        {
            var result = Pages.BudgetPage.BudgetToolbar.IsDisplayed();
            Assert.IsTrue(result);
        }

        [Then(@"the Budget Name is ""(.*)""")]
        public void ThenTheBudgetNameIs(string name)
        {
            Assert.AreEqual(name, Pages.BudgetPage.BudgetName.Element.Text);
        }

        [Then(@"a transition plays for (.*) ms")]
        public void ThenATransitionPlaysForMs(int ms)
        {
            // Attach listener to transition end and result to dom
            IJavaScriptExecutor script = DriverContext.Driver as IJavaScriptExecutor;
            script.ExecuteScript(@"
            
            document.result = false;

            $('.budget-options-menu-hider')
                .on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', 
                function(event) {
                document.result = true;
            });");

            // Wait for 3/4 the duration of the ms
            int wait = (int)(ms * 0.75);
            Thread.Sleep(wait);

            var result = script.ExecuteScript("return document.result").ToString();
            Assert.AreEqual(result,"False");

            Thread.Sleep(ms - wait);
            result = script.ExecuteScript("return document.result").ToString();
            Assert.AreEqual(result, "True");

        }

        #endregion
    }
}
