﻿using System;
using System.Linq;
using AutomatedTestFramework.Framework;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Testing.FrontEndTests.Framework;
using MoneyManager.Testing.TestHelper;
using MoneyManager.Testing.TestHelper.Base;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace MoneyManager.Testing.FrontEndTests.Tests.Budget
{
    [Binding]
    public class BudgetGroupFormSteps
    {
        private PropertyHelper<BudgetGroupModel, BudgetModel> BudgetGroupHelper { get; }
            = new PropertyHelper<BudgetGroupModel, BudgetModel>();

        [Given(@"The Create Group form is open")]
        public void GivenTheCreateGroupFormIsOpen()
        {
            Pages.BudgetPage.BudgetToolbar.AddGroupButton.Click();

            PageHelper pageHelper = new PageHelper();
            var form = pageHelper.Form = Pages.BudgetPage.CreateGroupForm;

            Assert.IsTrue(form.IsDisplayed());
        }

        [Given(@"The Create Group form is close and reopened")]
        public void GivenTheCreateGroupFormIsCloseAndReopened()
        {
            PageHelper pageHelper = new PageHelper();
            var form = pageHelper.Form = Pages.BudgetPage.CreateGroupForm;
            if (form.IsDisplayed())
            {
                Pages.BudgetPage.BudgetToolbar.AddGroupButton.Click();
                Pages.BudgetPage.BudgetToolbar.AddGroupButton.Click();
            }
            else
            {
                Pages.BudgetPage.BudgetToolbar.AddGroupButton.Click();
            }

            Assert.IsTrue(form.IsDisplayed());
        }

        [Given(@"the user has entered ""(.*)"" into the Group Name field")]
        public void GivenTheUserHasEnteredIntoTheCategoryNameField(string p0)
        {
            Pages.BudgetPage.CreateGroupForm.GroupNameField.EnterText(p0);
            BudgetGroupHelper.ExpectedModel.Name = p0;
        }

        [Then(@"the Group Name field error is displayed")]
        public void ThenTheGroupNameFieldErrorIsDisplayed()
        {
            var displayed = Pages.BudgetPage.CreateGroupForm.GroupNameFieldError.IsDisplayed(500);
            Assert.IsTrue(displayed);
        }

        [Then(@"the Group Name field error is not displayed")]
        public void ThenTheGroupNameFieldErrorIsNotDisplayed()
        {
            new WebDriverWait(DriverContext.Driver, TimeSpan.FromMilliseconds(3000))
                .Until(x => !Pages.BudgetPage.CreateGroupForm.GroupNameFieldError.IsDisplayed());
        }

        [Then(@"the Create Group Form is not displayed")]
        public void ThenTheCreateGroupFormIsNotDisplayed()
        {
            new WebDriverWait(DriverContext.Driver, TimeSpan.FromMilliseconds(3000))
                .Until(x => !Pages.BudgetPage.CreateGroupForm.IsDisplayed());
        }

        [Then(@"the Group Name field is empty")]
        public void ThenTheGroupNameFieldIsEmpty()
        {
            new WebDriverWait(DriverContext.Driver, TimeSpan.FromMilliseconds(3000))
                .Until(x => Pages.BudgetPage.CreateGroupForm.GroupNameField.IsDisplayed());
            var text = Pages.BudgetPage.CreateGroupForm.GroupNameField.Element.Text;
            Assert.IsEmpty(text);
        }

        [Then(@"the Group is stored")]
        public void ThenTheGroupIsStored()
        {
            DataGenerationHelper dgHelper = new DataGenerationHelper();
            Assert.IsTrue(dgHelper.AssertBudgetGroups(BudgetGroupHelper.ExpectedModels.ToList(),
                                                      BudgetGroupHelper.ParentModel()));
        }

        [Then(@"the Group is not stored")]
        public void ThenTheGroupIsNotStored()
        {
            DataGenerationHelper dgHelper = new DataGenerationHelper();
            Assert.IsTrue(dgHelper.AssertNotBudgetGroups(BudgetGroupHelper.ExpectedModels.ToList(),
                                                         BudgetGroupHelper.ParentModel()));
        }

    }
}
