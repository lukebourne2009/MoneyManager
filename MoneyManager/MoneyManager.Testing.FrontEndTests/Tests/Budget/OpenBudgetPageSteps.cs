﻿using System.Linq;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Testing.FrontEndTests.Framework;
using MoneyManager.Testing.TestHelper;
using MoneyManager.Testing.TestHelper.Base;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace MoneyManager.Testing.FrontEndTests.Tests.Budget
{
    [Binding]
    public class OpenBudgetPageSteps : PageHelper
    {
        [Given(@"the user has clicked the Create Budget icon")]
        public void GivenTheUserHasClickedTheCreateBudgetIcon()
        {
            WhenTheUserClicksTheCreateBudgetIcon();
        }

        #region Whens

        [When(@"the user clicks on budget (.*)")]
        public void WhenTheUserClicksOnBudget(int p0)
        {
            Pages.OpenBudgetPage.OpenBudgetIcons.Click(p0-1);
        }

        [When(@"the user clicks on the delete-icon for budget (.*)")]
        public void WhenTheUserClicksOnTheDelete_IconForBudget(int p0)
        {
            Pages.OpenBudgetPage.OpenBudgetIcons.MouseOver(p0 - 1);
            Pages.OpenBudgetPage.OpenBudgetIcons.DeleteBudget.Click(p0 - 1);
        }

        [When(@"the user clicks the Create Budget icon")]
        public void WhenTheUserClicksTheCreateBudgetIcon()
        {
            Pages.OpenBudgetPage.CreateBudget.Click();
            new PageHelper().Form = Pages.OpenBudgetPage.CreateBudgetForm;
        }

        [When(@"the user clicks the drop down arrow")]
        public void WhenTheUserClicksTheDropDownArrow()
        {
            Pages.OpenBudgetPage.DropDownArrow.Click();
        }


        #endregion

        #region Thens

        [Then(@"the Title is displayed")]
        public void ThenTheTitleIsDisplayed()
        {
            var result = Pages.OpenBudgetPage.Title.IsDisplayed();
            Assert.IsTrue(result,"Title Is Not Displayed");

            QueriedElement = Pages.OpenBudgetPage.Title;
        }
        
        [Then(@"the Create Budget button is displayed")]
        public void ThenTheCreateBudgetButtonIsDisplayed()
        {
            var result = Pages.OpenBudgetPage.CreateBudget.IsDisplayed();
            Assert.IsTrue(result, "Title Is Not Displayed");

            QueriedElement = Pages.OpenBudgetPage.CreateBudget;
        }
        
        [Then(@"budget (.*) is displayed as icon (.*)")]
        public void ThenBudgetIsDisplayedAsIcon(int budgetN, int iconN)
        {
            var budgetHelper = new PropertyHelper<BudgetModel, UserEntity>();
            var element = Pages.OpenBudgetPage.OpenBudgetIcons.GetElement(iconN-1);
            var elementId = element.GetAttribute("data-id");
            var budget = budgetHelper.ExpectedModels[budgetN-1];

            Assert.AreEqual(budget.Id.ToString(),elementId);
        }
        
        [Then(@"the Create Budget button is displayed as icon (.*)")]
        public void ThenTheCreateBudgetButtonIsDisplayedAsIcon(int iconN)
        {
            var element = Pages.OpenBudgetPage.ClickableIcons.GetElement(iconN-1);
            var id = element.GetAttribute("id");

            Assert.AreEqual("create-budget", id);
        }
        
        [Then(@"icon (.*) has a name ""(.*)""")]
        public void ThenIconHasAName(int iconN, string name)
        {
            var element = Pages.OpenBudgetPage.OpenBudgetIcons.GetElement(iconN-1);
            var text = element.Text;

            Assert.AreEqual(name,text);
        }
        
        [Then(@"(.*) budget icons are displayed")]
        public void ThenBudgetIconsAreDisplayed(int count)
        {
            var result = Pages.OpenBudgetPage.OpenBudgetIcons.Elements.Count;
            Assert.AreEqual(count, result);
        }
        
        [Then(@"a drop down arrow is displayed")]
        public void ThenADropDownArrowIsDisplayed()
        {
            var result = Pages.OpenBudgetPage.DropDownArrow.IsDisplayed();
            Assert.IsTrue(result);
        }

        [Then(@"a drop down arrow is not displayed")]
        public void ThenADropDownArrowIsNotDisplayed()
        {
            var result = Pages.OpenBudgetPage.DropDownArrow.IsDisplayed();
            Assert.IsFalse(result);
        }

        [Then(@"the budgets are displayed in the that order")]
        public void ThenTheBudgetsAreDisplayedInTheThatOrder()
        {
            var budgetHelper = new PropertyHelper<BudgetModel, UserEntity>();
            var sortedBudgets = budgetHelper.ExpectedModels.OrderByDescending(x => x.LastUsed);

            for (int i = 0; i < sortedBudgets.Count(); i++)
            {
                var element = Pages.OpenBudgetPage.OpenBudgetIcons.GetElement(i);
                var id = element.GetAttribute("data-id");

                Assert.AreEqual(sortedBudgets.ElementAt(i).Id.ToString(), id);
            }
        }
        #endregion

    }
}
