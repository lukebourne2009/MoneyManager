﻿using MoneyManager.Testing.FrontEndTests.Framework;
using NUnit.Framework;
using System;
using AutomatedTestFramework.Framework;
using AutomatedTestFramework.Framework.Common;
using MoneyManager.Testing.FrontEndTests.Framework.Elements;
using MoneyManager.Testing.TestHelper;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace MoneyManager.Testing.FrontEndTests.Tests.Budget
{
    [Binding]
    public class BudgetGroupsSteps : PageHelper
    {
        [Then(@"(.*) Budget Group row\(s\) are displayed")]
        public void ThenBudgetGroupRowsAreDisplayed(int p0)
        {
            new WebDriverWait(DriverContext.Driver, TimeSpan.FromMilliseconds(3000)).Until(x =>
            {
                int total = Pages.BudgetPage.BudgetGroupRow.Elements.Count;
                int count = 0;
                for (int i = 0; i < total; i++)
                {
                    var isDisplayed = Pages.BudgetPage.BudgetGroupRow[0].Displayed;
                    if (isDisplayed) count++;
                }
                return count == p0;
            });
        }

        [Then(@"the Budget Group row column (.*) says ""(.*)""")]
        public void ThenTheBudgetGroupRowColumnSays(int p0, string p1)
        {
            var isDisplayed = Pages.BudgetPage.BudgetGroupRow.Columns[p0-1].Displayed;
            Assert.IsTrue(isDisplayed);
            var text = Pages.BudgetPage.BudgetGroupRow.Columns[p0-1].Text;
            Assert.AreEqual(p1, text);
        }

        [Then(@"Budget Group row column (.*) has a dropper")]
        public void ThenBudgetGroupRowColumnHasADropper(int p0)
        {
            var column = Pages.BudgetPage.BudgetGroupRow.Columns[p0 - 1];
            QueriedElement = new ElementQuery(column, BudgetGroupRow.Dropper);
            var isDisplayed = QueriedElement.IsDisplayed();
            Assert.IsTrue(isDisplayed);
        }

        [Then(@"it is displayed on the left of the Budget Group name")]
        public void ThenItIsDisplayedOnTheLeftOfTheBudgetGroupName()
        {
            var dropperPoint = QueriedElement.GetElement().Location;
            var namePoint = Pages.BudgetPage.BudgetGroupRow.BudgetName.GetElement().Location;

            Assert.That(dropperPoint.X,Is.LessThan(namePoint.X));
            Assert.That(dropperPoint.Y, Is.InRange(namePoint.Y-10,namePoint.Y+10));
        }
    }
}
