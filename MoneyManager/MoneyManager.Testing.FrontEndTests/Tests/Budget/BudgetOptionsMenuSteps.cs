﻿using System.Diagnostics.CodeAnalysis;
using MoneyManager.Testing.FrontEndTests.Framework;
using MoneyManager.Testing.TestHelper;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace MoneyManager.Testing.FrontEndTests.Tests.Budget
{
    [Binding]
    [ExcludeFromCodeCoverage]
    public class BudgetOptionsMenuSteps
    {
        [Given(@"the user has clicked on the Create A Budget Form button")]
        public void GivenTheUserHasClickedOnTheCreateABudgetFormButton()
        {
            if (!Pages.BudgetPage.CreateBudgetForm.IsDisplayed())
            {
                WhenTheUserClicksOnTheCreateABudgetFormOption();
            }
        }

        [When(@"the user clicks on the Open A Budget Form option")]
        public void WhenTheUserClicksOnTheOpenABudgetFormOption()
        {
            //new WebDriverWait(DriverContext.Driver, TimeSpan.FromMilliseconds(3000))
            //    .Until(x => Pages.BudgetPage.BudgetOptionsMenu.OpenBudget.IsDisplayed());
                Pages.BudgetPage.BudgetOptionsMenu.OpenBudget.Click();
        }


        [When(@"the user clicks on the Create A Budget Form option")]
        public void WhenTheUserClicksOnTheCreateABudgetFormOption()
        {
            Pages.BudgetPage.BudgetOptionsMenu.CreateBudget.Click();
            new PageHelper().Form = Pages.BudgetPage.CreateBudgetForm;
        }
        
        [Then(@"the Create a Budget form is displayed")]
        public void ThenTheCreateABudgetFormIsDisplayed()
        {
            var result = Pages.BudgetPage.CreateBudgetForm.IsDisplayed();
            Assert.IsTrue(result);
        }
    }
}
