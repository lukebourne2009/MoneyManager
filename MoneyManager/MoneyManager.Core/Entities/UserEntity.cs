﻿using MoneyManager.Core.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Entities;

namespace MoneyManager.Core.Entities
{
    [Table("Users")]
    public class UserEntity : IEntity<string>, IGroupParent<BudgetEntity>, IChild<ApplicationUser>
    {
        [Key, ForeignKey("Parent")]
        public string Id { get; set; }
        public ApplicationUser Parent { get; set; }

        // Properties
        [Column("Budgets")]
        public ICollection<BudgetEntity> Collection { get; set; }
        public BudgetEntity DefaultBudget { get; set; }
    }
}
