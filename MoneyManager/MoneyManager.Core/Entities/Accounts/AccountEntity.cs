﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Entities;

namespace MoneyManager.Core.Entities.Accounts
{
    [Table("Accounts")]
    public class AccountEntity : IEntity<int>, IChildId<int>, IChild<BudgetEntity>
    {
        [Key]
        public int Id { get; set; }
        [Required, ForeignKey("Parent")]
        public int ParentId { get; set; }
        public BudgetEntity Parent { get; set; }
    }
}