﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MoneyManager.Core.Interfaces.Entities;

namespace MoneyManager.Core.Entities.Accounts
{
    [Table("Transactions")]
    public class TransactionEntity : IEntity<int>, IChild<AccountEntity>, IChildId<int>
    {
        [Key]
        public int Id { get; set; }
        [Required, ForeignKey("Parent")]
        public int ParentId { get; set; }
        public AccountEntity Parent { get; set; }

        // Properties
        [Required]
        public int Value { get; set; }
        [Required]
        public int CategoryId { get; set; }
    }
}