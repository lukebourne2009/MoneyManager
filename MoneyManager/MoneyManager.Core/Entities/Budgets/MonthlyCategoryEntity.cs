﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MoneyManager.Core.Interfaces.Entities;

namespace MoneyManager.Core.Entities.Budgets
{
    [Table("MonthlyCategories")]
    public class MonthlyCategoryEntity : IChild<BudgetCategoryEntity>, IChildId<int>, IEntity<int>
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Parent")]
        [Column("BudgetCategory_Id")]
        [Required]
        public int ParentId { get; set; }
        public BudgetCategoryEntity Parent { get; set; }

        [Required]
        public int BudgetedValue { get; set; }
        [Required]
        [StringLength(7,MinimumLength = 7)]
        public string Date { get; set; }
    }
}