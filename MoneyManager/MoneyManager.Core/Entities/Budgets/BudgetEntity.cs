﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MoneyManager.Core.Interfaces.Entities;

namespace MoneyManager.Core.Entities.Budgets
{
    [Table("Budgets")]
    public class BudgetEntity : IEntity<int>, IChild<UserEntity>, IGroupParent<BudgetGroupEntity>
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Parent")]
        public string User_Id { get; set; }
        [Required, Column("User")]
        public UserEntity Parent { get; set; }
        [Column("BudgetGroups")]
        public ICollection<BudgetGroupEntity> Collection { get; set; }

        // Properties
        [StringLength(128)]
        public string Name { get; set; }
        //[Required]
        [Column(TypeName = "datetime2")]
        public DateTime LastUsed { get; set; }
    }
}