﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Interfaces.Entities;

namespace MoneyManager.Core.Entities.Budgets
{
    [Table("BudgetCategories")]
    public class BudgetCategoryEntity : IChild<BudgetGroupEntity>, IChildId<int>, IEntity<int>
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Parent")]
        [Column("BudgetGroup_Id")]
        [Required]
        public int ParentId { get; set; }
        public BudgetGroupEntity Parent { get; set; }

        // Properties
        [Required]
        public string Name { get; set; }
        [Required]
        public int CategoryOrder { get; set; }
        public ICollection<TransactionEntity> Transactions { get; set; }
        public ICollection<MonthlyCategoryEntity> MonthlyCategories { get; set; }
    }
}