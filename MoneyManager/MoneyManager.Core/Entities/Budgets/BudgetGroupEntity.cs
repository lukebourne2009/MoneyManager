﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MoneyManager.Core.Interfaces.Entities;

namespace MoneyManager.Core.Entities.Budgets
{
    [Table("BudgetGroups")]
    public class BudgetGroupEntity : IEntity<int>, IChild<BudgetEntity>, IChildId<int>, IGroupParent<BudgetCategoryEntity>
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Parent")]
        [Column("Budget_Id")]
        [Required]
        public int ParentId { get; set; }
        public BudgetEntity Parent { get; set; }

        // Properties
        [Required]
        public string Name { get; set; }
        [Column("BudgetCategories")]
        public ICollection<BudgetCategoryEntity> Collection { get; set; }


    }
}