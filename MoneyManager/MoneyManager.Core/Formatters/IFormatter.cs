﻿namespace MoneyManager.Core.Formatters
{
    public interface IFormatter<TObject>
    {
        TObject Format(TObject obj);
    }
}