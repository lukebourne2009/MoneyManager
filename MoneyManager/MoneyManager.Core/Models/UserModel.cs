﻿using System.Collections.Generic;
using MoneyManager.Core.Models.Budgets;

namespace MoneyManager.Core.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        public BudgetModel DefaultBudget { get; set; }
        public ICollection<BudgetModel> Budgets { get; set; }
    }
}