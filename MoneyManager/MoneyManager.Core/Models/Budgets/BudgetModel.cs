﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoneyManager.Core.Models.Budgets
{
    public class BudgetModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        [Required(ErrorMessage = @"You can't have a name with no letters, can you?")]
        [StringLength(128, ErrorMessage = @"Don't be greedy with yer letters. Keep it below 128 letters please!")]
        public string Name { get; set; }
        public DateTime LastUsed { get; set; }

        public IEnumerable<BudgetGroupModel> BudgetGroups { get; set; }
    }
}