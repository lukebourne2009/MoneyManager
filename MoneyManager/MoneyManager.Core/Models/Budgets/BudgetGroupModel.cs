﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoneyManager.Core.Models.Budgets
{
    public class BudgetGroupModel
    {
        public int Id { get; set; }
        [Required]
        public int BudgetId { get; set; }

        // Properties
        [Required(ErrorMessage = "You can\'t have a name with no letters, can you?")] 
        [StringLength(128, ErrorMessage = "Don\'t be greedy with yer letters. Keep it below 128 letters please!")]
        public string Name { get; set; }
        public IEnumerable<BudgetCategoryModel> BudgetCategories { get; set; }
    }
}