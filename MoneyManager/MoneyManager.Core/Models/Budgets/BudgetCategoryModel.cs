﻿using System.ComponentModel.DataAnnotations;

namespace MoneyManager.Core.Models.Budgets
{
    public class BudgetCategoryModel
    {
        public int Id { get; set; }
        [Required]
        public int BudgetGroupId { get; set; }

        // Properties
        [Required]
        public string Name { get; set; }
        public int BudgetedValue { get; set; }
        public int CategoryOrder { get; set; }
        public int ActivityValue { get; set; }
        public int Available { get; set; }
        public int PercentageAvailable { get; set; }
    }
}
