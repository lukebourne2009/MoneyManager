﻿using System.ComponentModel.DataAnnotations;

namespace MoneyManager.Core.Models.Accounts
{
    public class AccountModel
    {
        public int Id { get; set; }
        [Required]
        public int BudgetId { get; set; }
    }
}