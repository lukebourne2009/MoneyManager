﻿using System.ComponentModel.DataAnnotations;

namespace MoneyManager.Core.Models.Accounts
{
    public class TransactionModel
    {
        public int Id { get; set; }
        [Required]
        public int AccountId { get; set; }

        // Properties
        [Required]
        public int Value { get; set; }
        public int CategoryId { get; set; }
    }
}