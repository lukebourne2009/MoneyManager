﻿using System.Collections.Generic;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Interfaces.Service;

namespace MoneyManager.Core.Services
{
    public interface ITransactionSummingService : IService<int, IEnumerable<TransactionEntity>>
    {
        
    }
}