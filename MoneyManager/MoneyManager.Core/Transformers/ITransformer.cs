﻿using System.Collections.Generic;

namespace MoneyManager.Core.Transformers
{
    public interface ITransformer<out TTarget, in TSource>
    {
        TTarget Transform(TSource source);
        IEnumerable<TTarget> Transform(IEnumerable<TSource> sources); 
    }
}
