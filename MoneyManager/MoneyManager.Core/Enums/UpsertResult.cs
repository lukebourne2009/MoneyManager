﻿namespace MoneyManager.Core.Enums
{
    public enum UpsertResult
    {
        Unknown = 0,
        Inserted = 1,
        Updated = 2
    }
}
