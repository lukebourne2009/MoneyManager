﻿namespace MoneyManager.Core.Interfaces.Entities
{
    public interface IChildId<TId>
    {
        TId ParentId { get; set; }
    }
}
