﻿namespace MoneyManager.Core.Interfaces.Entities
{
    public interface IChild<TParent>
    {
        TParent Parent { get; set; }
    }
}
