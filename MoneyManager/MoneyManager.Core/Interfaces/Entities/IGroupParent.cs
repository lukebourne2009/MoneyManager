﻿using System.Collections.Generic;

namespace MoneyManager.Core.Interfaces.Entities
{
    public interface IGroupParent<TChild>
    {
        ICollection<TChild> Collection { get; set; }
    }
}
