﻿using System.Collections.Generic;

namespace MoneyManager.Core.Interfaces.Managers
{
    public interface IManager<TModel>
    {
        TModel Create(TModel obj);
        IEnumerable<TModel> Get();
        TModel Update(TModel obj);
		void Delete(int id);
    }
}
