﻿using System;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Models.Budgets;

namespace MoneyManager.Core.Interfaces.Managers
{
    public interface IBudgetCategoryManager : IManager<BudgetCategoryModel>
    {
        BudgetCategoryModel UpdateProperty(int id, Action<BudgetCategoryEntity> action);
        BudgetCategoryModel UpdateCategoryOrder(int id, int newOrder);
    }
}