﻿using System;
using System.Collections.Generic;
using MoneyManager.Core.Models.Budgets;

namespace MoneyManager.Core.Interfaces.Managers
{
	public interface IBudgetGroupManager
	{
		BudgetGroupModel CreateBudgetGroup(BudgetGroupModel budgetGroup);
		IEnumerable<BudgetGroupModel> GetBudgetGroups(DateTime? month = null);
	}
}