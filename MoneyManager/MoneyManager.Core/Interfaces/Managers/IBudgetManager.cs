﻿using System;
using System.Collections.Generic;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Models.Budgets;

namespace MoneyManager.Core.Interfaces.Managers
{
	public interface IBudgetManager : IManager<BudgetModel>
	{
		IEnumerable<BudgetModel> Get(int? amount = null);

		BudgetModel GetBudget(int id);

		BudgetModel GetLastUsedBudget();

		int BudgetCount();
		BudgetModel Update(int id, Action<BudgetEntity> func);
	}
}