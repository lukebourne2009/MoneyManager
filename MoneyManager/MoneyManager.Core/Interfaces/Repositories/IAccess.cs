﻿namespace MoneyManager.Core.Interfaces.Repositories
{
    public interface IAccess<T> : ICreateAccess<T>, IReadAccess<T>, IDeleteAccess
    {
        int Count();
    }
}
