﻿using System;
using MoneyManager.Core.Enums;

namespace MoneyManager.Core.Interfaces.Repositories
{
    public interface ICreateAccess<TObject>
    {
        UpsertResult Upsert(TObject entity);
        TObject Insert(TObject entity);
        TObject Update(TObject entity);
        TObject Update<TId>(TId id, Action<TObject> propertyAssign);
    }
}
