﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MoneyManager.Core.Interfaces.Repositories
{
    public interface IReadAccess<T>
    {
        IEnumerable<T> GetAllWhere(Func<T, bool> comparitor, params string[] includes);
	    IEnumerable<T> GetAll();
	    IEnumerable<T> GetAll(params string[] includes);
		IEnumerable<T> GetAll(params Expression<Func<T, IEnumerable<object>>>[] func);
        IEnumerable<T> GetAll<TSelect1>(Expression<Func<T, IEnumerable<TSelect1>>> func);
        IEnumerable<T> GetAll<TSelect1, TSelect2>(Expression<Func<T, IEnumerable<TSelect1>>> func, Expression<Func<T, IEnumerable<TSelect2>>> func2);
	    IEnumerable<T> GetById<TId>(TId budgetId);
	    IEnumerable<T> GetById<TId>(TId budgetId, params string[] includes);
	    IEnumerable<T> GetById<TId>(TId budgetId, params Expression<Func<T, IEnumerable<object>>>[] funcs);
	    IEnumerable<T> GetHighest<TComparitor>(Func<T, TComparitor> compareObject);
	    IEnumerable<T> GetHighest<TComparitor>(Func<T, TComparitor> compareObject, params string[] includes);
	    IEnumerable<T> GetHighest<TComparitor>(Func<T, TComparitor> compareObject, params Expression<Func<T, IEnumerable<object>>>[] funcs);
		IEnumerable<T> GetN(int amount);
    }
}