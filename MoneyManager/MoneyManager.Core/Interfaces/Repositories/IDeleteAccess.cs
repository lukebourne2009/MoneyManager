﻿namespace MoneyManager.Core.Interfaces.Repositories
{
    public interface IDeleteAccess
    {
        void Delete(int id);
    }
}
