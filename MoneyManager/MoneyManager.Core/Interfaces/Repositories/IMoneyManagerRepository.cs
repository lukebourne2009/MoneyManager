﻿using MoneyManager.Core.Entities;
using System.Data.Entity;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Entities.Budgets;

namespace MoneyManager.Core.Interfaces.Repositories
{
    public interface IMoneyManagerRepository
    {
        // Database
        DbSet<UserEntity> Users { get; set; }
        DbSet<BudgetEntity> Budgets { get; set; }
        DbSet<BudgetGroupEntity> BudgetGroups { get; set; }
        DbSet<BudgetCategoryEntity> BudgetCategories { get; set; }
        DbSet<AccountEntity> Accounts { get; set; }
        DbSet<TransactionEntity> Transactions { get; set; }
        DbSet<MonthlyCategoryEntity> MonthlyCategories { get; set; }
    }
}
