﻿namespace MoneyManager.Core.Interfaces.Repositories
{
    public interface ISingleAccess<TEntity> : ICreateAccess<TEntity>
    {
        TEntity Get(params string[] includes);
    }
}
