﻿namespace MoneyManager.Core.Interfaces.Factories
{
	public interface IFactory<out TObject>
	{
		TObject Make();
	}

	public interface IFactory<out TObject, in TInput>
    {
        TObject Make(TInput input);
    }
}
