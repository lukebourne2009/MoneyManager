﻿using System.Data.Entity;

namespace MoneyManager.Core.Interfaces
{
    public interface IEntityStateSetter
    {
        void SetState<TEntity>(DbContext repo, TEntity entity, EntityState state)
            where TEntity : class;
    }
}