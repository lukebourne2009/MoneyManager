﻿using System.Collections.Generic;
using MoneyManager.Core.Models.Budgets;

namespace MoneyManager.Core.Interfaces.Service
{
    public interface IBudgetSortingService : IService<IEnumerable<BudgetModel>,IEnumerable<BudgetModel>>
    {}
}
