﻿using System.Collections.Generic;

namespace MoneyManager.Core.Interfaces.Service
{
    public interface IActionUrlCreatorService : IService<string, string, string, IDictionary<string, object>>,
        IService<string, string, string>
    {
        
    }
}