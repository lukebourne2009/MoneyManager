﻿namespace MoneyManager.Core.Interfaces.Service
{
    public interface IService<out TOut, in TIn1, in TIn2, in TIn3>
    {
        TOut Excecute(TIn1 in1, TIn2 in2, TIn3 in3);
    }
    public interface IService<out TOut, in TIn1, in TIn2>
    {
        TOut Excecute(TIn1 in1, TIn2 in2);
    }
    public interface IService<out TOut, in TIn1>
    {
        TOut Excecute(TIn1 in1);
    }
    public interface IService<out TOut>
    {
        TOut Excecute();
    }
}
