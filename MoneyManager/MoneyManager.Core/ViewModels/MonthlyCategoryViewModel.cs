﻿using System.ComponentModel.DataAnnotations;

namespace MoneyManager.Core.ViewModels
{
    public class MonthlyCategoryViewModel
    {
        public int Id { get; set; }
        public int BudgetedValue { get; set; }
        public int BudgetCategoryId { get; set; }
        [Required]
        public string Date { get; set; }
    }
}