﻿namespace MoneyManager.Core.ViewModels.Account
{
    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }
}
