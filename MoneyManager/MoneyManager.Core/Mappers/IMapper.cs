﻿using System.Collections.Generic;

namespace MoneyManager.Core.Mappers
{
    public interface IMapper<out TTarget, in TSource, in TArg>
    {
        TTarget Map(TSource source, TArg arg);
        IEnumerable<TTarget> Map(IEnumerable<TSource> source, TArg arg);
    }
}