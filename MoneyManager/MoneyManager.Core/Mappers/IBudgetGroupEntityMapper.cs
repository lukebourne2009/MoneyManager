﻿using System;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Models.Budgets;

namespace MoneyManager.Core.Mappers
{
    public interface IBudgetGroupEntityMapper : IMapper<BudgetGroupModel, BudgetGroupEntity, DateTime>
    {
        
    }
}