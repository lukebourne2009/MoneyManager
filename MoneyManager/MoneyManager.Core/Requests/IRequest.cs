﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MoneyManager.Core.Models.Budgets;

namespace MoneyManager.Core.Requests
{

	public interface IRequest<TOut, in TIn1>
	{
		Task<TOut> ExecuteAsync(TIn1 in1);
	}
	public interface IRequest<TOut, in TIn1, in TIn2>
	{
		Task<TOut> ExecuteAsync(TIn1 in1, TIn2 in2);
	}
	public interface IRequest<TOut, in TIn1, in TIn2, in TIn3>
	{
		Task<TOut> ExecuteAsync(TIn1 in1, TIn2 in2, TIn3 in3);
	}
}