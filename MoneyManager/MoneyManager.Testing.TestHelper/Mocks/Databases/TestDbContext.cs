﻿using System.Configuration;
using MoneyManager.Data.Database;

namespace MoneyManager.Testing.TestHelper.Mocks.Databases
{
    public class TestDbContext : MoneyManagerDbContext
    {
        public TestDbContext()
            : base(ConfigurationManager.ConnectionStrings["TestConnection"].ConnectionString)
        {}
    }
}
