﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MoneyManager.Common.Factories;
using MoneyManager.Common.Factories.Budgets;
using MoneyManager.Common.Managers.Identity;
using MoneyManager.Common.Mappers;
using MoneyManager.Common.Services.Budgets;
using MoneyManager.Core.Models;
using MoneyManager.Data.Database;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Common.Transformers;
using MoneyManager.Common.Transformers.Budgets;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Models.Accounts;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Data.Repositories;
using MoneyManager.Data.Repositories.Accounts;
using MoneyManager.Testing.TestHelper.Base;
using MoneyManager.Testing.TestHelper.Interfaces;
using MoneyManager.Testing.TestHelper.PropertyHelpers;

namespace MoneyManager.Testing.TestHelper
{
    public class DataGenerationHelper : IDataGenerationHelper
    {
        public IPropertyHelper<ApplicationUser> UserHelper => new PropertyHelper<ApplicationUser, object>();

        public IEnumerable<BudgetModel> CreateBudgets(IEnumerable<BudgetModel> budgets, UserEntity appUser = null)
        {
            var budgetFactory = new BudgetAccessFactory(DbHelper.Repo);
            var budgetEntityToBudgetModel = new BudgetEntityToBudgetModel(new BudgetGroupEntityToBudgetGroupModel(new BudgetCategoryEntityMapper(new TransactionSummingService(), new MonthlyCategoryChooserService())));
            var budgetManFactory = new BudgetManagerFactory(budgetFactory, budgetEntityToBudgetModel, new BudgetModelToBudgetEntity());
            var budgetMan = budgetManFactory.Make(appUser ?? UserHelper.ExpectedModel.User);

            var result = new List<BudgetModel>();
            foreach (var budget in budgets)
            {
                result.Add(budgetMan.Create(budget));
            }
            return result;
        }
        public IEnumerable<BudgetModel> CreateBudgets(int nBudgets, UserEntity appUser = null)
        {
            var budgetFactory = new BudgetAccessFactory(DbHelper.Repo);
            var budgetManFactory = new BudgetManagerFactory(budgetFactory, new BudgetEntityToBudgetModel(new BudgetGroupEntityToBudgetGroupModel(new BudgetCategoryEntityMapper(new TransactionSummingService(), new MonthlyCategoryChooserService()))), new BudgetModelToBudgetEntity());
            var budgetMan = budgetManFactory.Make(appUser ?? UserHelper.ExpectedModel.User);

            var budgets = new List<BudgetModel>(budgetMan.Get());
            if (budgets.Count > nBudgets)
                throw new Exception("Too Many Budgets, Need To Delete some");

            if (budgets.Count < nBudgets)
            {
                for (int i = budgets.Count; i < nBudgets; i++)
                {
                    var model = new BudgetModel
                    {
                        //User = Transform.ToUserModel(appUser ?? UserHelper.AppUser.User),
                        Name = $"My Budget {i}"
                    };
                    budgets.Add(budgetMan.Create(model));
                }
            }

            return budgets;
        }
        public IEnumerable<BudgetGroupModel> CreateBudgetGroups(int nGroups, BudgetModel budget, MoneyManagerDbContext repo)
        {
            var access = new BudgetGroupAccess(budget.Id, repo);
            for (int i = 0; i < nGroups; i++)
            {
                access.Insert(new BudgetGroupEntity
                {
                    Id = i,
                    Name = $"Budget Group {i}"
                });
            }
            return new BudgetGroupEntityToBudgetGroupModel(new BudgetCategoryEntityMapper(new TransactionSummingService(), new MonthlyCategoryChooserService()))
                .Transform(access.GetAll());
        }
        public IEnumerable<BudgetGroupModel> CreateBudgetGroups(IEnumerable<BudgetGroupModel> budgetGroups, BudgetModel budget, MoneyManagerDbContext repo)
        {
            var access = new BudgetGroupAccess(budget.Id, repo);
            foreach (var groupModel in budgetGroups)
            {
                access.Insert(Transform.ToBudgetGroupEntity(groupModel));
            }
            return null;
        }
        public IEnumerable<BudgetCategoryModel> CreateBudgetCategories(int p0, BudgetGroupModel group, MoneyManagerDbContext repo)
        {
            var list = new List<BudgetCategoryEntity>();
            var access = new BudgetCategoryAccess(group.Id, repo);
            for (int i = 0; i < p0; i++)
            {
                list.Add(new BudgetCategoryEntity
                {
                    Id = i,
                    Name = $"Budget Cat {i}",
                    CategoryOrder = i,
                    Transactions = new List<TransactionEntity>()
                });
                access.Insert(list[i]);
            }
            return new BudgetCategoryEntityMapper(new TransactionSummingService(), new MonthlyCategoryChooserService()).Map(list,DateTime.Now);
        }
        public IEnumerable<AccountModel> CreateAccounts(int p0, BudgetModel budget, MoneyManagerDbContext repo)
        {
            var access = new AccountAccess(budget.Id, repo);
            for (int i = 0; i < p0; i++)
            {
                access.Insert(new AccountEntity());
            }
            return Transform.ToAccountModels(access.GetAll());
        }
        public IEnumerable<TransactionModel> CreateTransactions(int p0, AccountModel account, 
            BudgetCategoryModel cat, int value, MoneyManagerDbContext repo)
        {
            var access = new TransactionAccess(account.Id, repo);
            for (int i = 0; i < p0; i++)
            {
                access.Insert(new TransactionEntity
                {
                    CategoryId = cat.Id,
                    Value = value
                });
            }
            return Transform.ToTransactionModels(access.GetAll());
        }
        public IEnumerable<MonthlyCategoryEntity> CreateMonthyCategories(int p0, BudgetCategoryModel cat, MoneyManagerDbContext repo)
        {
            var access = new MonthlyCategoryAccess(cat.Id, repo);
            var list = new List<MonthlyCategoryEntity>();
            for (int i = 0; i < p0; i++)
            {
                list.Add(new MonthlyCategoryEntity
                {
                    Id = i,
                    BudgetedValue = 100,
                    Date = "01-2000"
                });
            }

            return CreateMonthyCategories(list, cat, repo);
        }
        public IEnumerable<MonthlyCategoryEntity> CreateMonthyCategories(IList<MonthlyCategoryEntity> monthlies, BudgetCategoryModel cat, MoneyManagerDbContext repo)
        {
            var access = new MonthlyCategoryAccess(cat.Id, repo);
            foreach (MonthlyCategoryEntity month in monthlies)
            {
                access.Insert(month);
            }
            return access.GetAll();
        }

        protected void AssertNBudgets(int nBudgets, ApplicationUser appUser = null)
        {
            var budgetFactory = new BudgetAccessFactory(DbHelper.Repo);
            var budgetManFactory = new BudgetManagerFactory(budgetFactory, new BudgetEntityToBudgetModel(new BudgetGroupEntityToBudgetGroupModel(new BudgetCategoryEntityMapper(new TransactionSummingService(), new MonthlyCategoryChooserService()))), new BudgetModelToBudgetEntity());
            var budgetMan = budgetManFactory.Make(appUser?.User ?? UserHelper.ExpectedModel.User);

            var count = budgetMan.Get().Count();
            if (count != nBudgets)
                throw new Exception($"You needed {nBudgets} budgets but you have {count}");
        }
        public bool AssertBudgetGroups(List<BudgetGroupModel> expectedGroups, BudgetModel budget)
        {
            return AssertBudgetGroups(expectedGroups, budget, x => !x);
        }
        public bool AssertNotBudgetGroups(List<BudgetGroupModel> expectedGroups, BudgetModel budget)
        {
            return AssertBudgetGroups(expectedGroups, budget, x => x);
        }

        private static bool AssertBudgetGroups(List<BudgetGroupModel> expectedGroups, BudgetModel budget, Func<bool, bool> func)
        {
            var budgetGroupFactory = new BudgetGroupAccessFactory(DbHelper.Repo);
            var budgetGroupManFactory = new BudgetGroupManagerFactory(budgetGroupFactory, new BudgetGroupEntityMapper(new BudgetCategoryEntityMapper(new TransactionSummingService(), new MonthlyCategoryChooserService())), new BudgetGroupModelToBudgetGroupEntity());
            var budgetGroupMan = budgetGroupManFactory.Make(budget.Id);

            var actualGroups = budgetGroupMan.GetBudgetGroups().ToList();
            foreach (var budgetGroupModel in expectedGroups)
            {
                var pass = actualGroups.Any(x => x.Name == budgetGroupModel.Name);
                if (func(pass)) return false;
            }
            return true;
        }

        protected ApplicationUser GetUser(string username)
        {
            var user = new ApplicationUserManager(new UserStore<ApplicationUser>(DbHelper.Repo))
                .FindByEmail(username);
            return user;
        }
        protected IEnumerable<BudgetModel> GetBudgets(ApplicationUser appUser = null)
        {
            var budgetFactory = new BudgetAccessFactory(DbHelper.Repo);
            var budgetManFactory = new BudgetManagerFactory(budgetFactory, new BudgetEntityToBudgetModel(new BudgetGroupEntityToBudgetGroupModel(new BudgetCategoryEntityMapper(new TransactionSummingService(), new MonthlyCategoryChooserService()))), new BudgetModelToBudgetEntity());
            var budgetMan = budgetManFactory.Make(appUser?.User ?? UserHelper.ExpectedModel.User);
            return budgetMan.Get();
        }
    }
}
