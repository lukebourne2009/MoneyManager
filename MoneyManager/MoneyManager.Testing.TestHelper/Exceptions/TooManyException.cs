﻿using System;

namespace MoneyManager.Testing.TestHelper.Exceptions
{
    public class TooManyException : Exception
    {
        public TooManyException(string msg)
            : base(msg)
        {}
    }
}