﻿using System.Collections.Generic;

namespace MoneyManager.Testing.TestHelper.Interfaces
{
    public interface IPropertyHelper : IPropertyHelper<object>
    {
        
    }

    public interface IPropertyHelper<TObject>
        where TObject : new()
    {
        TObject ExpectedModel { get; set; }
        TObject ActualModel { get; set; }
        IList<TObject> ExpectedModels { get; set; }
        IList<TObject> ActualModels { get; set; }
    }

    public interface IParentPropertyHelper<out TParent>
    {
        TParent ParentModel(int i = 0);
    }
}