﻿using System.Collections.Generic;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Data.Database;

namespace MoneyManager.Testing.TestHelper.Interfaces
{
    public interface IDataGenerationHelper
    {
        IEnumerable<BudgetModel> CreateBudgets(IEnumerable<BudgetModel> budgets, UserEntity user = null);
        IEnumerable<BudgetModel> CreateBudgets(int nBudgets, UserEntity user = null);
        IEnumerable<BudgetGroupModel> CreateBudgetGroups(int nGroups, BudgetModel budget, MoneyManagerDbContext repo);
        IEnumerable<BudgetGroupModel> CreateBudgetGroups(IEnumerable<BudgetGroupModel> budgetGroups, BudgetModel budget, MoneyManagerDbContext repo);

    }
}