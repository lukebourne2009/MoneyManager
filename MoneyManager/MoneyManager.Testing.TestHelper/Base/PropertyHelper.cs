﻿using System;
using System.Collections.Generic;
using MoneyManager.Testing.TestHelper.Interfaces;
using TechTalk.SpecFlow;

namespace MoneyManager.Testing.TestHelper.Base
{
    public class PropertyHelper<TObject, TParent> : IPropertyHelper<TObject>, IParentPropertyHelper<TParent> 
        where TObject : new()
    {
        public TParent ParentModel(int i = 0)
        {
            return ScenarioContext.Current.Get<IList<TParent>>("List of" + typeof(TParent) + nameof(ExpectedModels))[i];
        }
        public virtual TObject ExpectedModel
        {
            get {
                try
                {
                    return ExpectedModels[0];
                }
                catch (ArgumentOutOfRangeException)
                {
                    var obj = new TObject();
                    ExpectedModels.Add(obj);
                    return obj;
                }
            }
            set
            {
                try
                {
                    ExpectedModels[0] = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ExpectedModels.Add(value);
                }
            }
        }
        public virtual TObject ActualModel
        {
            get
            {
                try
                {
                    return ActualModels[0];
                }
                catch (ArgumentOutOfRangeException)
                {
                    var obj = new TObject();
                    ActualModels.Add(obj);
                    return obj;
                }
            }
            set {
                try
                {
                    ActualModels[0] = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ActualModels.Add(value);
                }
            }
        }
        public virtual IList<TObject> ExpectedModels
        {
            get
            {
                try
                {
                    return ScenarioContext.Current.Get<IList<TObject>>("List of" + typeof(TObject) + nameof(ExpectedModels));
                }
                catch (KeyNotFoundException)
                {
                    return ExpectedModels = new List<TObject>();
                }
            }
            set { ScenarioContext.Current.Set(value, "List of" + typeof(TObject) + nameof(ExpectedModels)); }
        }
        public virtual IList<TObject> ActualModels
        {
            get
            {
                try
                {
                    return ScenarioContext.Current.Get<IList<TObject>>("List of" + typeof(TObject) + nameof(ActualModels));
                }
                catch (KeyNotFoundException)
                {
                    return ActualModels = new List<TObject>();
                }
            }
            set { ScenarioContext.Current.Set(value, "List of" + typeof(TObject) + nameof(ActualModels)); }
        }
    }

    public class PropertyHelper
    {
        private Type _childType;

        public PropertyHelper(Type childType)
        {
            _childType = childType;
        }

        public object ExpectedModel
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public object ActualModel
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public object ExpectedModels
        {
            get
            {
                object model;
                if (ScenarioContext.Current.TryGetValue("List of" + _childType + nameof(ExpectedModels), out model))
                {
                    return model;
                }
                throw new KeyNotFoundException();
            }
            set { ScenarioContext.Current.Set(value, "List of" + _childType + nameof(ExpectedModels)); }
        }

        public object ActualModels
        {
            get
            {
                object model;
                if (ScenarioContext.Current.TryGetValue("List of" + _childType + nameof(ExpectedModels), out model))
                {
                    return model;
                }
                throw new KeyNotFoundException();
            }
            set { ScenarioContext.Current.Set(value, "List of" + _childType + nameof(ExpectedModels)); }
        }
    }
}