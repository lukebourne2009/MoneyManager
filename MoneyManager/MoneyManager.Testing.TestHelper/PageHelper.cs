﻿using AutomatedTestFramework.Framework.Core;
using AutomatedTestFramework.Framework.Core.ElementGroups;
using TechTalk.SpecFlow;

namespace MoneyManager.Testing.TestHelper
{
    public class PageHelper : DataGenerationHelper
    {
        public IElementQuery QueriedElement
        {
            get { return ScenarioContext.Current.Get<IElementQuery>(nameof(QueriedElement)); }
            set { ScenarioContext.Current.Set(value, nameof(QueriedElement)); }
        }

        public IForm Form
        {
            get { return ScenarioContext.Current.Get<IForm>(nameof(Form)); }
            set { ScenarioContext.Current.Set(value, nameof(Form)); }
        }
    }
}
