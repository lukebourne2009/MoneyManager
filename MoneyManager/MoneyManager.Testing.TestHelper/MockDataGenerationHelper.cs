﻿using System.Collections.Generic;
using System.Linq;
using MoneyManager.Common.Transformers;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Data.Database;
using MoneyManager.Testing.TestHelper.Interfaces;

namespace MoneyManager.Testing.TestHelper
{
    public class MockDataGenerationHelper : IDataGenerationHelper
    {
        public IEnumerable<BudgetModel> CreateBudgets(IEnumerable<BudgetModel> budgets, UserEntity user = null)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<BudgetModel> CreateBudgets(int nBudgets, UserEntity user = null)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<BudgetGroupModel> CreateBudgetGroups(int nGroups, BudgetModel budget, MoneyManagerDbContext repo)
        {
            ICollection<BudgetGroupEntity> groups = new List<BudgetGroupEntity>(nGroups);
            for (int i = 0; i < nGroups; i++)
            {
                groups.Add(new BudgetGroupEntity
                {
                    Id = i,
                    Parent = Transform.ToBudgetEntity(budget),
                    ParentId = budget.Id,
                    Name = $"Budget Group {i}"
                });
            }
            return null;
        }

        public IEnumerable<BudgetGroupModel> CreateBudgetGroups(IEnumerable<BudgetGroupModel> budgetGroups, BudgetModel budget, MoneyManagerDbContext repo)
        {
            ICollection<BudgetGroupModel> groups = new List<BudgetGroupModel>(budgetGroups);
            for (var i = 0; i < groups.Count; i++)
            {
                groups.ElementAt(i).Id = i;
            }
            return groups;
        }
    }
}