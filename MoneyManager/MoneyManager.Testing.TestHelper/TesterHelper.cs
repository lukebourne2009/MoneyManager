﻿using System;
using NUnit.Framework;

namespace MoneyManager.Testing.TestHelper
{
    public class TesterHelper
    {
        public static void ThrowOnNull(TestDelegate func)
        {
            ThrowOn<ArgumentNullException>(func);
        }

        public static void AssertAllProperties<TModel>(TModel expected, TModel actual)
        {
            foreach (var prop in expected.GetType().GetProperties())
            {
                Assert.AreEqual(prop.GetValue(expected), prop.GetValue(actual)); 
            }
        }

        public static void ThrowOn<T>(TestDelegate func)
            where T : Exception
        {
            Assert.Throws<T>(func);
        }
    }
}