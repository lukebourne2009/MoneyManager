﻿using MoneyManager.Common.Managers.Budgets;
using TechTalk.SpecFlow;

namespace MoneyManager.Testing.TestHelper.PropertyHelpers
{
    public class BudgetHelper : MockDbHelper
    {
        protected BudgetManager BudgetMan
        {
            get { return ScenarioContext.Current.Get<BudgetManager>(nameof(BudgetMan)); }
            set { ScenarioContext.Current.Set(value, nameof(BudgetMan)); }
        }
        protected object Result
        {
            get { return ScenarioContext.Current.Get<object>(nameof(Result)); }
            set { ScenarioContext.Current.Set(value, nameof(Result)); }
        }
    }
}
