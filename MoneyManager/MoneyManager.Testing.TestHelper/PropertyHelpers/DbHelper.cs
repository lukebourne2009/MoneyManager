﻿using MoneyManager.Data.Database;
using MoneyManager.Testing.TestHelper.Mocks.Databases;
using TechTalk.SpecFlow;

namespace MoneyManager.Testing.TestHelper.PropertyHelpers
{
    public class DbHelper
    {
        private static MoneyManagerDbContext _repo;

        public static MoneyManagerDbContext Repo
        {
            get
            {
                
                if (ScenarioContext.Current != null)
                {
                    MoneyManagerDbContext repo;
                    if (ScenarioContext.Current.TryGetValue(nameof(Repo), out repo))
                        return repo;
                    else
                        return Repo = new TestDbContext();
                }
                else
                {
                    return _repo ?? (_repo = new TestDbContext());
                }
            }
            set
            {
                if (ScenarioContext.Current != null)
                    ScenarioContext.Current.Set(value, nameof(Repo));
                else
                    _repo = value;
            }
        }
    }
}
