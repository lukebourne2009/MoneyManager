﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Data.Database;
using NSubstitute;
using TechTalk.SpecFlow;

namespace MoneyManager.Testing.TestHelper.PropertyHelpers
{
    public class MockDbHelper
    {
        public static MoneyManagerDbContext Repo
        {
            get
            {
                MoneyManagerDbContext repo;
                if (ScenarioContext.Current.TryGetValue(nameof(Repo), out repo))
                    return repo;
                else
                {
                    ScenarioContext.Current.Set(repo = SetupRepo(Substitute.For<MoneyManagerDbContext>()), nameof(Repo));
                    return repo;
                }
            }
            set { ScenarioContext.Current.Set(value, nameof(Repo)); }
        }

        private static MoneyManagerDbContext SetupRepo(IMoneyManagerRepository repo)
        {
            repo.BudgetCategories = CreateMockSet(new List<BudgetCategoryEntity>());
            repo.BudgetGroups = CreateMockSet(new List<BudgetGroupEntity>());
            repo.Budgets = CreateMockSet(new List<BudgetEntity>());
            repo.Users = CreateMockSet(new List<UserEntity>());
            repo.Accounts = CreateMockSet(new List<AccountEntity>());
            repo.Transactions = CreateMockSet(new List<TransactionEntity>());
            return repo as MoneyManagerDbContext;
        }

        public static DbSet<T> CreateMockSet<T>(ICollection<T> data)
            where T : class
        {
            var query = data.AsQueryable();
            var mockSet = Substitute.For<DbSet<T>, IQueryable<T>>();

            // setup all IQueryable methods using what you have from "data"
            ((IQueryable<T>)mockSet).Provider.Returns(query.Provider);
            ((IQueryable<T>)mockSet).Expression.Returns(query.Expression);
            ((IQueryable<T>)mockSet).ElementType.Returns(query.ElementType);
            ((IQueryable<T>)mockSet).GetEnumerator().Returns(info => data.GetEnumerator());
            mockSet.Add(Arg.Any<T>())
                .Returns(info => info.Arg<T>())
                .AndDoes(
                    info =>
                    {
                        data.Add(info.Arg<T>());
                    }
                );
            mockSet.Include(Arg.Any<string>()).Returns(info => mockSet.AsQueryable());

            return mockSet;
        }
    }
}
