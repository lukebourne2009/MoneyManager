﻿using System.Collections.Specialized;
using System.Globalization;
using System.Web.Mvc;
using MoneyManager.Data.Database;
using MoneyManager.Testing.TestHelper.Mocks.Databases;

namespace MoneyManager.Testing.TestHelper
{
    public class ControllerHelper
    {
        public static void AttachModelStateToController<TObject>(TObject model, Controller controller)
        {
            var modelBinder = new ModelBindingContext()
            {
                ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(
                    () => model, model.GetType()),
                ValueProvider = new NameValueCollectionValueProvider(
                    new NameValueCollection(), CultureInfo.InvariantCulture)
            };
            var binder = new DefaultModelBinder().BindModel(
                new ControllerContext(), modelBinder);
            controller.ModelState.Clear();
            controller.ModelState.Merge(modelBinder.ModelState);
        }

        public static TObject GetJsonData<TObject>(JsonResult result, string prop)
        {
            return (TObject)result.Data.GetType()
                .GetProperty(prop)
                .GetValue(result.Data, null);
        }


        public static MoneyManagerDbContext ReloadDb(MoneyManagerDbContext repo)
        {
            repo.Dispose();
            return repo = new TestDbContext();
        }
    }
}