﻿using System;
using System.Web;
using System.Web.Mvc;

namespace MoneyManager.Web
{
    public static class MyHelpers
    {
        public static HtmlString ModalButton(this HtmlHelper helper, string target, string id = null, string @class = null)
        {
            return new HtmlString(string.Format("<div data-toggle='modal' data-target={0}>" +
                                                "<i {1} class='button {2}' aria-hidden='true'></i>" +
                                                "</div>", target, id == null ? "" : string.Format("id='{0}'", id), @class));
        }
    }
}