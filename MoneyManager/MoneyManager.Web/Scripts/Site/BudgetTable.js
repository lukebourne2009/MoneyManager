﻿/// <reference path="~/Scripts/knockout-3.4.2.js" />
/// <reference path="~/Scripts/Site/Validation/Validate.js" />

var budgetTable = {
    groupRows: ko.observableArray([]),
    addGroup: function (data) {
        this.groupRows.push(data);
    },
    selectAll: function () {
        var selected = $("#budget-table thead input[type='checkbox']").is(":checked");

        for (var i = 0; i < this.groupRows().length; i++) {
            var row = this.groupRows()[i];
            row.select(selected);
            row.selectCategories(selected);
        }
    }
}

$("#edit-budget-menu form button.submit").on('click', function (){
    var $form = $(this).closest("form");
    if(Validate($form))
    {
        $.ajax({
            url: $form.get(0).action,
            type: "PUT",
            data: $form.serialize(),
            success: function(result) {
                $("#budget-name").text(result.Name);
            }
        });
        $("#budget-edit").dropdown("toggle");
    }
});

$("#edit-budget-menu form button.cancel").on('click', function () {
    var $form = $(this).closest("form");
    ResetFormValidation($form);
});

$("#delete-budget-form form button.submit").on('click', function () {
    var $form = $(this).closest("form");
    if (Validate($form)) {
        $.ajax({
            url: $form.get(0).action,
            type: "Delete",
            data: $form.serialize(),
            success: function (result) {
                window.location.href = result.returnUrl;
            }
        });
    }
});

$.ajax({
    url: $("#budget-table").data("group-get-url"),
    type: "GET",
    data: { budgetId: $("#budget-table").data("budget-id") },
    success: function (data) {
        var rows = [];
        for (var i = 0; i < data.length; i++) {
            rows.push(new GroupRow(data[i]));
        }

        budgetTable.groupRows(rows);
    }
});

ko.applyBindings(budgetTable, $("#budget-table")[0]);