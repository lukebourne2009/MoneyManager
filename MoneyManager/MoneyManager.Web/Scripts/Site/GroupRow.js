﻿/// <reference path="~/Scripts/Site/site-lib.js"/>
/// <reference path="~/Scripts/knockout-3.4.2.js" />
/// <reference path="~/Scripts/Site/BudgetTable.js"/>
/// <reference path="~/Scripts/Site/Validation/Validate.js"/>

//var GroupRow = function (data) {
//    var self = this;

//    self.select = ko.observable(false);
//    self.dropperDown = ko.observable(true);
//    self.toggleDropper = function () {
//        self.dropperDown(!self.dropperDown());
//        var visible = self.dropperDown();

//        for (var i = 0; i < self.categoryRows().length; i++) {
//            self.categoryRows()[i].isVisible(visible);
//        }
//    }
//    self.selectCategories = function (selected) {

//        if (selected === undefined)
//            selected = self.select();

//        for (var i = 0; i < self.categoryRows().length; i++) {
//            var row = self.categoryRows()[i];
//            row.select(selected);
//        }
//    }
//    self.addCategory = function(data) {
//        self.categoryRows.push(data);
//    }

//    // data
//    self.name = ko.observable(data.Name);
//    self.categoryRows = ko.observableArray([]);
//    self.id = data.Id;

//    var rows = [];
//    for (var i = 0; i < data.BudgetCategories.length; i++) {
//        rows.push(new CategoryRow(data.BudgetCategories[i]));
//    }

//    self.categoryRows(rows);
//};
var GroupRow = class {
    constructor(data) {
        this.select = ko.observable(false);
        this.dropperDown = ko.observable(true);
        this.toggleDropper = function () {
            this.dropperDown(!this.dropperDown());
            var visible = this.dropperDown();

            for (var i = 0; i < this.categoryRows().length; i++) {
                this.categoryRows()[i].isVisible(visible);
            }
        }
        this.selectCategories = function (selected) {

            if (selected === undefined)
                selected = this.select();

            for (var i = 0; i < this.categoryRows().length; i++) {
                var row = this.categoryRows()[i];
                row.select(selected);
            }
        }

        // data
        this.name = ko.observable(data.Name);
        this.categoryRows = ko.observableArray([]);
        this.id = data.Id;

        var rows = [];
        for (var i = 0; i < data.BudgetCategories.length; i++) {
            rows.push(new CategoryRow(data.BudgetCategories[i]));
        }

        this.categoryRows(rows);
    }

    addCategory(data) {
        this.categoryRows.push(data);
    }
};