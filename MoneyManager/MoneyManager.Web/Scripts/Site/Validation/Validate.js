﻿
var Validate;
var ResetFormValidation;

$(function () {

    var validateSingle = function (dataVal, selector, check) {
        var $input = $(selector).find("input.form-control");
        var inputName = $input.attr("name");
        var required = $input.attr(dataVal);
        if (required) {
            var text = $input.val();
            if (check($input)) {

                var valMessage = $(selector).find(`[data-valmsg-for='${inputName}']`);
                // Change validation class
                valMessage.removeClass("field-validation-valid")
                    .addClass("field-validation-error");

                // Add Message
                valMessage.text(required);
                return false;
            }
        }
        return true;
    }

    Validate = function (selector) {
        var result = true;

        if (!validateSingle("data-val-required", selector, function (text) {
            return text.val().length === 0;
        })) result = false;
        if (!validateSingle("data-val-length", selector, function (text) {
            return text.val().length > text.attr("data-val-length-max");
        })) result = false;
        return result;
    }

    ResetFormValidation = function (selector) {
        var form = $(selector);

        // Field resets
        form.find('input').removeClass('input-validation-error')
            .addClass('input-validation-valid').attr("value", "");
        form.find('.field-validation-error').text('').removeClass('field-validation-error')
            .addClass('field-validation-valid');

        // Summary reset
        form.find(".validation-summary-errors")
            .removeClass("validation-summary-errors")
            .addClass("validation-summary-valid")
            .find("li")
            .each(function () {
                $(this).text("").css("display", "none");
            });
    }
});