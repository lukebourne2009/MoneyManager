﻿/// <reference path="~/Scripts/Site/site-lib.js"/>
/// <reference path="~/Scripts/knockout-3.4.2.js" />
/// <reference path="~/Scripts/Site/BudgetTable.js"/>
/// <reference path="~/Scripts/Site/Validation/Validate.js"/>

//$(function () {

    var groupForm = {
        groupName: ko.observable(""),
        isVisible: ko.observable(false),
        formScale: ko.observable(0),
        submitForm: function () {
            Validate("#group-create-form",
                function () {
                    $.ajax({
                        url: $("#group-create-form form").attr("action"),
                        type: "post",
                        data: $("#group-create-form form").serialize(),
                        success: function (data) {
                            budgetTable.addGroup(new GroupRow(data));
                            groupForm.cancelForm();
                        }
                    });
                });
        },
        cancelForm: function (data, event) {
            groupFormButton.toggleGroupForm(data, event);
        }
    }
    var groupFormButton = {
        toggleGroupForm: function (data, event) {

            var closeForm = function() {
                groupForm.isVisible(false);
                groupForm.formScale(0);
                groupForm.groupName("");

                ResetFormValidation("#group-create-form");
                $(document).off(".groupForm");
            }
            var openForm = function () {
                groupForm.isVisible(true);
                groupForm.formScale(1);

                $(document).on("click.groupForm", function (event) {
                    if (ClickIsOutsideElement(event, "#group-create-form")) {
                        closeForm();
                    }
                });
                event.stopPropagation();
            }

            if (groupForm.isVisible()) {
                closeForm();
            } else {
                openForm();
            }
        }
    }

    ko.applyBindings(groupFormButton, $("#group-create")[0]);
    ko.applyBindings(groupForm, $("#group-create-form-hider")[0]);
//});