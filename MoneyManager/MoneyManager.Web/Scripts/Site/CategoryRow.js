﻿/// <reference path="~/Scripts/Site/site-lib.js"/>
/// <reference path="~/Scripts/knockout-3.4.2.js" />
/// <reference path="~/Scripts/Site/BudgetTable.js"/>

var CategoryRow = function(data) {
    var self = this;

    self.select = ko.observable(true);
    self.isVisible = ko.observable(true);
    self.hovering = ko.observable(false);
    self.focusing = ko.observable(false);
    self.budgetedInputVisible = ko.observable(false);
    self.signValue = ko.pureComputed(function() {
        if (data.Available > 0)
            return "positive";
        else if (data.Available < 0)
            return "negative";
        else
            return "neutral";
    }, self);
    self.setHovering = function(hovering) {
        self.hovering(hovering);
    }
    self.setbudgetedInputVisible = function (event, visible) {
        var target;
        if ($(event.target).is(".column-budgeted input"))
            target = $(event.target).get(0);
        else
            target = $(event.target).children(".column-budgeted input").get(0);

        if (target === document.activeElement && !!(target.type || target.href || ~target.tabIndex)) {
            self.budgetedInputVisible(true);
            $(target).focusout(function() {
                self.budgetedInputVisible(false);
            });
            return;
        }
        self.budgetedInputVisible(visible);
    }
    self.focus = function(event, focusing) {
        self.focusing(focusing);
    }

    // Data
    {
        self.name = data.Name;

        if (data.Available < 0)
            self.percentageFill = "100%";
        else
            self.percentageFill = data.PercentageAvailable + "%";

        var budgetedVal = "£" +
            (data.BudgetedValue / 100).toString() +
            "." +
            (data.BudgetedValue % 100).toString() +
            ((data.BudgetedValue % 100) < 9 ? "0" : "");
        self.budgetedValue = budgetedVal;

        var activityVal = "£" +
            (data.ActivityValue / 100).toString() +
            "." +
            (data.ActivityValue % 100).toString() +
            ((data.ActivityValue % 100) < 9 ? "0" : "");
        self.activityValue = activityVal;

        var availableVal = "£" +
            (data.Available / 100).toString() +
            "." +
            (data.Available % 100).toString() +
            ((data.Available % 100) < 9 ? "0" : "");
        self.availableValue = availableVal;
    }
}