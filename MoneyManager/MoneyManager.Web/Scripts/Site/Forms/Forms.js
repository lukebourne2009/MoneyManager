﻿
function AttachAjaxFormCalls() {
    $("form.ajax-form button.submit").on("click",
        function() {
            var $form = $(this).closest("form");
            if (Validate($form)) {
                $.ajax({
                    url: $form.attr("action"),
                    type: $form.attr("method"),
                    data: $form.serialize(),
                    success: function(result) {
                        if ($.type(result.returnUrl) === "string")
                        {
                            window.location.href = result.returnUrl;
                            return;
                        }
                        else if ($.type(result.view) === "string")
                        {
                            if ($.type(result.attachInsideOf) === "string")
                                $(result.attachInsideOf).append(result.view);
                            else if ($.type(result.attachAfter) === "string")
                                $(result.attachAfter).after(result.view);

                        }
                    },
                    error: function (request, status, error) {
                        alert(request.responseText);
                    }
                });
            }
        });
}

setTimeout(AttachAjaxFormCalls, 250);