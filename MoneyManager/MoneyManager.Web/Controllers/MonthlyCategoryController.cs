﻿using System.Web.Mvc;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.ViewModels;

namespace MoneyManager.Web.Controllers
{
    public class MonthlyCategoryController : Controller
    {
        private readonly IFactory<IManager<MonthlyCategoryViewModel>, int> _monthlyManagerFactory;

        public MonthlyCategoryController(IFactory<IManager<MonthlyCategoryViewModel>, int> monthlyManagerFactory)
        {
            _monthlyManagerFactory = monthlyManagerFactory;
        }

        public ActionResult Create(MonthlyCategoryViewModel viewModel)
        {
            viewModel = _monthlyManagerFactory.Make(viewModel.BudgetCategoryId).Create(viewModel);
            return Json(viewModel);
        }
    }
}