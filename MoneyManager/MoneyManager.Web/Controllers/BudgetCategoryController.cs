﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Requests;

namespace MoneyManager.Web.Controllers
{
    public class BudgetCategoryController : Controller
    {
	    private readonly IRequest<BudgetCategoryModel, BudgetCategoryModel> _createRequest;

        public BudgetCategoryController(IRequest<BudgetCategoryModel, BudgetCategoryModel> createRequest)
        {
	        _createRequest = createRequest;
        }

        [HttpPost]
        public async Task<ActionResult> Create(BudgetCategoryModel model)
        {
	        if (!ModelState.IsValid)
		        return PartialView("../Budget/Pop-up Forms/CreateCategoryForm");

            model = await _createRequest.ExecuteAsync(model);

	        return Json(new
	        {
		        attachAfter = $"tr.budget-group-row[data-group-id='{model.BudgetGroupId}']",
		        view = this.RenderPartialView("../Budget/BudgetTable/CategoryRow", model)
	        });
		}
    }
}