﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MoneyManager.Common.Managers.Identity;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Interfaces.Service;
using MoneyManager.Core.Models;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Requests;

namespace MoneyManager.Web.Controllers
{
    [Authorize]
    public class BudgetController : Controller
    {
	    private readonly IRequest<BudgetModel, int?, Task<ApplicationUser>> _getRequest;
	    private readonly IRequest<BudgetModel, BudgetModel, Task<ApplicationUser>> _createRequest;
	    private readonly IRequest<IEnumerable<BudgetModel>, int, Task<ApplicationUser>> _openRequest;
	    private readonly IRequest<bool, int, Task<ApplicationUser>> _deleteRequest;
	    private readonly IRequest<BudgetModel, int, string, Task<ApplicationUser>> _updateNameRequest;
	    private UserManager<ApplicationUser> _userManager;

        public BudgetController(
	        IRequest<BudgetModel, int?, Task<ApplicationUser>> getRequest,
	        IRequest<BudgetModel, BudgetModel, Task<ApplicationUser>> createRequest,
			IRequest<IEnumerable<BudgetModel>, int, Task<ApplicationUser>> openRequest,
	        IRequest<bool, int, Task<ApplicationUser>> deleteRequest,
			IRequest<BudgetModel, int, string, Task<ApplicationUser>> updateNameRequest)
            : this(getRequest, createRequest, openRequest, deleteRequest, updateNameRequest, null)
        {}
        public BudgetController(
			IRequest<BudgetModel, int?, Task<ApplicationUser>> getRequest,
			IRequest<BudgetModel, BudgetModel, Task<ApplicationUser>> createRequest,
			IRequest<IEnumerable<BudgetModel>, int, Task<ApplicationUser>> openRequest,
	        IRequest<bool, int, Task<ApplicationUser>> deleteRequest,
			IRequest<BudgetModel, int, string, Task<ApplicationUser>> updateNameRequest,
			UserManager<ApplicationUser> userManager)
        {
	        _getRequest = getRequest;
	        _createRequest = createRequest;
	        _openRequest = openRequest;
	        _deleteRequest = deleteRequest;
	        _updateNameRequest = updateNameRequest;
	        _userManager = userManager;
        }

	    private UserManager<ApplicationUser> UserManager => 
			_userManager ?? (_userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>());

	    [HttpGet]
        public async Task<ActionResult> Index(int? id = null)
        {
            if (User == null)
                return RedirectToAction("Login", "Account");

            var budget = await _getRequest.ExecuteAsync(id, AppUser);

			if (budget == null)
                return RedirectToAction("Open");

			return View(budget);
        }

		[HttpPost]
        public async Task<ActionResult> Create(BudgetModel budgetModel)
        {
            if (User == null)
                return RedirectToAction("Login", "Account");

            if (!ModelState.IsValid)
                return PartialView("Forms/CreateBudgetForm");

            budgetModel = await _createRequest.ExecuteAsync(budgetModel, AppUser);

			return RedirectToAction("Index", new { id = budgetModel.Id });
        }

        [HttpGet]
        public async Task<ActionResult> Open()
        {
	        var budgets = await _openRequest.ExecuteAsync(10, AppUser);

			return View(budgets);
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
	        await _deleteRequest.ExecuteAsync(id, AppUser);
			return Json(new { returnUrl = Url.Action("Open") });
        }

        [HttpPut]
        public async Task<ActionResult> UpdateName(int id, string name)
        {
            if (User == null)
                return RedirectToAction("Login", "Account");

            if (string.IsNullOrEmpty(name))
				return PartialView("Pop-up Forms/EditBudgetNameForm");

	        var resultModel = await _updateNameRequest.ExecuteAsync(id, name, AppUser);

			return Json(new {returnUrl = Url.Action("Index", new {id = resultModel.Id})});
        }

        private Task<ApplicationUser> AppUser
        {
            get
            {
                if (User == null) return Task.FromResult<ApplicationUser>(null);
                return UserManager.FindByNameAsync(User.Identity.Name);
            }
        }
    }
}
