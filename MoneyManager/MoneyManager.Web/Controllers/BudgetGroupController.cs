﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Requests;

namespace MoneyManager.Web.Controllers
{
    [Authorize]
    public class BudgetGroupController : Controller
    {
	    private readonly IRequest<BudgetGroupModel, BudgetGroupModel> _createRequest;

        public BudgetGroupController(IRequest<BudgetGroupModel, BudgetGroupModel> createRequest)
        {
	        _createRequest = createRequest;
        }

        [HttpPost]
        public async Task<ActionResult> Create(BudgetGroupModel groupModel)
        {
            if(!ModelState.IsValid)
				return PartialView("Error");

            groupModel = await _createRequest.ExecuteAsync(groupModel);

			return Json(new
			{
				attachInsideOf = "#budget-table > table > tbody",
				view = this.RenderPartialView("../Budget/BudgetTable/GroupRow", groupModel)
			});
        }
	}
}