using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using MoneyManager.Common;
using MoneyManager.Common.Factories;
using MoneyManager.Common.Factories.Budgets;
using MoneyManager.Common.Mappers;
using MoneyManager.Common.Requests;
using MoneyManager.Common.Requests.BudgetCategoryRequests;
using MoneyManager.Common.Requests.BudgetGroupRequests;
using MoneyManager.Common.Services.Budgets;
using MoneyManager.Common.Transformers.Budgets;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Interfaces.Service;
using MoneyManager.Core.Mappers;
using MoneyManager.Core.Models;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Requests;
using MoneyManager.Core.Transformers;
using MoneyManager.Data.Database;
using MoneyManager.Web;
using Ninject;
using Ninject.Extensions.Factory;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace MoneyManager.Web
{
	public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();
	    public static StandardKernel Kernel;
        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
	        var kernal = CreateKernel();
			bootstrapper.Initialize(() => kernal);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        public static IKernel CreateKernel()
        {
            Kernel = new StandardKernel();
            try
            {
                Kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                Kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(Kernel);
                return Kernel;
            }
            catch
            {
                Kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
			// Db
	        kernel.Bind<DbContext, MoneyManagerDbContext>().To<MoneyManagerDbContext>().InRequestScope();

			// Requests
	        kernel.Bind<IRequest<BudgetModel, int?, Task<ApplicationUser>>>().To<BudgetGetRequest>();
	        kernel.Bind<IRequest<BudgetModel, BudgetModel, Task<ApplicationUser>>>().To<BudgetCreateRequest>();
	        kernel.Bind<IRequest<IEnumerable<BudgetModel>, int, Task<ApplicationUser>>>().To<BudgetOpenRequest>();
	        kernel.Bind<IRequest<bool, int, Task<ApplicationUser>>>().To<BudgetDeleteRequest>();
	        kernel.Bind<IRequest<BudgetModel, int, string, Task<ApplicationUser>>>().To<BudgetUpdateNameRequest>();
	        kernel.Bind<IRequest<BudgetGroupModel, BudgetGroupModel>>().To<GroupCreateRequest>();
	        kernel.Bind<IRequest<BudgetCategoryModel, BudgetCategoryModel>>().To<CategoryCreateRequest>();

			// Services
			kernel.Bind<IService<IEnumerable<BudgetModel>, IEnumerable<BudgetModel>>>().To<BudgetLastUsedSortingService>();
			kernel.Bind<IService<int, IEnumerable<TransactionEntity>>>().To<TransactionSummingService>();
			kernel.Bind<IService<Func<MonthlyCategoryEntity, bool>, DateTime>>().To<MonthlyCategoryChooserService>();
			
			// Manager Factories
			kernel.Bind<IFactory<IBudgetManager, UserEntity>>().To<BudgetManagerFactory>();
			kernel.Bind<IFactory<IBudgetGroupManager, int>>().To<BudgetGroupManagerFactory>();
			kernel.Bind<IFactory<IBudgetCategoryManager, int>>().To<BudgetCategoryManagerFactory>();
			
			// Transformers
			kernel.Bind<ITransformer<BudgetModel, BudgetEntity>>().To<BudgetEntityToBudgetModel>();
			kernel.Bind<ITransformer<BudgetGroupModel, BudgetGroupEntity>>().To<BudgetGroupEntityToBudgetGroupModel>();
			kernel.Bind<ITransformer<BudgetCategoryEntity, BudgetCategoryModel>>().To<BudgetCategoryModelToBudgetCategoryEntity>();
			kernel.Bind<ITransformer<BudgetEntity, BudgetModel>>().To<BudgetModelToBudgetEntity>();
			kernel.Bind<ITransformer<BudgetGroupEntity, BudgetGroupModel>>().To<BudgetGroupModelToBudgetGroupEntity>();
			
			// Mappers
			kernel.Bind<IMapper<BudgetCategoryModel, BudgetCategoryEntity, DateTime>>().To<BudgetCategoryEntityMapper>();
			kernel.Bind<IMapper<BudgetGroupModel, BudgetGroupEntity, DateTime>>().To<BudgetGroupEntityMapper>();
			
			// Access Factories
			kernel.Bind<IFactory<IAccess<BudgetEntity>, UserEntity>>().To<BudgetAccessFactory>();
			kernel.Bind<IFactory<IAccess<BudgetGroupEntity>, int>>().To<BudgetGroupAccessFactory>();
			kernel.Bind<IFactory<IAccess<BudgetCategoryEntity>, int>>().To<BudgetCategoryAccessFactory>();
			kernel.Bind<IFactory<ISingleAccess<UserEntity>, ApplicationUser>>().To<UserAccessFactory>();
        }
    }
}
