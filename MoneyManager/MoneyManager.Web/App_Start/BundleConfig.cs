﻿using System.Web.Optimization;

namespace MoneyManager.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery")
						.Include("~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval")
						.Include("~/Scripts/jquery.validate.js")
						.Include("~/Scripts/jquery.validate.unobtrusive.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/Content/knockout").Include(
                "~/Scripts/knockout-3.4.2.js"));

            bundles.Add(new ScriptBundle("~/Content/js")
                .Include("~/Scripts/Site/GroupRow.js")
                .IncludeDirectory("~/Scripts/Site","*.js")
                .IncludeDirectory("~/Scripts/Site/Validation", "*.js")
                .IncludeDirectory("~/Scripts/Site/Forms", "*.js"));

            bundles.Add(new StyleBundle("~/Bootstrap").Include(
                "~/Content/bootstrap.css",
                "~/Content/bootstrap-theme.css"));

            bundles.Add(new StyleBundle("~/BudgetIndex").Include(
                "~/Content/CSS/BudgetIndex.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/site.css",
                      "~/Content/CSS/font-awesome/css/font-awesome.css"));
        }
    }
}
