﻿using System.Web.Http;
using System.Web.Mvc;
using Microsoft.Owin;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi;
using Ninject.Web.WebApi.OwinHost;
using Owin;

[assembly: OwinStartupAttribute(typeof(MoneyManager.Web.Startup))]
namespace MoneyManager.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

	        var config = new HttpConfiguration
	        {
		        DependencyResolver = new NinjectDependencyResolver(NinjectWebCommon.Kernel)
	        };
	        config.MapHttpAttributeRoutes();
        }
    }
}
