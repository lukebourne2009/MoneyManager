﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoneyManager.Common.Mappers;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Service;
using MoneyManager.Core.Models.Budgets;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Mappers
{
    [TestFixture]
    public class BudgetCategoryEntityMapperTests : BaseTests<BudgetCategoryEntityMapper>
    {
        [Test]
		[TestCaseSource(nameof(Map_GivenAValidModel_ValuesAreMappedCorrectly_TestData))]
        public void Map_GivenAValidModel_ValuesAreMappedCorrectly(BudgetCategoryEntity entity, Action<BudgetCategoryModel> assertion)
        {
            // Arrange

            // Act
            var result = Sut.Map(entity, DateTime.Now);

			// Assert
	        assertion(result);

        }
	    private static IEnumerable Map_GivenAValidModel_ValuesAreMappedCorrectly_TestData()
	    {
			var entity = new BudgetCategoryEntity
			{
				CategoryOrder = 1,
				Id = 2,
				Name = "Name",
				ParentId = 3
			};
			yield return new TestCaseData(entity, new Action<BudgetCategoryModel>(x => Assert.AreEqual(entity.CategoryOrder, x.CategoryOrder)))
			{
				TestName = "Map: CategoryOrder Property Is Mapped Correctly"
			};
			yield return new TestCaseData(entity, new Action<BudgetCategoryModel>(x => Assert.AreEqual(entity.Id, x.Id)))
			{
				TestName = "Map: Id Property Is Mapped Correctly"
			};
			yield return new TestCaseData(entity, new Action<BudgetCategoryModel>(x => Assert.AreEqual(entity.Name, x.Name)))
			{
				TestName = "Map: Name Property Is Mapped Correctly"
			};
			yield return new TestCaseData(entity, new Action<BudgetCategoryModel>(x => Assert.AreEqual(entity.ParentId, x.BudgetGroupId)))
			{
				TestName = "Map: BudgetGroupId Property Is Mapped Correctly"
			};
		}

		[Test]
        public void Map_GivenASpecifiedDate_AndNoMonthsAssociatedWithCat_ReturnZeroBudgetedValue()
        {
            // Arrange
            var entity = new BudgetCategoryEntity
            {
                MonthlyCategories = new List<MonthlyCategoryEntity>
                {
                    new MonthlyCategoryEntity
                    {
                        BudgetedValue = 500,
                        Date = "02-2000"
                    }
                }
            };

            // Act
            var result = Sut.Map(entity, new DateTime(2000, 1, 1));

            // Assert
            Assert.AreEqual(0, result.BudgetedValue);
        }

        [TestCase(2000, 1000, ExpectedResult = 3000)]
        [TestCase(0, 1000, ExpectedResult = 1000)]
        [TestCase(-2000, 1000, ExpectedResult = -1000)]
        public int Map_GivenAListOfTransactions_AvailableIsTheSumOfActivityAndBudgeted(int activity, int budgeted)
        {
            // Arrange
            Mock<IService<int, IEnumerable<TransactionEntity>>>().Excecute(Arg.Any<IEnumerable<TransactionEntity>>()).Returns(activity);
            Mock<IService<Func<MonthlyCategoryEntity, bool>, DateTime>>().Excecute(Arg.Is<DateTime>(x => x == new DateTime(2000, 1, 1)))
                .Returns(x => true);
            var entity = new BudgetCategoryEntity
            {
                MonthlyCategories = new List<MonthlyCategoryEntity>
                {
                    new MonthlyCategoryEntity
                    {
                        BudgetedValue = budgeted,
                        Date = "01-2000"
                    }
                },
                Transactions = new List<TransactionEntity>()
            };

            // Act
            var result = Sut.Map(entity, new DateTime(2000, 1, 1));

            // Assert
            return result.Available;
        }
        [Test]
        public void Map_GivenNoTransactions_ReturnZero()
        {
            // Arrange
            var entity = new BudgetCategoryEntity();

            // Act
            var result = Sut.Map(entity, new DateTime(2000, 1, 1));

            // Assert
            Assert.AreEqual(0, result.ActivityValue);
        }

        [TestCase(0, 1000, ExpectedResult = 100)]
        [TestCase(1000, 1000, ExpectedResult = 100)]
        [TestCase(-1000, 1000, ExpectedResult = 0)]
        [TestCase(-2000, 1000, ExpectedResult = 0)]
        [TestCase(-500, 1000, ExpectedResult = 50)]
        [TestCase(-250, 1000, ExpectedResult = 75)]
        public int Transform_PercentageAvailableIsThePercentageOfBudgetedAndCashFlow(int activity, int budgeted)
        {
            // Arrange
	        Mock<IService<int, IEnumerable<TransactionEntity>>>().Excecute(Arg.Any<IEnumerable<TransactionEntity>>()).Returns(activity);
            Mock<IService<Func<MonthlyCategoryEntity, bool>, DateTime>>().Excecute(Arg.Is<DateTime>(x => x.CompareTo(new DateTime(2000, 1, 1)) == 0))
                .Returns(x => true);
            var entity = new BudgetCategoryEntity
            {
                MonthlyCategories = new List<MonthlyCategoryEntity>
                {
                    new MonthlyCategoryEntity
                    {
                        BudgetedValue = budgeted,
                        Date = "01-2000"
                    }
                },
                Transactions = new List<TransactionEntity>()
            };

            // Act
            var result = Sut.Map(entity, new DateTime(2000, 1, 1));

            // Assert
            return result.PercentageAvailable;
        }
    }
}