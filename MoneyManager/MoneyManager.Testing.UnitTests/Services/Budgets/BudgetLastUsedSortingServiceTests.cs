﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoneyManager.Common.Services.Budgets;
using MoneyManager.Core.Models.Budgets;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Services.Budgets
{
	[TestFixture]
    public class BudgetLastUsedSortingServiceTests : BaseTests<BudgetLastUsedSortingService>
    {
        [Test]
        public void Excecute_GivenNoBudgets_ReturnZeroLengthList()
        {
            // Arrange
            IEnumerable<BudgetModel> budgets = new List<BudgetModel>();

            // Act
            budgets = Sut.Excecute(budgets);

            // Assert
            Assert.AreEqual(0,budgets.Count());
        }

        [Test]
        public void Excecute_GivenOneBudget_ReturnOneLengthList()
        {
            // Arrange
            IEnumerable<BudgetModel> budgets = new List<BudgetModel>
            {
                new BudgetModel()
            };

            // Act
            budgets = Sut.Excecute(budgets);

            // Assert
            Assert.AreEqual(1, budgets.Count());
        }

        [Test]
        public void Excecute_GivenTwoBudgets_ReturnInOrderOfLastUsedTimeMostRecentFirst()
        {
            // Arrange
            var budgets = new List<BudgetModel>
            {
                new BudgetModel { LastUsed = DateTime.Now-TimeSpan.FromDays(3) },
                new BudgetModel { LastUsed = DateTime.Now }
            };

            // Act
            var result = Sut.Excecute(budgets).ToList();

            // Assert
            Assert.AreEqual(budgets[1].LastUsed, result[0].LastUsed);
            Assert.AreEqual(budgets[0].LastUsed, result[1].LastUsed);
        }

    }
}
