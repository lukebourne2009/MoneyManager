﻿using System;
using MoneyManager.Common.Services.Budgets;
using MoneyManager.Core.Entities.Budgets;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Services.Budgets
{
    [TestFixture]
    public class MonthlyCategoryChooserServiceTests : BaseTests<MonthlyCategoryChooserService>
    {
        [Test]
        public void Excecute_TheDateParameterIsEvaluatedAgainstPassedInMonthlyCategoryDate()
        {
            // Arrange

            // Act
            var result = Sut.Excecute(new DateTime(2000, 1, 1));

            // Assert
            Assert.IsTrue(result.Invoke(new MonthlyCategoryEntity { Date = "01-2000" }));
            Assert.IsFalse(result.Invoke(new MonthlyCategoryEntity { Date = "01-1999" }));
        }
    }
}