﻿using System.Collections.Generic;
using MoneyManager.Common.Services.Budgets;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Interfaces.Service;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Services
{
    [TestFixture]
    public class TransactionSummingServiceTests : BaseTests<TransactionSummingService>
    {
        [Test]
        public void Execute_GivenAnEmptyList_Return0()
        {
            // Arrange
            IService<int, IEnumerable<TransactionEntity>> tranService = new TransactionSummingService();

            // Act
            int result = tranService.Excecute(new List<TransactionEntity>());

            // Assert
            Assert.That(result, Is.Zero);
        }

        [TestCase(new[] { 200 }, ExpectedResult = 200)]
        [TestCase(new[] { 200,500 }, ExpectedResult = 700)]
        [TestCase(new[] { 0, 500, -300 }, ExpectedResult = 200)]
        public int Execute_GivenAList_SumContentsAndReturnResult(int[] values)
        {
            // Arrange
            var transactions = new List<TransactionEntity>();
            foreach (var value in values)
            {
                transactions.Add(new TransactionEntity
                {
                    Value = value
                });
            }

            // Act
            int result = Sut.Excecute(transactions);

            // Assert
            return result;
        }
    }
}