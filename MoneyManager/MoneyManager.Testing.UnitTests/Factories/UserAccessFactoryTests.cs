﻿using MoneyManager.Common.Factories;
using MoneyManager.Core.Models;
using MoneyManager.Data.Repositories;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Factories
{
    [TestFixture]
    public class UserAccessFactoryTests : BaseTests<UserAccessFactory>
    {
        [Test]
        public void Make_ReturnsUserAccess()
        {
            // Arrange
            var user = Substitute.For<ApplicationUser>();

            // Act
            var result = Sut.Make(user);

            // Assert
            Assert.IsInstanceOf<UserAccess>(result);
        }
    }
}