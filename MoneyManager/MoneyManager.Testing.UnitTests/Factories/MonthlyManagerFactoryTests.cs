﻿using System.Collections.Generic;
using MoneyManager.Common.Factories;
using MoneyManager.Common.Managers;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Data.Database;
using MoneyManager.Testing.TestHelper.PropertyHelpers;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Factories
{
    [TestFixture]
    public class MonthlyManagerFactoryTests : BaseTests<MonthlyManagerFactory>
    {
        [Test]
        public void Make_ReturnsInstanceOfMonthlyManagerFactory()
        {
            // Arrange
	        Mock<MoneyManagerDbContext>().MonthlyCategories = MockDbHelper.CreateMockSet(new List<MonthlyCategoryEntity>());

            // Act
            var result = Sut.Make(0);

            // Assert
            Assert.IsInstanceOf<MonthlyManager>(result);
        }
    }
}