﻿using System.Collections.Generic;
using MoneyManager.Common.Factories;
using MoneyManager.Common.Factories.Budgets;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Data.Database;
using MoneyManager.Data.Repositories;
using MoneyManager.Testing.TestHelper.PropertyHelpers;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Factories.Budgets
{
    [TestFixture]
    public class BudgetAccessFactoryTests : BaseTests<BudgetAccessFactory>
    {
        [Test]
        public void Make_ReturnsNonNull()
        {
			// Arrange
			Mock<MoneyManagerDbContext>().Budgets = MockDbHelper.CreateMockSet(new List<BudgetEntity>());

			// Act
			var result = Sut.Make(new UserEntity());

            // Assert
            Assert.IsNotNull(result);
        }

        [Test]
        public void Make_ReturnsABudgetAccess()
        {
			// Arrange
			Mock<MoneyManagerDbContext>().Budgets = MockDbHelper.CreateMockSet(new List<BudgetEntity>());

			// Act
			var result = Sut.Make(new UserEntity());

            // Assert
            Assert.IsInstanceOf<BudgetAccess>(result);
        }
    }
}
