﻿using MoneyManager.Common.Factories.Budgets;
using MoneyManager.Common.Managers.Budgets;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Repositories;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Factories.Budgets
{
    [TestFixture]
    public class BudgetManagerFactoryTests : BaseTests<BudgetManagerFactory>
    {
        [Test]
        public void Make_ReturnsNonNull()
        {
            // Arrange
            var mockUser = new UserEntity();

            // Act
            var result = Sut.Make(mockUser);

            // Assert
            Assert.IsNotNull(result);
        }
        [Test]
        public void Make_ReturnsABudgetManager()
        {
            // Arrange
            var mockUser = new UserEntity();

            // Act
            var result = Sut.Make(mockUser);

            // Assert
            Assert.IsInstanceOf<BudgetManager>(result);
        }
        [Test]
        public void Make_GivenAUser_ReturnsBudgetAccessWithThatUser()
        {
            // Arrange
            var user = new UserEntity();

            // Act
            Sut.Make(user);

            // Assert
            Mock<IFactory<IAccess<BudgetEntity>, UserEntity>>().Received().Make(user);
        }
    }
}
