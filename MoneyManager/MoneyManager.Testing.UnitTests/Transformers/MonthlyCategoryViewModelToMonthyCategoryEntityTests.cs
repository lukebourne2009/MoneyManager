﻿using System;
using System.Collections;
using MoneyManager.Common.Transformers;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Formatters;
using MoneyManager.Core.ViewModels;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Transformers
{
    [TestFixture]
    public class MonthlyCategoryViewModelToMonthyCategoryEntityTests : BaseTests<MonthlyCategoryViewModelToMonthyCategoryEntity>
    {
        [Test]
		[TestCaseSource(nameof(Transform_ModelIsCorrectlyTransformed_TestData))]
        public void Transform_ModelIsCorrectlyTransformed(MonthlyCategoryViewModel model, Action<MonthlyCategoryEntity> assertion)
        {
			// Arrange

			// Act
            var result = Sut.Transform(model);

			// Assert
	        assertion(result);
        }
	    private static IEnumerable Transform_ModelIsCorrectlyTransformed_TestData()
	    {
			var model = new MonthlyCategoryViewModel
			{
				BudgetedValue = 123,
				BudgetCategoryId = 234,
				Id = 345
			};
			yield return new TestCaseData(model, new Action<MonthlyCategoryEntity>(x => Assert.AreEqual(123, x.BudgetedValue)))
			{
				TestName = "Transform: BudgetedValue is correctly mapped"
			};
			yield return new TestCaseData(model, new Action<MonthlyCategoryEntity>(x => Assert.AreEqual(234, x.ParentId)))
			{
				TestName = "Transform: BudgetCategoryId is correctly mapped"
			};
			yield return new TestCaseData(model, new Action<MonthlyCategoryEntity>(x => Assert.AreEqual(345, x.Id)))
			{
				TestName = "Transform: Id is correctly mapped"
			};
		}

	    [Test]
	    public void Transform_ModelDateIsPopulatedWithResultFromFormatter()
	    {
		    // Arrange

			// Act
		    Sut.Transform(Mock<MonthlyCategoryViewModel>());

			// Assert
		    Mock<IFormatter<string>>().Received(1).Format(Mock<MonthlyCategoryViewModel>().Date);
	    }
	}
}