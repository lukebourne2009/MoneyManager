﻿using System;
using System.Collections;
using MoneyManager.Common.Transformers.Accounts;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Models.Accounts;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Transformers.Accounts
{
    [TestFixture]
    public class AccountModelToAccountEntityTests : BaseTests<AccountModelToAccountEntity>
    {
	    [Test]
	    [TestCaseSource(nameof(Transform_GivenAValidModel_TransformObjectCorrectly_TestData))]
	    public void Transform_PropertiesAreTransformedCorrectly(AccountModel model, Action<AccountEntity> assertion)
	    {
		    // Arrange

		    // Act
		    var result = Sut.Transform(model);

		    // Assert
		    assertion(result);
	    }
	    private static IEnumerable Transform_GivenAValidModel_TransformObjectCorrectly_TestData()
	    {
		    var model = new AccountModel
		    {
			    Id = 123,
			    BudgetId = 456
		    };

		    yield return new TestCaseData(model, new Action<AccountEntity>(x => Assert.AreEqual(model.Id, x.Id)))
		    {
			    TestName = "Transform: Id is populated correctly"
		    };
		    yield return new TestCaseData(model, new Action<AccountEntity>(x => Assert.AreEqual(model.BudgetId, x.ParentId)))
		    {
			    TestName = "Transform: ParentId is populated correctly"
			};
	    }
	}
}