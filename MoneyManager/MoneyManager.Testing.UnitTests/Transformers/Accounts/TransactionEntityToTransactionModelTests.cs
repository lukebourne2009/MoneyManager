﻿using System;
using System.Collections;
using MoneyManager.Common.Transformers.Accounts;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Models.Accounts;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Transformers.Accounts
{
    [TestFixture]
    public class TransactionEntityToTransactionModelTests : BaseTests<TransactionEntityToTransactionModel>
    {
	    [Test]
	    [TestCaseSource(nameof(Transform_GivenAValidModel_TransformObjectCorrectly_TestData))]
	    public void Transform_PropertiesAreTransformedCorrectly(TransactionEntity entity, Action<TransactionModel> assertion)
	    {
		    // Arrange

		    // Act
		    var result = Sut.Transform(entity);

		    // Assert
		    assertion(result);
	    }
	    private static IEnumerable Transform_GivenAValidModel_TransformObjectCorrectly_TestData()
	    {
		    var entity = new TransactionEntity
			{
			    Id = 123,
				CategoryId = 456,
				ParentId = 789,
				Value = 999999
		    };

		    yield return new TestCaseData(entity, new Action<TransactionModel>(x => Assert.AreEqual(entity.Id, x.Id)))
		    {
			    TestName = "Transform: Id is populated correctly"
		    };
		    yield return new TestCaseData(entity, new Action<TransactionModel>(x => Assert.AreEqual(entity.CategoryId, x.CategoryId)))
		    {
			    TestName = "Transform: CategoryId is populated correctly"
			};
		    yield return new TestCaseData(entity, new Action<TransactionModel>(x => Assert.AreEqual(entity.ParentId, x.AccountId)))
		    {
			    TestName = "Transform: AccountId is populated correctly"
			};
		    yield return new TestCaseData(entity, new Action<TransactionModel>(x => Assert.AreEqual(entity.Value, x.Value)))
		    {
			    TestName = "Transform: Value is populated correctly"
			};
	    }
	}
}