﻿using System;
using System.Collections;
using MoneyManager.Common.Transformers.Accounts;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Models.Accounts;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Transformers.Accounts
{
    [TestFixture]
    public class AccountEntityToAccountModelTests : BaseTests<AccountEntityToAccountModel>
    {
        [Test]
        [TestCaseSource(nameof(Transform_GivenAValidModel_TransformObjectCorrectly_TestData))]
		public void Transform_PropertiesAreTransformedCorrectly(AccountEntity entity, Action<AccountModel> assertion)
        {
            // Arrange
            // Act
            var result = Sut.Transform(entity);

			// Assert
	        assertion(result);
        }
		private static IEnumerable Transform_GivenAValidModel_TransformObjectCorrectly_TestData()
		{
			var entity = new AccountEntity
			{
				Id = 123,
				ParentId = 456
			};

			yield return new TestCaseData(entity, new Action<AccountModel>(x => Assert.AreEqual(entity.Id, x.Id)))
			{
				TestName = "Transform: Id is populated correctly"
			};
			yield return new TestCaseData(entity, new Action<AccountModel>(x => Assert.AreEqual(entity.ParentId, x.BudgetId)))
			{
				TestName = "Transform: BudgetId is populated correctly"
			};
		}
	}
}