﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoneyManager.Common.Transformers.Budgets;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Transformers;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Transformers.Budgets
{
    [TestFixture]
    public class BudgetEntityToBudgetModelTests : BaseTests<BudgetEntityToBudgetModel>
    {
	    [Test]
	    [TestCaseSource(nameof(Transform_GivenAValidModel_TransformObjectCorrectly_TestData))]
	    public void Transform_GivenAValidModel_TransformObjectCorrectly(BudgetEntity entity, Action<BudgetModel> assertion)
	    {
		    // Arrange

		    // Act
		    var result = Sut.Transform(entity);

		    // Assert
		    assertion(result);
	    }
	    private static IEnumerable Transform_GivenAValidModel_TransformObjectCorrectly_TestData()
	    {
		    var entity = new BudgetEntity
		    {
			    Id = 1,
			    Name = "Name",
				User_Id = "User",
				LastUsed = new DateTime(1,1,1)
		    };
		    yield return new TestCaseData(entity, new Action<BudgetModel>(x => Assert.AreEqual(entity.Id, x.Id)))
		    {
			    TestName = "Transform: Id is populated correctly"
		    };
		    yield return new TestCaseData(entity, new Action<BudgetModel>(x => Assert.AreEqual(entity.Name, x.Name)))
		    {
			    TestName = "Transform: Name is populated correctly"
		    };
		    yield return new TestCaseData(entity, new Action<BudgetModel>(x => Assert.AreEqual(entity.User_Id, x.UserId)))
		    {
			    TestName = "Transform: UserId is populated correctly"
			};
			yield return new TestCaseData(entity, new Action<BudgetModel>(x => Assert.AreEqual(entity.LastUsed, x.LastUsed)))
		    {
			    TestName = "Transform: LastUsed is populated correctly"
			};
	    }

		[Test]
        public void Transform_GivenAModelWithGroups_TransformIsCalledOnGroupTransformer()
        {
            // Arrange
            var entity = new BudgetEntity
            {
                Collection = new List<BudgetGroupEntity>()
            };

            // Act
            var result = Sut.Transform(entity);

            // Assert
            Assert.AreSame(Mock<ITransformer<BudgetGroupModel, BudgetGroupEntity>>().Transform(entity.Collection), result.BudgetGroups);
        }
		[Test]
        public void Transform_GivenAModelNullGroups_TransformIsNotCalledAndNewListIsCreated()
        {
            // Arrange
            var entity = new BudgetEntity
            {
                Collection = null
            };

            // Act
            var result = Sut.Transform(entity);

            // Assert
			Assert.IsNotNull(result.BudgetGroups);
            Assert.AreNotSame(Mock<ITransformer<BudgetGroupModel, BudgetGroupEntity>>().Transform(entity.Collection), result.BudgetGroups);
        }
    }
}