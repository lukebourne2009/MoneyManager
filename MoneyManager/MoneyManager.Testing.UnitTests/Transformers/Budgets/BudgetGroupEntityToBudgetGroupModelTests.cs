﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoneyManager.Common.Transformers.Budgets;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Mappers;
using MoneyManager.Core.Models.Budgets;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Transformers.Budgets
{
    [TestFixture]
    public class BudgetGroupEntityToBudgetGroupModelTests : BaseTests<BudgetGroupEntityToBudgetGroupModel>
    {
        [Test]
		[TestCaseSource(nameof(Transform_GivenAValidModel_TransformObjectCorrectly_TestData))]
        public void Transform_GivenAValidModel_TransformObjectCorrectly(BudgetGroupEntity entity, Action<BudgetGroupModel> assertion)
        {
            // Arrange

            // Act
            var result = Sut.Transform(entity);

            // Assert
	        assertion(result);
        }
	    private static IEnumerable Transform_GivenAValidModel_TransformObjectCorrectly_TestData()
	    {
		    var entity = new BudgetGroupEntity
		    {
			    Id = 1,
			    Name = "Name",
			    ParentId = 2
		    };
		    yield return new TestCaseData(entity, new Action<BudgetGroupModel>(x => Assert.AreEqual(entity.Id, x.Id)))
		    {
			    TestName = "Transform: Id is populated correctly"
		    };
		    yield return new TestCaseData(entity, new Action<BudgetGroupModel>(x => Assert.AreEqual(entity.Name, x.Name)))
		    {
			    TestName = "Transform: Name is populated correctly"
			};
		    yield return new TestCaseData(entity, new Action<BudgetGroupModel>(x => Assert.AreEqual(entity.ParentId, x.BudgetId)))
		    {
			    TestName = "Transform: ParentId is populated correctly"
			};
	    }

		[Test]
        public void Transform_GivenAModelWithCategories_TransformIsCalledOnCategoryTransformer()
        {
            // Arrange
            var entity = new BudgetGroupEntity
            {
                Id = 1,
                Name = "Name",
                ParentId = 2,
                Collection = new List<BudgetCategoryEntity>()
            };

            // Act
            var result = Sut.Transform(entity);

            // Assert
			Assert.AreEqual(Mock<IBudgetCategoryEntityMapper>().Map(entity.Collection, Arg.Any<DateTime>()),
				result.BudgetCategories);
        }
		[Test]
        public void Transform_GivenAModelWithNoCategories_CreateNewEmptyList()
        {
            // Arrange
            var entity = new BudgetGroupEntity
            {
                Collection = null
            };

            // Act
            var result = Sut.Transform(entity);

			// Assert
			Assert.IsNotNull(result.BudgetCategories);
			Assert.AreNotSame(Mock<IBudgetCategoryEntityMapper>().Map(entity.Collection, Arg.Any<DateTime>()),
				result.BudgetCategories);
			
		}
    }
}