﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MoneyManager.Common.Managers.Budgets;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Mappers;
using MoneyManager.Core.Models.Budgets;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Managers.Budgets
{
    [TestFixture]
    public class BudgetGroupManagerTests : BaseTests<BudgetGroupManager>
    {
        [Test]
        public void CreateBudgetGroup_GivenAValidModel_ReturnNonNull()
        {
            // Arrange
            Mock<IMapper<BudgetGroupModel, BudgetGroupEntity, DateTime>>()
				.Map(Arg.Any<BudgetGroupEntity>(), Arg.Any<DateTime>()).Returns(new BudgetGroupModel());

            // Act
            var result = Sut.CreateBudgetGroup(new BudgetGroupModel());

            // Assert
            Assert.IsNotNull(result);
        }

        [Test]
        public void CreateBudgetGroup_GivenAValidModel_ReturnCreatedModel()
        {
            // Arrange
            var model = new BudgetGroupModel();
	        Mock<IMapper<BudgetGroupModel, BudgetGroupEntity, DateTime>>()
				.Map(Arg.Any<BudgetGroupEntity>(), Arg.Any<DateTime>()).Returns(model);

            // Act
            var result = Sut.CreateBudgetGroup(new BudgetGroupModel());

            // Assert
            Assert.AreEqual(model, result);
        }

        [Test]
        public void GetBudgetGroups_MadeTheCorrectCallToTheDb()
        {
            // Arrange

            // Act
            Sut.GetBudgetGroups();

            // Assert
            Mock<IAccess<BudgetGroupEntity>>().Received(1).GetAll(Arg
                .Any<Expression<Func<BudgetGroupEntity, IEnumerable<ICollection<MonthlyCategoryEntity>>>>>(),
                Arg.Any<Expression<Func<BudgetGroupEntity, IEnumerable<ICollection<TransactionEntity>>>>>());
        }

        [Test]
        public void GetBudgetGroups_GivenANullDateParameter_GetMostRecentDateCategory()
        {
            // Arrange
            var func = Arg.Any<Expression<Func<BudgetGroupEntity, IEnumerable<ICollection<MonthlyCategoryEntity>>>>>();
            var func2 = Arg.Any<Expression<Func<BudgetGroupEntity, IEnumerable<ICollection<TransactionEntity>>>>>();
            Mock<IAccess<BudgetGroupEntity>>().GetAll(func, func2).Returns(new List<BudgetGroupEntity> {new BudgetGroupEntity()});

            // Act
            var result = Sut.GetBudgetGroups().ToList();

			// Assert
	        Mock<IMapper<BudgetGroupModel, BudgetGroupEntity, DateTime>>().Received(1)
				.Map(Arg.Any<BudgetGroupEntity>(), Arg.Is<DateTime>(x => x.Month == DateTime.Now.Month));
        }
        [Test]
        public void GetBudgetGroups_GivenASpecificDateParameter_GetThatDate()
        {
            // Arrange
            var func = Arg.Any<Expression<Func<BudgetGroupEntity, IEnumerable<ICollection<MonthlyCategoryEntity>>>>>();
            var func2 = Arg.Any<Expression<Func<BudgetGroupEntity, IEnumerable<ICollection<TransactionEntity>>>>>();
	        Mock<IAccess<BudgetGroupEntity>>().GetAll(func, func2).Returns(new List<BudgetGroupEntity> { new BudgetGroupEntity() });

            // Act
            Sut.GetBudgetGroups(DateTime.Now.AddMonths(-4)).ToList();

			// Assert
	        Mock<IMapper<BudgetGroupModel, BudgetGroupEntity, DateTime>>().Received(1)
				.Map(Arg.Any<BudgetGroupEntity>(), Arg.Is<DateTime>(x => x.Month == DateTime.Now.AddMonths(-4).Month));
        }
    }
}