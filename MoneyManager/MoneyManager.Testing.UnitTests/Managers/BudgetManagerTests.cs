﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MoneyManager.Common.Managers.Budgets;
using MoneyManager.Common.Transformers;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Transformers;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Managers
{
    [TestFixture]
    public class BudgetManagerTests : BaseTests<BudgetManager>
    {
        #region Get
        [Test]
        public void GetBudgets_ReturnsNonNull_WhenNoBudgetsAreAvailable()
        {
            // Arrange

            // Act
            var result = Sut.Get();

            // Assert
            Assert.IsNotNull(result);
        }
        [Test]
        public void GetBudgets_ReturnsAllBudgets_WhenBudgetsAreAvailable()
        {
            // Arrange
            var budgets = new List<BudgetEntity>()
            {
                new BudgetEntity { Id = 1 }
            };
            Mock<IAccess<BudgetEntity>>().GetAll().Returns(budgets);

            // Act
            var result = Sut.Get();

            // Assert
            Assert.AreEqual(1, result.Count());
        }
        #endregion

        #region Create
        [Test]
        public void CreateBudget_CallsInsertBudgetOnDatabase()
        {
			// Arrange
	        var entity = new BudgetEntity();
			var budget = new BudgetModel
			{
				Id = 1,
				UserId = "IdString"
			};
	        Mock<ITransformer<BudgetEntity, BudgetModel>>().Transform(budget).Returns(entity);


			// Act
			Sut.Create(budget);

			// Assert
	        Mock<IAccess<BudgetEntity>>().ReceivedWithAnyArgs().Insert(entity);
        }
        [Test]
        public void CreateBudget_InsertedCurrentTimeIntoLastUsed()
        {
			// Arrange
			var entity = new BudgetEntity();
	        var budget = new BudgetModel
	        {
		        Id = 1,
		        UserId = "IdString"
	        };
	        Mock<ITransformer<BudgetEntity, BudgetModel>>().Transform(budget).Returns(entity);

            // Act
            Sut.Create(budget);

            // Assert
	        var a = DateTime.Now;
	        a = new DateTime(a.Year, a.Month, a.Day, a.Hour, a.Minute, a.Second, 0);
	        var b = entity.LastUsed;
	        b = new DateTime(b.Year, b.Month, b.Day, b.Hour, b.Minute, b.Second, 0);
	        Assert.AreEqual(a, b);
        }
        #endregion

        #region GetLastUsedBudget
        [Test]
        public void GetLastUsedBudget_GetsBudgetWithMostRecentLastUsedTime()
        {
            // Arrange
            var list = new List<BudgetModel>
            {
                new BudgetModel {Name = "Budget 1", LastUsed = DateTime.Now - TimeSpan.FromDays(5)},
                new BudgetModel {Name = "Budget 2", LastUsed = DateTime.Now - TimeSpan.FromDays(4)},
                new BudgetModel {Name = "Budget 3", LastUsed = DateTime.Now - TimeSpan.FromDays(30)},
            };
	        Mock<IAccess<BudgetEntity>>().GetHighest(Arg.Any<Func<BudgetEntity, DateTime>>()).ReturnsForAnyArgs(
		        new List<BudgetEntity>
		        {
			        new BudgetEntity
			        {
				        Name = "Budget 2",
				        LastUsed = list[1].LastUsed
			        }
		        });

            // Act
            Sut.GetLastUsedBudget();

			// Assert
	        Mock<IAccess<BudgetEntity>>().Received(1).GetHighest(Arg.Any<Func<BudgetEntity, DateTime>>());
        }
        [Test]
        public void GetLastUsedBudget_ConvertsEntityToModel()
        {
            // Arrange
            var list = new List<BudgetModel>
            {
                new BudgetModel {Name = "Budget 1", LastUsed = DateTime.Now - TimeSpan.FromDays(5)},
                new BudgetModel {Name = "Budget 2", LastUsed = DateTime.Now - TimeSpan.FromDays(4)},
                new BudgetModel {Name = "Budget 3", LastUsed = DateTime.Now - TimeSpan.FromDays(30)},
            };
            var entity = Transform.ToBudgetEntity(list[1]);
	        Mock<IAccess<BudgetEntity>>().GetHighest(Arg.Any<Func<BudgetEntity, DateTime>>()).ReturnsForAnyArgs(
		        new List<BudgetEntity>
		        {
			        new BudgetEntity
			        {
				        Name = "Budget 2",
				        LastUsed = list[1].LastUsed
			        }
		        });

			// Act
			var budget = Sut.GetLastUsedBudget();

            // Assert
            Assert.AreEqual(entity.Id, budget.Id);
            Assert.AreEqual(entity.Name, budget.Name);
            Assert.AreEqual(entity.LastUsed, budget.LastUsed);
        }
        #endregion

        #region GetBudget

        [Test]
        public void GetBudget_ThrowsException_WhenNoBudgetsAreAvailable()
        {
            // Arrange

            // Act
            TestDelegate func = () => Sut.GetBudget(10);

            // Assert
            Assert.Throws<ArgumentNullException>(func);
        }
        [Test]
        public void GetBudget_ReturnsBudgetMatchingID()
        {
            // Arrange
            var func = Arg.Any<Expression<Func<BudgetEntity, IEnumerable<BudgetGroupEntity>>>>();
            var func2 = Arg.Any<Expression<Func<BudgetEntity, IEnumerable<ICollection<BudgetCategoryEntity>>>>>();
	        Mock<IAccess<BudgetEntity>>().GetAll(func, func2).Returns(new List<BudgetEntity> {new BudgetEntity { Id = 10 }});

            // Act
            var result = Sut.GetBudget(10);

            // Assert
            Mock<ITransformer<BudgetModel, BudgetEntity>>().Received(1).Transform(Arg.Is<BudgetEntity>(x => x.Id == 10));
        }

        #endregion

        [Test]
        public void BudgetCount_CallsCountOnAccess()
        {
            // Arrange

            // Act
            Sut.BudgetCount();

			// Assert
	        Mock<IAccess<BudgetEntity>>().Received(1).Count();
        }
    }
}
