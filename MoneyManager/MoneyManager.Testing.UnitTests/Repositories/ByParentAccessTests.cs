﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using MoneyManager.Core.Enums;
using MoneyManager.Core.Interfaces;
using MoneyManager.Core.Interfaces.Entities;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Data.Database;
using MoneyManager.Data.Repositories.General;
using MoneyManager.Testing.TestHelper.PropertyHelpers;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Repositories
{


    [TestFixture]
    public class ByParentAccessTests : BaseTests<ByParentAccessTests.TestAccess>
    {
	    public class TestChild : IChild<TestParent>, IChildId<string>, IEntity<int>
	    {
		    public int Id { get; set; }
		    public TestParent Parent { get; set; }
		    public string ParentId { get; set; }
		    public string Name { get; set; }
		    public int SpecialNumber { get; set; }

	    }
	    public class TestParent : IEntity<string>, IGroupParent<TestChild>
	    {
		    public string Id { get; set; }
		    public ICollection<TestChild> Collection { get; set; }
	    }
	    public class TestAccess : ParentByObjectGroupAccess<TestChild, int, TestParent>
	    {
		    public TestAccess(TestParent parent,
			    IDbSet<TestChild> dbSet,
			    MoneyManagerDbContext repo)
			    : base(parent, repo,
				    x => x.Parent.Id == parent.Id,
				    Substitute.For<IEntityStateSetter>())
		    { }
	    };
	    public class TestAccessById : ParentByIdGroupAccess<TestChild, int, string>
	    {
		    public TestAccessById(string parentId,
			    IDbSet<TestChild> dbSet,
			    MoneyManagerDbContext repo)
			    : base(parentId, repo,
				    x => x.ParentId == parentId,
				    Substitute.For<IEntityStateSetter>())
		    {
		    }
	    }

		#region Constructor

		[Test]
        public void Constructor_CanBuildADerivedClassFromByParentAccess_UsingParentID()
        {
            // Arrange
            string parent = "Id";

            // Act
            var result = new TestAccessById(parent, Mock<IDbSet<TestChild>>(), null);

            // Assert
            Assert.IsNotNull(result);
        }
        [Test]
        public void Constructor_PassingANullDbSet_ThrowsAnException()
        {
            // Arrange

            // Act

            // Assert
            Assert.Throws<NullReferenceException>(
                () => new TestAccess(new TestParent(), null, Mock<MoneyManagerDbContext>()));
        }
        [Test]
        public void Constructor_PassingANullParent_ThrowsAnException()
        {
            // Arrange

            // Act

            // Assert
            Assert.Throws<NullReferenceException>(
                () => new TestAccess(null, Mock<IDbSet<TestChild>>(), Mock<MoneyManagerDbContext>()));
        }


        #endregion

        #region GetAll

        [Test]
        public void GetAll_GivenNoChildrenObjects_ReturnAZeroLengthList()
        {
            // Arrange
            var access = CreateAccessByObject();

            // Act
            var result = access.GetAll();

            // Assert
            Assert.AreEqual(0, result.Count());
        }
        [Test]
        public void GetAll_GivenOneChildObject_ReturnAListWithMatchingObject()
        {
            // Arrange
            var parent = new TestParent();
            var data = new List<TestChild>
            {
                new TestChild() {Parent = parent}
            };
            var access = CreateAccessByObject(data, parent);

            // Act
            var result = access.GetAll();

            // Assert
            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(data[0], result.ElementAt(0));
        }
        [Test]
        public void GetAll_GivenMultipleChildObjects_ReturnAListWithMatchingObjects()
        {
            // Arrange
            var parent = new TestParent();
            var data = new List<TestChild>
            {
                new TestChild {Parent = parent},
                new TestChild {Parent = parent},
                new TestChild {Parent = parent} 
            };
            var access = CreateAccessByObject(data, parent);

            // Act
            var result = access.GetAll();

            // Assert
            Assert.AreEqual(3, result.Count());
            for (int i = 0; i < 3; i++)
            {
                Assert.AreEqual(data[i], result.ElementAt(i));
            }
        }
        [Test]
        public void GetAll_GivenMultipleChildObjects_AndTheyHaveDifferentParentObjects_ReturnOnlyChildObjectsOfParent()
        {
            // Arrange
            var parent1 = new TestParent {Id = "Id1"};
            var parent2 = new TestParent {Id = "Id2"};
            var parent3 = new TestParent {Id = "Id3"};

            var data = new List<TestChild>
            {
                new TestChild {Parent = parent1},
                new TestChild {Parent = parent1},
                new TestChild {Parent = parent1},
                new TestChild {Parent = parent2},
                new TestChild {Parent = parent2},
                new TestChild {Parent = parent3},
                new TestChild {Parent = parent3},
                new TestChild {Parent = parent3},
                new TestChild {Parent = parent3},
                new TestChild {Parent = parent3},
            };
            var access = CreateAccessByObject(data, parent3);

            // Act
            var result = access.GetAll();

            // Assert
            Assert.AreEqual(5, result.Count());
            for (int i = 0; i < 5; i++)
            {
                Assert.AreEqual(data[i + 5], result.ElementAt(i));
            }
        }
        [Test]
        public void GetAll_PassingAIncludeStringInCallsIncludeForThatValue()
        {
            // Arrange
            var parent = new TestParent();
            var data = new List<TestChild>
            {
                new TestChild {Parent = parent}
            };
            IDbSet<TestChild> dbSet;
            var access = CreateAccessByObject(out dbSet, data, parent);

            // Act
            var result = access.GetAll("TestString");

            // Assert
            dbSet.Received(1).Include("TestString");
        }
        [Test]
        public void GetAll_PassingAListOfIncludesInCallsIncludeForThoseValues()
        {
            // Arrange
            var parent = new TestParent();
            var data = new List<TestChild>
            {
                new TestChild {Parent = parent}
            };
            IDbSet<TestChild> dbSet;
            var access = CreateAccessByObject(out dbSet, data, parent);

            // Act
            var result = access.GetAll("Test","AnotherTest", "oneMoreTest");

            // Assert
            dbSet.Received(1).Include("Test.AnotherTest.oneMoreTest");
        }
        [Test]
        public void GetAll_PassingAnIncludeFuncCallsTheCorrectInclude()
        {
            // Arrange
            var parent = new TestParent();
            var data = new List<TestChild>
            {
                new TestChild {Parent = parent}
            };
            IDbSet<TestChild> dbSet;
            var access = CreateAccessByObject(out dbSet, data, parent);

            // Act
            Expression<Func<TestChild, IEnumerable<char>>> func = x => x.Name;
            var result = access.GetAll(func);

            // Assert
            dbSet.Received(1).Include(func);
        }

        #endregion

        #region GetAllWhere

        [Test]
        public void GetAllWhere_GivenANullComparitor_returnsAllChildObjects()
        {
            // Arrange
            var parent = new TestParent();
            var data = new List<TestChild>
            {
                new TestChild {Parent = parent}
            };
            var access = CreateAccessByObject(data, parent);

            // Act
            var result = access.GetAllWhere(null);

            // Assert
            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(data[0], result.ElementAt(0));
        }
        [Test]
        public void GetAllWhere_GivenAComparitor_returnsAllChildObjectsThatMatchThisComparison()
        {
            // Arrange
            var parent = new TestParent();
            var data = new List<TestChild>
            {
                new TestChild {Parent = parent, Name = "Valid"},
                new TestChild {Parent = parent, Name = "Valid"},
                new TestChild {Parent = parent, Name = "InValid"}
            };
            var access = CreateAccessByObject(data, parent);

            // Act
            var result = access.GetAllWhere(x => x.Name == "Valid");

            // Assert
            Assert.AreEqual(2, result.Count());
            Assert.AreEqual(data[0], result.ElementAt(0));
            Assert.AreEqual(data[1], result.ElementAt(1));
        }
        [Test]
        public void GetAllWhere_GivenAComparitor_OnlyReturnMatchingObjectsWithMatchingParent()
        {
            // Arrange
            var parent1 = new TestParent { Id = "Id1" };
            var parent2 = new TestParent { Id = "Id2" };
            var data = new List<TestChild>
            {
                new TestChild {Parent = parent1, Name = "Valid"},
                new TestChild {Parent = parent2, Name = "Valid"},
                new TestChild {Parent = parent2, Name = "InValid"}
            };
            var access = CreateAccessByObject(data, parent2);

            // Act
            var result = access.GetAllWhere(x => x.Name == "Valid");

            // Assert
            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(data[1], result.ElementAt(0));
        }

        #endregion

        #region GetById

        [Test]
        public void GetById_ReturnsOnlyObjectIfItMatchesId()
        {
            // Arrange
            var parent1 = new TestParent();
            var data = new List<TestChild>
            {
                new TestChild {Id = 1, Parent = parent1, Name = "Valid"},
                new TestChild {Id = 2, Parent = parent1, Name = "Valid"},
                new TestChild {Id = 3, Parent = parent1, Name = "InValid"}
            };
            var access = CreateAccessByObject(data, parent1);

            // Act
            var result = access.GetById(1);

            // Assert
            Assert.AreEqual(data[0], result);
        }

            #endregion

        #region Insert

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void Insert_GivenANewObject_CallsAddOnDBSet(bool byObject)
        {
            // Arrange
            IDbSet<TestChild> dbSet;
            IAccess<TestChild> access;
            if (byObject)
                access = CreateAccessByObject(out dbSet);
            else
                access = CreateAccessById(out dbSet);

            var obj = new TestChild
            {
                Name = "My Lovely Test Child"
            };

            // Act
            var result = access.Insert(obj);

            // Assert
            Assert.AreEqual(obj, result);
            dbSet.Received(1).Add(obj);
        }
        [Test]
        public void Insert_GivenAnExisitingObject_ThrowsException()
        {
            // Arrange
            IDbSet<TestChild> dbSet;
            var parent = new TestParent();
            var access = CreateAccessByObject(out dbSet, null, parent);
            var obj = new TestChild
            {
                Name = "My Lovely Test Child"
            };
            access.Insert(obj);
            obj.Id = 1;
            obj.Parent = parent;

            // Act

            // Assert
            Assert.Throws<InvalidOperationException>(() => access.Insert(obj));
            Assert.AreEqual(1, dbSet.Count());
        }
        [Test]
        public void Insert_GivenAnExisitingObject_AndSettingParentById_ThrowsException()
        {
            // Arrange
            IDbSet<TestChild> dbSet;
            var access = CreateAccessById(out dbSet, null, "Id");
            var obj = new TestChild
            {
                Name = "My Lovely Test Child"
            };
            access.Insert(obj);
            obj.Id = 1;
            obj.ParentId = "Id";

            // Act

            // Assert
            Assert.Throws<InvalidOperationException>(() => access.Insert(obj));
            Assert.AreEqual(1, dbSet.Count());
        }

        #endregion

        #region Update

        [TestCase(true)]
        [TestCase(false)]
        public void Update_GivenAnObjectThatAlreadyExists_AttachIsCalledOnTheDbSet(bool byObject)
        {
            // Arrange
            IDbSet<TestChild> dbSet;
            IAccess<TestChild> access;
            if (byObject)
                access = CreateAccessByObject(out dbSet);
            else
                access = CreateAccessById(out dbSet);
            var obj = new TestChild
            {
                Name = "My Lovely Test"
            };
            obj = access.Insert(obj);
            obj.Name = "Changed lovely Name";

            // Act
            var result = access.Update(obj);

            // Assert
            dbSet.Received(1).Attach(obj);
        }
        [TestCase(true)]
        [TestCase(false)]
        public void Update_GivenAnObjectThatDoesntExist_ThrowException(bool byObject)
        {
            // Arrange
            IDbSet<TestChild> dbSet;
            IAccess<TestChild> access;
            if (byObject)
                access = CreateAccessByObject(out dbSet);
            else
                access = CreateAccessById(out dbSet);
            var obj = new TestChild()
            {
                Name = "My Lovely Test"
            };

            // Act

            // Assert
            Assert.Throws<InvalidOperationException>(() => access.Update(obj));
        }

        [Test]
        public void Update_CanPassADelegateInToSetProperty()
        {
            // Arrange
            IDbSet<TestChild> dbSet;
            IAccess<TestChild> access = CreateAccessById(out dbSet);
            var obj = new TestChild
            {
                Name = "My Lovely Test",
                SpecialNumber = 100
            };
            obj = access.Insert(obj);
            obj.Name = "Changed lovely Name";

            // Act
            var result = access.Update(obj.Id, x => x.SpecialNumber = 500);

            // Assert
            dbSet.Received(1).Attach(Arg.Any<TestChild>());
        }

        #endregion

        #region Upsert

        [Test]
        public void Upsert_GivenAnObjectThatAlreadyExists_AttachIsCalledOnTheDbSet()
        {
            // Arrange
            IDbSet<TestChild> dbSet;
            var access = CreateAccessByObject(out dbSet);
            var obj = new TestChild()
            {
                Name = "My Lovely Test"
            };
            obj = access.Insert(obj);
            obj.Name = "Changed lovely Name";
            dbSet.ClearReceivedCalls();

            // Act
            var result = access.Upsert(obj);

            // Assert
            dbSet.Received(1).Attach(obj);
            dbSet.DidNotReceive().Add(Arg.Any<TestChild>());
            Assert.AreEqual(UpsertResult.Updated,result);
        }
        [Test]
        public void Upsert_GivenANewObject_CallsAddOnDBSet()
        {
            // Arrange
            IDbSet<TestChild> dbSet;
            var access = CreateAccessByObject(out dbSet);
            var obj = new TestChild()
            {
                Name = "My Lovely Test"
            };

            // Act
            var result = access.Upsert(obj);

            // Assert
            dbSet.Received(1).Add(obj);
            dbSet.DidNotReceive().Attach(Arg.Any<TestChild>());
            Assert.AreEqual(UpsertResult.Inserted, result);
        }

        #endregion

        #region GetHighest

        [Test]
        public void GetHighest_GivenNoChildObjects_ReturnNull()
        {
            // Arrange
            var access = CreateAccessByObject();

            // Act
            var result = access.GetHighest(x => x.Id);

            // Assert
            Assert.IsNull(result);
        }
        [Test]
        public void GetHighest_GivenOneChildObjects_ReturnThatObject()
        {
            // Arrange
            var parent = new TestParent();
            var list = new List<TestChild>
            {
                new TestChild {Id = 1,Parent = parent}
            };
            var access = CreateAccessByObject(list, parent);

            // Act
            var result = access.GetHighest(x => x.Id);

            // Assert
            Assert.AreEqual(list[0],result);
        }
        [Test]
        public void GetHighest_GivenMultipleChildObjects_ReturnTheObjectWithHighestParameter()
        {
            // Arrange
            var parent = new TestParent();
            var list = new List<TestChild>
            {
                new TestChild {Id = 12,Parent = parent},
                new TestChild {Id = 65,Parent = parent},
                new TestChild {Id = 23,Parent = parent},
            };
            var access = CreateAccessByObject(list, parent);

            // Act
            var result = access.GetHighest(x => x.Id);

            // Assert
            Assert.AreEqual(list[1], result);
        }
        [Test]
        public void GetHighest_GivenMultipleChildObjects_AndResultsWithTheSameComparisonValue_ReturnEither()
        {
            // Arrange
            var parent = new TestParent();
            var list = new List<TestChild>
            {
                new TestChild {Id = 12,Parent = parent},
                new TestChild {Id = 65,Parent = parent},
                new TestChild {Id = 65,Parent = parent},
            };
            var access = CreateAccessByObject(list, parent);

            // Act
            var result = access.GetHighest(x => x.Id);

            // Assert
            Assert.IsTrue(result == list[1] || result == list[2]);
        }
        [Test]
        public void GetHighest_NullValuesAreIgnoredInSearch()
        {
            // Arrange
            var parent = new TestParent();
            var list = new List<TestChild>
            {
                new TestChild {Id=1, Parent = parent}
            };
            var access = CreateAccessByObject(list, parent);

            // Act
            var result = access.GetHighest(x => x.Name);

            // Assert
            Assert.IsNull(result);
        }

        #endregion

        #region Count

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(50)]
        public void Count_GivenXBudgets_ReturnX(int nBudgets)
        {
            // Arrange
            var parent = new TestParent();
            var list = new List<TestChild>();
            for (int i = 0; i < nBudgets; i++)
            {
                list.Add(new TestChild {Id = i,Parent = parent});
            }
            var access = CreateAccessByObject(list, parent);

            // Act
            var result = access.Count();

            // Assert
            Assert.AreEqual(nBudgets, result);
        }

        #endregion

        #region Delete

        [Test]
        public void Delete_GivenInvalidId_ThrowException()
        {
            // Arrange
            var access = CreateAccessByObject();

            // Act

            // Assert
            Assert.Throws<ArgumentException>(() => access.Delete(1));
        }

        [Test]
        public void Delete_GivenValidId_ObjectIsDeleted()
        {
            // Arrange
            var parent = new TestParent();
            var data = new List<TestChild>
            {
                new TestChild {Id = 56, Parent = parent}
            };
            IDbSet<TestChild> set;
            var access = CreateAccessByObject(out set, data, parent);

            // Act
            access.Delete(56);

            // Assert
            set.Received(1).Remove(data[0]);
        }

        #endregion

        private TestAccess CreateAccessByObject(List<TestChild> data = null, TestParent parent = null)
        {
            IDbSet<TestChild> set;
            return CreateAccessByObject(out set, data, parent);
        }
        private TestAccess CreateAccessByObject(out IDbSet<TestChild> mockDbSet, 
            List <TestChild> data = null, TestParent parent = null)
        {
            var repo = Substitute.For<MoneyManagerDbContext>();
            if (data == null)
            {
                data = new List<TestChild>();
            }
            if (parent == null)
            {
                parent = new TestParent();
            }
            mockDbSet = MockDbHelper.CreateMockSet(data);

            return new TestAccess(parent, mockDbSet, repo);
        }
        private TestAccessById CreateAccessById(out IDbSet<TestChild> mockDbSet, 
        List<TestChild> data = null, string parentId = "")
        {
            var repo = Substitute.For<MoneyManagerDbContext>();
            if (data == null)
            {
                data = new List<TestChild>();
            }
            mockDbSet = MockDbHelper.CreateMockSet(data);

            return new TestAccessById(parentId, mockDbSet, repo);
        }
    }
}
