﻿using System;
using System.Data.Entity;
using System.Linq;
using MoneyManager.Core.Interfaces.Entities;
using MoneyManager.Data.Repositories;
using MoneyManager.Data.Repositories.General;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Repositories
{
	[TestFixture]
	public class SingleAccessTests : BaseTests<SingleAccessTests.TestAccess>
	{
		public class TestAccess : SingleAccess<TestChild, TestParent>
		{
			public TestAccess(TestParent parent, DbContext repo)
				: base(parent, repo, x => x.Parent == parent)
			{
			}
		}
		public class TestParent
		{

		}
		public class TestChild : IChild<TestParent>, IEntity<string>
		{
			public TestParent Parent { get; set; }
			public string Id { get; set; }
		}

		#region Get

		[Test]
		public void Get_GivenNoChildedObject_ReturnNull()
		{
			// Arrange

			// Act
			var result = Sut.Get();

			// Assert
			Assert.IsNull(result);
		}
		[Test]
		[TestCase("include", new object[] { "include" })]
		[TestCase("include.OtherInlcude", new object[] { "include", "OtherInlcude" })]
		[TestCase("include.OtherInlcude.MoreInclude", new object[] { "include", "OtherInlcude", "MoreInclude" })]
		public void Get_GivenAChildObject_ReturnThat(string expected, object[] strings)
		{
			// Arrange

			// Act
			var result = Sut.Get(strings.Cast<string>().ToArray());

			// Assert
			Mock<IDbSet<TestChild>>().Received(1).Include(expected);
		}

		#endregion

		#region Insert

		[Test]
		public void Insert_IfEntityIsAdded_AddedIsCalled()
		{
			// Arrange
			var child = new TestChild();

			// Act
			Sut.Insert(child);

			// Assert
			Mock<IDbSet<TestChild>>().Received(1).Add(child);
		}
		[Test]
		public void Insert_IfEntityIsAdded_SaveChangesIsCalled()
		{
			// Arrange
			var child = new TestChild();

			// Act
			Sut.Insert(child);

			// Assert
			Mock<DbContext>().Received(1).SaveChanges();
		}
		[Test]
		public void Insert_IfEntityIsAdded_ParentIsAttached()
		{
			// Arrange
			var child = new TestChild
			{
				Parent = null
			};

			// Act
			var result = Sut.Insert(child);

			// Assert
			Assert.IsNotNull(result.Parent);
		}

		#endregion

		#region Update

		[Test]
		public void Update_IfEntityIsNotAlreadyInDb_ThrowException()
		{
			// Arrange
			var entity = new TestChild();

			// Act
			TestDelegate result = () => Sut.Update(entity);

			// Assert
			Assert.Throws<InvalidOperationException>(result);

		}
		#endregion
	}
}
