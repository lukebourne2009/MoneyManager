﻿using System;
using MoneyManager.Common.Formatter;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Formatter
{
    [TestFixture]
    public class LongMonthAndYearFormatterTests : BaseTests<LongMonthAndYearFormatter>
    {
        [TestCase("")]
        [TestCase("a0-2000")]
        [TestCase("00-2000")]
        [TestCase("13-2000")]
        [TestCase("00-2000234")]
        [TestCase("01-2000-2000")]
        public void Format_ThrowFormatExceptionOn(string input)
        {
			// Arrange

            // Act
	        TestDelegate del = () => Sut.Format(input);

			// Assert
	        Assert.Throws<FormatException>(del);
        }

        [Test(ExpectedResult = "December 2000")]
        public string Format_ValidDateStringFormatsCorrectly()
        {
            // Arrange

            // Act
            var result = Sut.Format("12-2000");

            // Assert
            return result;
        }
    }
}