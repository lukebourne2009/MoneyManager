﻿using System;
using MoneyManager.Common.Formatter;
using NUnit.Framework;
using TesterHelper.Core;

namespace MoneyManager.Testing.UnitTests.Formatter
{
    [TestFixture]
    public class ShortMonthAndYearFormatterTests : BaseTests<ShortMonthAndYearFormatter>
    {
        [TestCase("")]
        [TestCase("blah blah 2000")]
        public void Format_ThrowFormatExceptionOn(string input)
        {
	        // Arrange

	        // Act
	        TestDelegate del = () => Sut.Format(input);

	        // Assert
	        Assert.Throws<FormatException>(del);
		}

        [Test(ExpectedResult = "08-2000")]
        public string Format_ValidDateStringFormatsCorrectly()
        {
            // Arrange

            // Act
            var result = Sut.Format("August 2000");

            // Assert
            return result;
        }
    }
}