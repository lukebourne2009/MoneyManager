﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MoreLinq;
using NSubstitute;
using NUnit.Framework;
using TesterHelper.Core.Exceptions;

namespace TesterHelper.Core
{
	[TestFixture]
	public abstract class BaseTests<TSut> : MockContainer<TSut>
		where TSut : class
	{
		[SetUp]
		public void BaseSetup()
		{
			Properties.Clear();
			Sut = Create<TSut>();
		}

		[Test]
		[TestCaseSource("NullParameterChecks_TestData")]
		public void NullParameterChecks(Action test)
		{
			test();
		}

		protected static IEnumerable NullParameterChecks_TestData()
		{
			List<TestCaseData> tests = new List<TestCaseData>();
			var methods = typeof(TSut).GetMethods().Where(x => x.DeclaringType == typeof(TSut));

			foreach (var method in methods)
			{
				if (method.DeclaringType.IsAbstract 
					|| method.DeclaringType.GetProperties().Any(x => x.GetSetMethod() == method))
					continue;

				var parameterInfos = method.GetParameters();
				for (var index = 0; index < parameterInfos.Length; index++)
				{
					var parameterInfo = parameterInfos[index];
					if (parameterInfo.ParameterType.IsValueType)
						continue;

					Action test = () =>
					{
						var sut = Create<TSut>();
						Assert.Throws<ArgumentNullException>(() =>
						{
							try
							{
								method.Invoke(sut, GenerateParameters(parameterInfos, parameterInfo));
							}
							catch (TargetInvocationException e)
							{
								if (e.InnerException != null) throw e.InnerException;
							}
						});
					};
					tests.Add(new TestCaseData(test)
					{
						TestName = string.Format("{2}: passing null for parameter {0} of type {1}, throws an exception.",
							index + 1, parameterInfo.ParameterType.Name, method.Name)
					});
				}
			}
			return tests;
		}
		private static object CreateSystemUnderTest(MethodInfo method)
		{
			object[] sutParameters = { };
			var constructorInfos = method.DeclaringType.GetConstructors();

			if (constructorInfos.Any())
			{
				var sutConstructor = constructorInfos[0];
				var sutConstructorParameters = sutConstructor.GetParameters();

				sutParameters = GenerateParameters(sutConstructorParameters);
			}

			var classInstance = Activator.CreateInstance(method.DeclaringType, sutParameters);
			return classInstance;
		}
		private static object[] GenerateParameters(ParameterInfo[] sutConstructorParameters, ParameterInfo excludedParameter = null)
		{
			object[] sutParameters = new object[sutConstructorParameters.Length];
			for (int i = 0; i < sutConstructorParameters.Length; i++)
			{
				if (excludedParameter != null && sutConstructorParameters[i].Equals(excludedParameter))
				{
					sutParameters[i] = null;
				}
				else
				{
					if (sutConstructorParameters[i].ParameterType.IsSealed)
					{
						sutParameters[i] = DefaultObjects[sutConstructorParameters[i].ParameterType];
					}
					else
					{
						var mockParameter = Substitute.For(new[] {sutConstructorParameters[i].ParameterType}, new object[0]);
						sutParameters[i] = mockParameter;
					}
				}
			}

			return sutParameters;
		}
	}
}