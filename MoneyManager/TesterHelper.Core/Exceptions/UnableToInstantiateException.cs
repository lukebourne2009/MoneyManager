﻿using System;

namespace TesterHelper.Core.Exceptions
{
	public class UnableToInstantiateException : Exception
	{
		public UnableToInstantiateException(Type type)
			:base(string.Format("Cannot instantiate the type: '{0}' as it is either an interface or an abstract class", type))
		{}
	}
}