﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoreLinq;
using NSubstitute;
using TesterHelper.Core.Exceptions;

namespace TesterHelper.Core
{
	public abstract class MockContainer<TSut>
	{
		protected TSut Sut { get; set; }
		protected static readonly IDictionary<string, object> Properties = new Dictionary<string, object>();
		protected static readonly IDictionary<Type, object> DefaultObjects = new Dictionary<Type, object>
		{
			{typeof(string), "Test"},
			{typeof(DateTime), new DateTime(1, 1, 1)},
			{typeof(int), 42}
		};

		protected static TType Mock<TType>()
			where TType : class
		{
			return Mock(typeof(TType)) as TType;
		}
		protected static TType Mock<TType>(string argumentName)
			where TType : class
		{
			return Mock(typeof(TType), argumentName) as TType;
		}
		protected static object Mock(Type type)
		{
			return Mock(type, "");
		}
		protected static object Mock(Type type, string argumentName)
		{
			var key = type.ToString() + '.' + argumentName;
			var value = Properties.SingleOrDefault(x => x.Key.Contains(key)).Value;
			if (value != null)
				return value;

			if (type.IsValueType || type == typeof(string))
				value = DefaultObjects[type];
			else
				value = Substitute.For(new[] { type }, new object[0]);

			Properties.Add(key, value);
			return value;
		}

		protected static TType Create<TType>()
			where TType : class
		{
			return Create(typeof(TType)) as TType;
		}
		protected static object Create(Type type)
		{
			if (type.IsAbstract || type.IsInterface)
				throw new UnableToInstantiateException(type);

			var constructor = type.GetConstructors().MaxBy(x => x.GetParameters().Length);

			List<object> parameters = new List<object>();
			foreach (var parameterInfo in constructor.GetParameters())
			{
				parameters.Add(Mock(parameterInfo.ParameterType, parameterInfo.Name));
			}

			var result = constructor.Invoke(parameters.ToArray());

			return result;
		}
	}
}