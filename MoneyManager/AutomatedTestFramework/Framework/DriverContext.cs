﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using AutomatedTestFramework.Framework.Core;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace AutomatedTestFramework.Framework
{
    public static class DriverContext
    {
        private static IWebDriver _driver;
        private static DriverType _driverType = DriverType.Chrome; // Default to FF as selenium natively supports it.
        private static readonly TimeSpan _pageLoadTimeout = TimeSpan.FromSeconds(60);
        private static readonly TimeSpan _elementLoadTimeout = TimeSpan.FromSeconds(0);

        public static IWebDriver Driver
        {
            get
            {
                if (_driver == null)
                {
                    InitialiseDriver();
                }

                return _driver;
            }
            set
            {
                _driver = value;
            }
        }
        public static DriverType DriverType
        {
            get { return _driverType; }
            set { _driverType = value; }
        }
        public static void CloseDriver()
        {
            _driver.Close();
            _driver.Quit();
            _driver = null;
        }
        public static void WindowMaximise()
        {
            Driver.Manage().Window.Maximize();
        }
        private static void InitialiseDriver()
        {
            switch (_driverType)
            {
                case DriverType.Firefox:
                {
                    string pathToCurrentUserProfiles = Environment.ExpandEnvironmentVariables("%APPDATA%") + @"\Mozilla\Firefox\Profiles"; // Path to profile
                    string[] pathsToProfiles = Directory.GetDirectories(pathToCurrentUserProfiles, "*.default", SearchOption.TopDirectoryOnly);
                    FirefoxProfile firefoxProfile = new FirefoxProfile(pathsToProfiles[0]);

                    firefoxProfile.AcceptUntrustedCertificates = true;
                    firefoxProfile.AssumeUntrustedCertificateIssuer = false;

                    _driver = new FirefoxDriver(firefoxProfile);
                    break;
                }
                case DriverType.Chrome:
                {
                    DesiredCapabilities desiredCapabilities = DesiredCapabilities.Chrome();
                    desiredCapabilities.SetCapability(CapabilityType.AcceptSslCertificates, true);

                    var path = Uri.UnescapeDataString(new Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath);
                    _driver = new ChromeDriver(new FileInfo(path).DirectoryName);
                    break;
                }
                default:
                {
                    throw new InvalidOperationException("Unknown Driver Type was requested.");
                }
            }

            _driver.Manage().Timeouts().PageLoad = _pageLoadTimeout;
            _driver.Manage().Timeouts().ImplicitWait = _elementLoadTimeout;
        }

        public static bool IsUrl(string url)
        {
            if (url.StartsWith("/"))
            {
                var homeUrl = ConfigurationManager.AppSettings["SiteAddress"];
                var fullUrl = homeUrl + url;

                if (Driver.Url == Regex.Match(Driver.Url, fullUrl).Value)
                {
                    return true;
                }
            }
            else
            {
                if (Driver.Url == url)
                {
                    return true;
                }
            }
  
            return false;
        }
    }
}