﻿using System;
using System.Configuration;
using AutomatedTestFramework.Framework.Core;
using OpenQA.Selenium;

namespace AutomatedTestFramework.Framework.Common
{
    public class Page : IPage
    {
        public static IWebDriver Driver = DriverContext.Driver;
        private readonly string _url;

        public Page(string url)
        {
            _url = url;
        }

        public static TPage Instance<TPage>()
            where TPage : new()
        {
            return new TPage();
        }

        public void GoTo()
        {
            GoTo(_url);
        }

        public static void GoTo(string url)
        {
            // Try absolute Uri
            Uri result;
            Uri.TryCreate(url, UriKind.Absolute, out result);

            if (result != null)
            {
                Driver.Navigate().GoToUrl(url);
            }

            // Try relative Uri
            Uri.TryCreate(url, UriKind.Relative, out result);

            if (result != null)
            {
                var address = ConfigurationManager.AppSettings["SiteAddress"];
                result = new Uri(address + result);
                Driver.Navigate().GoToUrl(result);
            }
        }
    }
}
