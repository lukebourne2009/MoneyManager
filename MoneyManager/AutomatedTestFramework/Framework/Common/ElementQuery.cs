﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using AutomatedTestFramework.Framework.Core;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace AutomatedTestFramework.Framework.Common
{
    public class ElementQuery : IElementQuery
    {
        private readonly IElementQuery _elementQuery;
        protected readonly IWebDriver _driver;
        protected readonly string _selector;

        private readonly IWebElement _webElement;

        #region Constructors

        protected ElementQuery(string selector)
        {
            _selector = selector;
        }

        public ElementQuery(string selector, IWebDriver driver) : this(selector)
        {
            _driver = driver;
        }

        public ElementQuery(IWebElement webElement, string selector)
        {
            _webElement = webElement;
            _selector = selector;
        }

        public ElementQuery(IElementQuery elementQuery, string selector)
        {
            _elementQuery = elementQuery;
            _selector = selector;
        }

        public static implicit operator ElementQuery(string selector)
        {
            ElementQuery element = new ElementQuery(selector);
            return element;
        }

        #endregion

        #region Properties

        public string Selector => (_elementQuery?.Selector == null ? "" : _elementQuery?.Selector + " ") + _selector;
        public ReadOnlyCollection<IWebElement> Elements {
            get
            {
                return _webElement != null
                    ? _webElement.FindElements(By.CssSelector(Selector))
                    : Driver.FindElements(By.CssSelector(Selector));
            }
        }
        public IWebElement Element
        {
            get
            {
                try
                {
                    var result = _webElement != null
                        ? _webElement.FindElement(By.CssSelector(Selector))
                        : Driver.FindElement(By.CssSelector(Selector));
                    return result;
                }
                catch (NoSuchElementException)
                {
                    return null;
                }
            }
        }
        private IWebDriver Driver => _driver ?? DriverContext.Driver;

        #endregion

        #region Methods

        public bool IsDisplayed(int wait = 0)
        {
            try
            {
                new WebDriverWait(Driver, TimeSpan.FromMilliseconds(wait))
                    .Until(x => AreDisplayed()
                    && Elements[0].Size.Height > 0
                    && Elements[0].Size.Width > 0);
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
            catch (ArgumentOutOfRangeException)
            {
                return false;
            }
        }
        public bool AreDisplayed()
        {
            foreach (var webElement in Elements)
            {
                if (!webElement.Displayed)
                {
                    return false;
                }
            }
            return true;
        }
        public void EnterText(string text)
        {
            CheckNotMutipleElements();
            new WebDriverWait(Driver, TimeSpan.FromMilliseconds(1500))
                .Until(x =>
                {
                    try
                    {
                        Element.Clear();
                        Element.SendKeys(text);
                        return true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
        }
        public bool IsMultipleElements()
        {
            return Elements.Count > 1;
        }
        public bool IsSingleElement()
        {
            return Elements.Count == 1;
        }
        public bool IsClickable()
        {
            CheckNotMutipleElements();
            var cursor = Elements[0].GetCssValue("cursor");

            if (cursor == "pointer") return true;
            return false;
        }

        public IElementQuery MouseLeave(int elementN = 0)
        {
            Actions action = new Actions(Driver);
            action.MoveToElement(Elements[elementN]).MoveByOffset(-50,-50).Build().Perform();
            return this;
        }
        public ElementQuery MouseOver(int elementN = 0)
        {
            Actions action = new Actions(Driver);
            action.MoveToElement(Elements[elementN]).Build().Perform();
            return this;
        }
        public void Click(int elementN = 0)
        {
            new WebDriverWait(DriverContext.Driver, TimeSpan.FromMilliseconds(3000))
                .Until(
                    x =>
                    {
                        try
                        {
                            Elements[elementN].Click();
                            return true;
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
        }
        public ICollection<IWebElement> Find(string selector)
        {
            return Element.FindElements(By.CssSelector(selector));
        }
        public IWebElement GetElement(int nBudget = 0)
        {
            new WebDriverWait(DriverContext.Driver, TimeSpan.FromMilliseconds(3000))
                .Until(x => Elements[nBudget].Displayed);
            return Elements[nBudget];
        }
        public ElementQuery Event(string eventTarget, int nElement = 0)
        {
            if (IsDisplayed(1000))
            {
                IJavaScriptExecutor executor = DriverContext.Driver as IJavaScriptExecutor;
                var script = $@"$($('{Selector}').get({nElement})).trigger('{eventTarget}');";
                executor.ExecuteScript(script);
            }
            else
                throw new Exception("Something went wrong");

            return this;
        }

        public IElementQuery On(IWebElement @on)
        {
            return new ElementQuery(on, Selector);
        }

        public ElementQuery Combine(string selector)
        {
            return new ElementQuery(this, selector);
        }

        #endregion Methods

        private void CheckNotMutipleElements()
        {
            if (IsMultipleElements())
            {
                throw new Exception("Multiple elements found using this selector. Use 'AreDisplayed()' If this is expected.");
            }
        }
        public IWebElement this[int i] => Elements[i];
    }
}