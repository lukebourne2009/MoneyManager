﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using AutomatedTestFramework.Framework.Common;
using OpenQA.Selenium;

namespace AutomatedTestFramework.Framework.Core
{
    public interface IElementQuery
    {
        ReadOnlyCollection<IWebElement> Elements { get; }
        string Selector { get; }

        bool IsDisplayed(int wait = 0);
        bool IsClickable();
        IElementQuery MouseLeave(int elementN = 0);
        ElementQuery MouseOver(int elementN = 0);
        void Click(int elementN = 0);
        void EnterText(string text);
        bool IsMultipleElements();
        bool IsSingleElement();
        ICollection<IWebElement> Find(string selector);
        IWebElement GetElement(int elementN = 0);
        ElementQuery Combine(string selector);
        ElementQuery Event(string mouseleave, int nElement = 0);
        IElementQuery On(IWebElement on);
        IWebElement this[int i] { get; }
    }
}