﻿using AutomatedTestFramework.Framework.Common;

namespace AutomatedTestFramework.Framework.Core.ElementGroups
{
    public interface IForm : IElementQuery
    {
        ElementQuery PositiveButton { get; }
        ElementQuery NegativeButton { get; }
    }
}