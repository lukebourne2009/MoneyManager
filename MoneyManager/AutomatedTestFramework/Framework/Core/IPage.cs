﻿namespace AutomatedTestFramework.Framework.Core
{
    public interface IPage
    {
        void GoTo();
    }
}
