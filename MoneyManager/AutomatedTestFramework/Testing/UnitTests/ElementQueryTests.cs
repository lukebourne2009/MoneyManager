﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using AutomatedTestFramework.Framework.Common;
using NSubstitute;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AutomatedTestFramework.Testing.UnitTests
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class ElementQueryTests
    {
        [Test]
        public void Constructor_StringCanBeAssignedToConstructAnElementQuery()
        {
            // Arrange

            // Act
            ElementQuery result = "String";

            // Assert
            Assert.IsNotNull(result);
        }

        [Test]
        public void IsDisplayed_GivenTheDriverFindsNoMatchForTheSelector_ThenReturnsFalse()
        {
            // Arrange
            var driver = Substitute.For<IWebDriver>();
            driver.FindElements(By.CssSelector("myElement")).Returns(new ReadOnlyCollection<IWebElement>(new List<IWebElement>()));
            var query = new ElementQuery("myElement", driver);

            // Act
            var result = query.IsDisplayed();

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void IsDisplayed_GivenTheDriverFindsAMatchForTheSelector_AndTheElementIsDisplayed_ThenReturnsTrue()
        {
            // Arrange
            var driver = Substitute.For<IWebDriver>();
            var mockElement = Substitute.For<IWebElement>();
            mockElement.Size.Returns(new Size(100, 100));
            mockElement.Displayed.Returns(true);
            driver.FindElements(By.CssSelector("myElement")).Returns(new ReadOnlyCollection<IWebElement>(new List<IWebElement> { mockElement }));
            var query = new ElementQuery("myElement", driver);

            // Act
            var result = query.IsDisplayed();

            // Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void IsDisplayed_GivenTheDriverFindsAMatchForTheSelector_ButTheElementIsNotDisplayed_ThenReturnsFalse()
        {
            // Arrange
            var driver = Substitute.For<IWebDriver>();
            var mockElement = Substitute.For<IWebElement>();
            mockElement.Displayed.Returns(false);
            driver.FindElements(By.CssSelector("myElement")).Returns(new ReadOnlyCollection<IWebElement>(new List<IWebElement> { mockElement }));
            var query = new ElementQuery("myElement", driver);

            // Act
            var result = query.IsDisplayed();

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void IsDisplayed_GivenTheDriverFindsMultipleMatchesForTheElement_ThenAnExceptionIsThrown()
        {
            // Arrange
            var driver = Substitute.For<IWebDriver>();
            var mockElement = Substitute.For<IWebElement>();
            driver.FindElements(By.CssSelector("myElement")).Returns(new ReadOnlyCollection<IWebElement>(new List<IWebElement> { mockElement, mockElement }));
            var query = new ElementQuery("myElement", driver);

            // Act

            // Assert
            Assert.Throws<Exception>(() => query.IsDisplayed());
        }

        [Test]
        public void Find_SearchsForAllElementsUnderAKnownElement()
        {
            // Arrange
            var driver = Substitute.For<IWebDriver>();
            var mockElement = Substitute.For<IWebElement>();
            driver.FindElement(By.CssSelector("myElement")).Returns(mockElement);
            var query = new ElementQuery("myElement", driver);

            // Act
            var results = query.Find("li");

            // Assert
            mockElement.Received(1).FindElements(Arg.Is(By.CssSelector("li")));
        }

        [Test]
        public void Elements_ReturnsAListOfElementsFoundBySelector()
        {
            // Arrange
            var driver = Substitute.For<IWebDriver>();
            var mockElements = new ReadOnlyCollection<IWebElement>(new List<IWebElement> { Substitute.For<IWebElement>()});
            driver.FindElements(By.CssSelector("myElement")).Returns(mockElements);
            var query = new ElementQuery("myElement", driver);

            // Act
            var results = query.Elements;

            // Assert
            driver.Received(1).FindElements(Arg.Is(By.CssSelector("myElement")));
            Assert.AreEqual(mockElements,results);
        }

        [Test]
        public void IndexOperator_GivenNoElements_ThrowException()
        {
            // Arrange
            var driver = Substitute.For<IWebDriver>();
            var mockElements = new ReadOnlyCollection<IWebElement>(new List<IWebElement>());
            driver.FindElements(By.CssSelector("myElement")).Returns(mockElements);
            var query = new ElementQuery("myElement", driver);

            // Act
            TestDelegate result = () => { var i = query[0]; };

            // Assert
            Assert.Throws<ArgumentOutOfRangeException>(result);

        }

        [Test]
        [TestCase(1)]
        [TestCase(5)]
        [TestCase(10)]
        public void IndexOperator_Given1Elements_OperatorXGetsTheXElement(int arg)
        {
            // Arrange
            var driver = Substitute.For<IWebDriver>();
            var elements = new List<IWebElement>();
            for (int i = 0; i < arg; i++)
            {
                elements.Add(Substitute.For<IWebElement>());
            }
            var mockElements = new ReadOnlyCollection<IWebElement>(elements);
            driver.FindElements(By.CssSelector("myElement")).Returns(mockElements);
            var query = new ElementQuery("myElement", driver);

            // Act
            var result = query[arg-1];

            // Assert
            Assert.AreEqual(elements[arg-1], result);

        }
    }
}
