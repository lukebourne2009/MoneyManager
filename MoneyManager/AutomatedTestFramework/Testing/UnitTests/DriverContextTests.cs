﻿using System.Diagnostics.CodeAnalysis;
using AutomatedTestFramework.Framework;
using NSubstitute;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AutomatedTestFramework.Testing.UnitTests
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class DriverContextTests
    {
        [Test]
        public void GivenUrlIsFooSlashBarWhenParammIsFooSlashBarThenReturnTrue()
        {
            // Arrange
            var driver = Substitute.For<IWebDriver>();
            var url = "www.joeblogs.com/Foo/Bar";
            driver.Url.Returns(url);
            DriverContext.Driver = driver;

            // Act
            var result = DriverContext.IsUrl(url);

            // Assert
            Assert.IsTrue(result);
        }
        [Test]
        public void GivenUrlIsPooSlashBlabWhenParammIsFooSlashBarThenReturnFalse()
        {
            // Arrange
            var driver = Substitute.For<IWebDriver>();
            var url = "www.joeblogs.com/Foo/Bar";
            driver.Url.Returns("www.joeblogs.com/Poo/Blab");
            DriverContext.Driver = driver;

            // Act
            var result = DriverContext.IsUrl(url);

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        [Ignore("Need to rework DriverContext")]
        public void GivenParamIsRelativePath_AndRelativePathMatches_ReturnTrue()
        {
            // Arrange
            var driver = Substitute.For<IWebDriver>();
            var url = "/Foo/Bar";
            driver.Url.Returns("www.joeblogs.com/Foo/Bar");
            DriverContext.Driver = driver;

            // Act
            var result = DriverContext.IsUrl(url);

            // Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void GivenParamIsRelativePath_AndRelativePathDoesNotMatch_ReturnFalse()
        {
            // Arrange
            var driver = Substitute.For<IWebDriver>();
            var url = "/Foo/Bar";
            driver.Url.Returns("www.joeblogs.com/Blog/Foo/Bar");
            DriverContext.Driver = driver;

            // Act
            var result = DriverContext.IsUrl(url);

            // Assert
            Assert.IsFalse(result);
        }


        [TearDown]
        public void TearDown()
        {
            DriverContext.Driver = null;
        }
    }
}