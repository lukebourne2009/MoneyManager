﻿using Microsoft.AspNet.Identity.EntityFramework;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Models;
using System.Data.Entity;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Entities.Budgets;

namespace MoneyManager.Data.Database
{
    public class MoneyManagerDbContext : IdentityDbContext<ApplicationUser>, IMoneyManagerRepository
    {
        private static MoneyManagerDbContext _context;

        public MoneyManagerDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {}

        public MoneyManagerDbContext(string connection)
            : base(connection, throwIfV1Schema: false)
        {}

        public static MoneyManagerDbContext Create()
        {
            return _context = new MoneyManagerDbContext();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityUserLogin>().HasKey<string>(l => l.UserId);
            modelBuilder.Entity<IdentityRole>().HasKey<string>(r => r.Id);
            modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });

            modelBuilder.Entity<UserEntity>()
                .HasOptional(x => x.DefaultBudget)
                .WithOptionalDependent()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetCategoryEntity>()
                .HasMany(p => p.Transactions)
                .WithRequired()
                .HasForeignKey(c => c.CategoryId)
                .WillCascadeOnDelete(false);
        }
		
		// Database
		public new DbSet<UserEntity> Users { get; set; }
        public DbSet<BudgetEntity> Budgets { get; set; }
        public DbSet<BudgetGroupEntity> BudgetGroups { get; set; }
        public DbSet<BudgetCategoryEntity> BudgetCategories { get; set; }
        public DbSet<AccountEntity> Accounts { get; set; }
        public DbSet<TransactionEntity> Transactions { get; set; }
        public DbSet<MonthlyCategoryEntity> MonthlyCategories { get; set; }
    }
}
