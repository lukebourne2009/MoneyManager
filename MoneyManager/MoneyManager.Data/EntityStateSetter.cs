﻿using System.Data.Entity;
using MoneyManager.Core.Interfaces;

namespace MoneyManager.Data
{
    public class EntityStateSetter : IEntityStateSetter
    {
        public void SetState<TEntity>(DbContext repo, TEntity entity, EntityState state) 
            where TEntity : class
        {
            repo.Entry(entity).State = state;
        }
    }
}