﻿using System.Data.Entity;
using MoneyManager.Core.Entities;
using MoneyManager.Data.Database;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Data.Repositories.General;

namespace MoneyManager.Data.Repositories
{
    public class BudgetAccess : ParentByObjectGroupAccess<BudgetEntity, int, UserEntity>
    {
        public BudgetAccess(UserEntity user, DbContext repo)
            : base(user,
                  repo,
                  x => x.User_Id == user.Id)
        { }
    }
}
