﻿using System.Data.Entity;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces;
using MoneyManager.Data.Database;
using MoneyManager.Data.Repositories.General;

namespace MoneyManager.Data.Repositories
{
    public class MonthlyCategoryAccess : ParentByIdGroupAccess<MonthlyCategoryEntity, int, int>
    {
        public MonthlyCategoryAccess(int parentId, DbContext repo) 
            : base(parentId, repo, x => x.ParentId == parentId)
        {
        }

        public MonthlyCategoryAccess(int parentId, 
            DbContext repo, 
            IEntityStateSetter stateSetter) 
            : base(parentId, repo, x => x.ParentId == parentId, stateSetter)
        {
        }
    }
}