﻿using System.Data.Entity;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Data.Database;
using MoneyManager.Data.Repositories.General;

namespace MoneyManager.Data.Repositories.Accounts
{
    public class AccountAccess : ParentByIdGroupAccess<AccountEntity,int,int>
    {
        public AccountAccess(int parentId, DbContext repo) 
            : base(parentId, repo, 
                  x => x.ParentId == parentId)
        {}
    }
}