﻿using System.Data.Entity;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Data.Database;
using MoneyManager.Data.Repositories.General;

namespace MoneyManager.Data.Repositories
{
    public class BudgetCategoryAccess : ParentByIdGroupAccess<BudgetCategoryEntity,int,int>
    {
        public BudgetCategoryAccess(int groupId, DbContext repo)
            : base(groupId,
                  repo,
                  x => x.ParentId == groupId)
        { }
    }
}