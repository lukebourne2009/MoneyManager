﻿using System.Data.Entity;
using MoneyManager.Data.Database;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Data.Repositories.General;

namespace MoneyManager.Data.Repositories
{
    public class BudgetGroupAccess : ParentByIdGroupAccess<BudgetGroupEntity, int, int>
    {
        public BudgetGroupAccess(int budgetId, DbContext repo)
            : base(budgetId,
                  repo,
                  x => x.ParentId == budgetId)
        { }
    }
}