﻿using System;
using System.Data.Entity;
using MoneyManager.Core.Interfaces;
using MoneyManager.Core.Interfaces.Entities;
using MoneyManager.Data.Database;

namespace MoneyManager.Data.Repositories.General
{
    public abstract class ParentByObjectGroupAccess<TChild, TChildId, TParent> : ParentGroupAccess<TChild,TChildId>
        where TChild : class, IChild<TParent>, IEntity<TChildId>
    {
        private readonly TParent _parent;

        protected ParentByObjectGroupAccess(TParent parent, DbContext repo, Func<TChild, bool> parentMatcher) 
            : this(parent, repo ,parentMatcher, new EntityStateSetter())
        {}

        protected ParentByObjectGroupAccess(TParent parent,
	        DbContext repo,
            Func<TChild, bool> parentMatcher,
            IEntityStateSetter stateSetter)
            : base(repo, parentMatcher, stateSetter)
        {
            if (parent == null) throw new NullReferenceException();
            _parent = parent;
        }

        protected override void ParentLink(TChild entity)
        {
            entity.Parent = _parent;
        }
    }
}
