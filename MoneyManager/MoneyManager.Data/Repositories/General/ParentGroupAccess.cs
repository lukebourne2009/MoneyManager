﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using MoneyManager.Core.Enums;
using MoneyManager.Core.Interfaces;
using MoneyManager.Core.Interfaces.Entities;
using MoneyManager.Core.Interfaces.Repositories;
using MoreLinq;

namespace MoneyManager.Data.Repositories.General
{
    public abstract class ParentGroupAccess<TChild, TChildId> : IAccess<TChild>
        where TChild : class, IEntity<TChildId>
    {
        private readonly IDbSet<TChild> _dbSet;
        private readonly DbContext _repo;
        private readonly Func<TChild, bool> _parentMatcher;
        private readonly IEntityStateSetter _entityStateSetter;

        protected ParentGroupAccess(DbContext repo,
            Func<TChild, bool> parentMatcher,
            IEntityStateSetter entityStateSetter)
        {
            _dbSet = repo.Set<TChild>();
            _repo = repo;
            _parentMatcher = parentMatcher;
            _entityStateSetter = entityStateSetter;
        }

        #region Get All

	    public IEnumerable<TChild> GetAll()
	    {
		    return GetAllWithStringIncludes();
	    }
	    public IEnumerable<TChild> GetAll(params string[] includes)
        {
            return GetAllWithStringIncludes(includes);
        }
        public IEnumerable<TChild> GetAll(params Expression<Func<TChild, IEnumerable<object>>>[] func)
        {
            IQueryable<TChild> query = _dbSet;
            foreach (var expression in func)
            {
                query = query.Include(expression);
            }

            return query.Where(_parentMatcher);
        }

        public IEnumerable<TChild> GetAll<TSelect1>(Expression<Func<TChild, IEnumerable<TSelect1>>> func)
        {
            return _dbSet.Include(func).Where(_parentMatcher);
        }

        public IEnumerable<TChild> GetAll<TSelect1, TSelect2>(Expression<Func<TChild, IEnumerable<TSelect1>>> func,
                    Expression<Func<TChild, IEnumerable<TSelect2>>> func2)
        {
            return _dbSet.Include(func).Include(func2).Where(_parentMatcher);
        }

        #endregion Get All

        public virtual IEnumerable<TChild> GetAllWhere(Func<TChild, bool> comparitor, params string[] includes)
        {
            if (comparitor == null) return GetAllWithStringIncludes(includes);
            return GetAllWithStringIncludes(includes).Where(comparitor);
        }

	    public virtual IEnumerable<TChild> GetById<TId>(TId id)
	    {
		    return GetAll().Where(x => x.Id.Equals(id)).Take(1);
	    }
		public virtual IEnumerable<TChild> GetById<TId>(TId id, params string[] includes)
        {
            return GetAllWithStringIncludes(includes).Where(x => x.Id.Equals(id)).Take(1);
        }
	    public IEnumerable<TChild> GetById<TId>(TId id, params Expression<Func<TChild, IEnumerable<object>>>[] funcs)
	    {
		    return GetAll(funcs).Where(x => x.Id.Equals(id)).Take(1);
	    }

	    public IEnumerable<TChild> GetHighest<TComparitor>(Func<TChild, TComparitor> compareObject)
	    {
		    return GetHighest(compareObject, new string[0]);
		}

	    public IEnumerable<TChild> GetHighest<TComparitor>(Func<TChild, TComparitor> compareObject, params string[] includes)
        {
            IEnumerable<TChild> results = GetAllWhere(x => compareObject(x) != null, includes);
            return !results.Any() ? new List<TChild>() : results.OrderByDescending(compareObject).Take(1);
        }
	    public IEnumerable<TChild> GetHighest<TComparitor>(Func<TChild, TComparitor> compareObject,
		    params Expression<Func<TChild, IEnumerable<object>>>[] funcs)
	    {
			IEnumerable<TChild> results = GetAll(funcs);
		    return !results.Any() ? new List<TChild>() : results.OrderByDescending(compareObject).Take(1);
		}

        public IEnumerable<TChild> GetN(int amount)
        {
            return GetAll().Take(amount);
        }
        public int Count()
        {
            return GetAll().Count();
        }
        public virtual TChild Insert(TChild entity)
        {
            if (GetById(entity.Id).Any())
                throw new InvalidOperationException(
                    $"The '{entity.GetType()}' you tried to insert is already in the database, consider an update instead if applicable.");

            return ParentLinkAndAdd(entity);
        }

        #region Update  

        public virtual TChild Update(TChild entity)
        {
            var existingEntity = GetById(entity.Id).FirstOrDefault();
            if (existingEntity == null)
                throw new InvalidOperationException(
                    "The object you are trying to update does not yet exist in the database");

            _entityStateSetter.SetState(_repo,existingEntity,EntityState.Detached);
            return Attach(entity);
        }
        public TChild Update<TId>(TId id, Action<TChild> propertyAssign)
        {
            var existingEntity = GetById(id).FirstOrDefault();
            if (existingEntity == null)
                throw new InvalidOperationException(
                    "The object you are trying to update does not yet exist in the database");

            propertyAssign(existingEntity);
            _entityStateSetter.SetState(_repo, existingEntity, EntityState.Detached);
            return Attach(existingEntity);
        }

        #endregion Update

        public virtual UpsertResult Upsert(TChild entity)
        {
            UpsertResult result;
            if (GetById(entity.Id) != null)
            {
                Attach(entity);
                result = UpsertResult.Updated;
            }
            else
            {
                ParentLinkAndAdd(entity);
                result = UpsertResult.Inserted;
            }

            return result;
        }
        public virtual void Delete(int id)
        {
            var entity = GetById(id).FirstOrDefault();
            if (entity == null) throw new ArgumentException();

            _dbSet.Remove(entity);
            _repo.SaveChanges();
        }

        #region Private 

        private TChild Attach(TChild entity)
        {
            _dbSet.Attach(entity);
            _entityStateSetter.SetState(_repo, entity, EntityState.Modified);
            _repo.SaveChanges();
            return entity;
        }
        private TChild ParentLinkAndAdd(TChild entity)
        {
            ParentLink(entity);
            _dbSet.Add(entity);
            _repo.SaveChanges();
            return entity;
        }
        protected abstract void ParentLink(TChild entity);
        private IEnumerable<TChild> GetAllWithStringIncludes(params string[] includes)
        {
            if (includes.Length > 0)
            {
                var includeList = string.Join(".", includes);
                return _dbSet.Include(includeList).Where(_parentMatcher);
            }
            return _dbSet.Where(_parentMatcher);
        }

        #endregion Private
    }
}
