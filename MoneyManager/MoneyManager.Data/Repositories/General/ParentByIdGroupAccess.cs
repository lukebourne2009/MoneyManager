﻿using System;
using System.Data.Entity;
using MoneyManager.Core.Interfaces.Entities;
using MoneyManager.Core.Interfaces;

namespace MoneyManager.Data.Repositories.General
{
    public abstract class ParentByIdGroupAccess<TChild, TChildId, TParentId> : ParentGroupAccess<TChild,TChildId>
        where TChild : class, IChildId<TParentId>, IEntity<TChildId>
    {
        private readonly TParentId _parentId;

        protected ParentByIdGroupAccess(TParentId parentId,
            DbContext repo,
            Func<TChild, bool> parentMatcher) 
            : this(parentId, repo,parentMatcher, new EntityStateSetter())
        {}

        protected ParentByIdGroupAccess(TParentId parentId,
            DbContext repo,
            Func<TChild, bool> parentMatcher,
            IEntityStateSetter stateSetter)
            : base(repo, parentMatcher, stateSetter)
        {
            if (parentId == null) throw new NullReferenceException();
            _parentId = parentId;
        }

        protected override void ParentLink(TChild entity)
        {
            entity.ParentId = _parentId;
        }
    }
}
