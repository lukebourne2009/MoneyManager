﻿using System;
using System.Data.Entity;
using System.Linq;
using MoneyManager.Core.Enums;
using MoneyManager.Core.Interfaces.Entities;
using MoneyManager.Core.Interfaces.Repositories;

namespace MoneyManager.Data.Repositories.General
{
    public abstract class SingleAccess<TChild, TParent> : ISingleAccess<TChild>
        where TChild : class, IChild<TParent>, IEntity<string>
        where TParent : class
    {
        private readonly IDbSet<TChild> _dbSet;
        private readonly TParent _parent;
        private readonly DbContext _repo;
        private readonly Func<TChild, bool> _parentMatcher;

        protected SingleAccess(TParent parent, 
            DbContext repo,
            Func<TChild,bool> parentMatcher)
        {
            _dbSet = repo.Set<TChild>();
            _parent = parent;
            _repo = repo;
            _parentMatcher = parentMatcher;
        }

        public virtual TChild Get(params string[] includes)
        {
            if (includes.Length > 0)
            {
                var includeList = string.Join(".", includes);
                return _dbSet.Include(includeList).FirstOrDefault(_parentMatcher);
            }
            return _dbSet.FirstOrDefault(_parentMatcher);
        }

        public virtual TChild Insert(TChild entity)
        {
            if (Get() == entity)
                throw new InvalidOperationException(
                    $"The '{entity.GetType()}' you tried to insert is already in the database, consider an update instead if applicable.");

            return ParentLinkAndAdd(entity);
        }
        public virtual TChild Update(TChild entity)
        {
            if (Get() != entity)
                throw new InvalidOperationException(
                    "The object you are trying to update does not yet exist in the database");

            return Attach(entity);
        }

        public TChild Update<TId>(TId id, Action<TChild> propertyAssign)
        {
            var entity = Get();
            if (!entity.Id.Equals(id))
                throw new InvalidOperationException(
                    "The object you are trying to update does not yet exist in the database");

	        propertyAssign(entity);

            return Attach(entity);
        }

        public virtual UpsertResult Upsert(TChild entity)
        {
            UpsertResult result;
            if (Get() == entity)
            {
                Attach(entity);
                result = UpsertResult.Updated;
            }
            else
            {
                ParentLinkAndAdd(entity);
                result = UpsertResult.Inserted;
            }

            return result;
        }

        private TChild Attach(TChild entity)
        {
            _dbSet.Attach(entity);
            _repo.SaveChanges();
            return entity;
        }
        private TChild ParentLinkAndAdd(TChild entity)
        {
            entity.Parent = _parent;
            _dbSet.Add(entity);
            _repo.SaveChanges();
            return entity;
        }
    }
}
