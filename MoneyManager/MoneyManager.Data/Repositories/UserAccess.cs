﻿using System.Data.Entity;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Models;
using MoneyManager.Data.Database;
using MoneyManager.Data.Repositories.General;

namespace MoneyManager.Data.Repositories
{
    public class UserAccess : SingleAccess<UserEntity,ApplicationUser>
    {
        public UserAccess(ApplicationUser parent, 
            DbContext repo) 
            : base(parent, repo,
                  x => x.Id == parent.Id)
        {}
    }
}
