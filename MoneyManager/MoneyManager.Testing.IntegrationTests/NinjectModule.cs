﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MoneyManager.Common;
using MoneyManager.Common.Factories;
using MoneyManager.Common.Factories.Budgets;
using MoneyManager.Common.Mappers;
using MoneyManager.Common.Requests;
using MoneyManager.Common.Requests.BudgetCategoryRequests;
using MoneyManager.Common.Requests.BudgetGroupRequests;
using MoneyManager.Common.Services.Budgets;
using MoneyManager.Common.Transformers.Budgets;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Interfaces.Service;
using MoneyManager.Core.Mappers;
using MoneyManager.Core.Models;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Requests;
using MoneyManager.Core.Transformers;
using MoneyManager.Data.Database;
using MoneyManager.Testing.TestHelper.Mocks.Databases;
using MoneyManager.Web.Controllers;
using Ninject;
using NSubstitute;

namespace MoneyManager.Testing.IntegrationTests
{
	public class NinjectModule : Ninject.Modules.NinjectModule
	{
		public static StandardKernel Maker { get; } = new StandardKernel(new NinjectModule());

		public override void Load()
		{
			// Controllers
			Bind<BudgetController>().ToSelf();
			Bind<UserManager<ApplicationUser>>().ToSelf();
			Bind<IUserStore<ApplicationUser>>().To<UserStore<ApplicationUser>>();

			// Db
			Bind<DbContext, MoneyManagerDbContext>().To<TestDbContext>();

			// Requests
			Bind<IRequest<BudgetModel, int?, Task<ApplicationUser>>>().To<BudgetGetRequest>();
			Bind<IRequest<BudgetModel, BudgetModel, Task<ApplicationUser>>>().To<BudgetCreateRequest>();
			Bind<IRequest<IEnumerable<BudgetModel>, int, Task<ApplicationUser>>>().To<BudgetOpenRequest>();
			Bind<IRequest<bool, int, Task<ApplicationUser>>>().To<BudgetDeleteRequest>();
			Bind<IRequest<BudgetModel, int, string, Task<ApplicationUser>>>().To<BudgetUpdateNameRequest>();
			Bind<IRequest<BudgetGroupModel, BudgetGroupModel>>().To<GroupCreateRequest>();
			Bind<IRequest<BudgetCategoryModel, BudgetCategoryModel>>().To<CategoryCreateRequest>(); 

			// Services
			Bind<IService<IEnumerable<BudgetModel>, IEnumerable<BudgetModel>>>().To<BudgetLastUsedSortingService>();
			Bind<IService<int, IEnumerable<TransactionEntity>>>().To<TransactionSummingService>();
			Bind<IService<Func<MonthlyCategoryEntity, bool>, DateTime>>().To<MonthlyCategoryChooserService>();

			// Manager Factories
			Bind<IFactory<IBudgetManager, UserEntity>>().To<BudgetManagerFactory>();
			Bind<IFactory<IBudgetGroupManager, int>>().To<BudgetGroupManagerFactory>();
			Bind<IFactory<IBudgetCategoryManager, int>>().To<BudgetCategoryManagerFactory>();

			// Transformers
			Bind<ITransformer<BudgetModel, BudgetEntity>>().To<BudgetEntityToBudgetModel>();
			Bind<ITransformer<BudgetGroupModel, BudgetGroupEntity>>().To<BudgetGroupEntityToBudgetGroupModel>();
			Bind<ITransformer<BudgetCategoryEntity, BudgetCategoryModel>>().To<BudgetCategoryModelToBudgetCategoryEntity>();
			Bind<ITransformer<BudgetEntity, BudgetModel>>().To<BudgetModelToBudgetEntity>();
			Bind<ITransformer<BudgetGroupEntity, BudgetGroupModel>>().To<BudgetGroupModelToBudgetGroupEntity>();

			// Mappers
			Bind<IMapper<BudgetCategoryModel, BudgetCategoryEntity, DateTime>>().To<BudgetCategoryEntityMapper>();
			Bind<IMapper<BudgetGroupModel, BudgetGroupEntity, DateTime>>().To<BudgetGroupEntityMapper>();

			// Service Factories
			var urlCreator = Substitute.For<IActionUrlCreatorService>();
			urlCreator.Excecute(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<IDictionary<string, object>>())
				.Returns(x => x.ArgAt<IDictionary<string, object>>(2).Values.ElementAt(0).ToString());
			var urlCreatorFactory = Substitute.For<IFactory<IActionUrlCreatorService, RequestContext>>();
			urlCreatorFactory.Make(Arg.Any<RequestContext>())
				.Returns(x => urlCreator);
			Bind<IFactory<IActionUrlCreatorService, RequestContext>>().ToConstant(urlCreatorFactory);

			// Access Factories
			Bind<IFactory<IAccess<BudgetEntity>, UserEntity>>().To<BudgetAccessFactory>();
			Bind<IFactory<IAccess<BudgetGroupEntity>, int>>().To<BudgetGroupAccessFactory>();
			Bind<IFactory<IAccess<BudgetCategoryEntity>, int>>().To<BudgetCategoryAccessFactory>();
			Bind<IFactory<ISingleAccess<UserEntity>, ApplicationUser>>().To<UserAccessFactory>();
		}
	}
}