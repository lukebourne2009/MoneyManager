﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MoneyManager.Common.Factories;
using MoneyManager.Common.Factories.Budgets;
using MoneyManager.Common.Services.Budgets;
using MoneyManager.Common.Transformers;
using MoneyManager.Core.Entities;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Interfaces.Service;
using MoneyManager.Core.Models;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Data.Database;
using MoneyManager.Testing.TestHelper;
using MoneyManager.Testing.TestHelper.Mocks.Databases;
using MoneyManager.Testing.TestHelper.PropertyHelpers;
using MoneyManager.Web.Controllers;
using Ninject;
using NSubstitute;
using NUnit.Framework;

namespace MoneyManager.Testing.IntegrationTests.ControllerTests
{
	[TestFixture]
	public class BudgetControllerTests : DataGenerationHelper
	{
		private BudgetController _budgetController;
		private HttpContextBase _httpContextMock;
		private IPrincipal _userMock;

		private MoneyManagerDbContext Repo
		{
			get { return DbHelper.Repo; }
			set { DbHelper.Repo = value; }
		}

		[SetUp]
		public void Setup()
		{
			// DB
			Repo = new TestDbContext();
			Repo.Database.ExecuteSqlCommand("UPDATE Users SET DefaultBudget_Id = NULL;" +
			                                "DELETE FROM Budgets;");

			NinjectModule.Maker.Release(NinjectModule.Maker.Get<MoneyManagerDbContext>());

			_budgetController = CreateController();
		}

		#region Create

		[Test]
		public async Task Create_GivenAValidBudgetModel_APartialViewIsReturned()
		{
			// Arrange
			var budgetModel = new BudgetModel
			{
				Name = "Budget Model"
			};
			ControllerHelper.AttachModelStateToController(budgetModel, _budgetController);

			// Act
			var view = await _budgetController.Create(budgetModel);

			// Assert
			Assert.IsInstanceOf<JsonResult>(view);
		}

		[Test]
		public async Task Create_GivenAValidBudgetModel_TheBudgetIdIsReturned()
		{
			// Arrange
			var budgetModel = new BudgetModel
			{
				Name = "Budget Model"
			};
			ControllerHelper.AttachModelStateToController(budgetModel, _budgetController);

			// Act
			var result = await _budgetController.Create(budgetModel) as JsonResult;
			var id = ControllerHelper.GetJsonData<string>(result, "returnUrl");

			// Assert
			Repo = ControllerHelper.ReloadDb(Repo);
			var budgetEntity = Repo.Budgets.FirstOrDefault();
			Assert.AreEqual(budgetEntity.Id.ToString(), id);
		}

		[Test]
		public async Task Create_GivenAValidBudgetModel_InsertIsCalledOnDB()
		{
			// Arrange
			var budgetModel = new BudgetModel
			{
				Name = "Budget Model"
			};
			ControllerHelper.AttachModelStateToController(budgetModel, _budgetController);

			// Act
			var result = await _budgetController.Create(budgetModel) as JsonResult;
			var id = Convert.ToInt32(ControllerHelper.GetJsonData<string>(result, "returnUrl"));

			// Assert
			Repo = ControllerHelper.ReloadDb(Repo);
			var budgetEntity = Repo.Budgets.FirstOrDefault(x => x.Id == id);
			Assert.IsNotNull(budgetEntity);
		}

		[Test]
		public async Task Create_GivenAnInvalidBudgetModel_TheUserIsReturnedBackToTheViewWithErrors()
		{
			// Arrange
			var budgetModel = new BudgetModel
			{
				Name = "Lots of Characters Lots of Characters Lots of Characters Lots of Characters Lots of Characters Lots of Characters Lots of Characters Lots of Characters Lots of Characters"
			};
			ControllerHelper.AttachModelStateToController(budgetModel, _budgetController);

			// Act
			var view = await _budgetController.Create(budgetModel) as PartialViewResult;
			var result = view.ViewData.ModelState.Count;

			// Assert
			Assert.IsTrue(result > 0);
		}

		[Test]
		public async Task Create_GivenUserIsNotLoggedIn_RedirectToLoginScreen()
		{
			// Arrange
			var budgetModel = new BudgetModel
			{
				Name = "Budget Model"
			};
			_httpContextMock.User.Returns((IPrincipal) null);
			ControllerHelper.AttachModelStateToController(budgetModel, _budgetController);

			// Act
			var redirect = await _budgetController.Create(budgetModel) as RedirectToRouteResult;

			// Assert
			Assert.AreEqual("Login", redirect.RouteValues["Action"]);
			Assert.AreEqual("Account", redirect.RouteValues["Controller"]);
		}

		#endregion

		#region Index

		[Test]
		public async Task Index_GivenUserIsNotLoggedIn_RedirectToLoginIsReturned()
		{
			// Arrange
			_httpContextMock.User.Returns((IPrincipal) null);

			// Act
			var redirect = await _budgetController.Index() as RedirectToRouteResult;

			// Assert
			Assert.AreEqual("Login", redirect.RouteValues["Action"]);
			Assert.AreEqual("Account", redirect.RouteValues["Controller"]);
		}

		[Test]
		public async Task Index_GivenABudgetIsAvailable_AViewResultIsReturned()
		{
			// Arrange
			CreateBudgets(1, GetUser("Test@test.co.uk").User);

			// Act
			var result = await _budgetController.Index();

			// Assert
			Assert.IsInstanceOf<ViewResult>(result);
		}

		[Test]
		public async Task Index_GivenABudgetIsAvailable_BudgetModelIsReturned()
		{
			// Arrange
			CreateBudgets(1, GetUser("Test@test.co.uk").User);

			// Act
			var result = await _budgetController.Index() as ViewResult;

			// Assert
			var model = result.Model;
			Assert.IsInstanceOf<BudgetModel>(model);
		}

		[Test]
		public async Task Index_GivenABudgetIsAvailable_TheSameBudgetModelIsReturned()
		{
			// Arrange
			var budgets = CreateBudgets(1, GetUser("Test@test.co.uk").User);

			// Act
			var result = await _budgetController.Index() as ViewResult;
			var model = result.Model as BudgetModel;

			// Assert
			Assert.AreEqual(budgets.ElementAt(0).Id, model.Id);
		}

		[Test]
		public async Task Index_GivenBudgetIsNotAvailable_RedirectToOpenBudgetPage()
		{
			// Arrange

			// Act
			var redirect = await _budgetController.Index() as RedirectToRouteResult;

			// Assert
			Assert.AreEqual("Open", redirect.RouteValues["Action"]);
		}

		[Test]
		public async Task Index_GivenMultipleBudgets_TheLastOpenedBudgetIsReturned()
		{
			// Arrange
			var list = new List<BudgetModel>
			{
				new BudgetModel {Name = "Budget 1", LastUsed = DateTime.Now - TimeSpan.FromDays(5)},
				new BudgetModel {Name = "Budget 1", LastUsed = DateTime.Now - TimeSpan.FromDays(4)},
				new BudgetModel {Name = "Budget 1", LastUsed = DateTime.Now - TimeSpan.FromDays(30)}
			};
			var budgetFactory = new BudgetAccessFactory(Repo);
			var budgetAccess = budgetFactory.Make(GetUser("Test@test.co.uk").User);
			var entityList = new List<BudgetEntity>();
			foreach (var budget in list)
			{
				var entity = Transform.ToBudgetEntity(budget);
				entityList.Add(budgetAccess.Insert(entity));
			}
			var budgets = Transform.ToBudgetModels(entityList);

			// Act
			var result = await _budgetController.Index() as ViewResult;
			var model = result.Model as BudgetModel;

			// Assert
			Assert.AreEqual(budgets.ElementAt(1).Id, model.Id);
		}

		[Test]
		public async Task Index_GivenAnIdIsPassedin_ThatBudgetIsOpened()
		{
			// Arrange
			var budgets = CreateBudgets(4, GetUser("Test@test.co.uk").User).ToList();

			// Act
			var result = await _budgetController.Index(budgets[2].Id) as ViewResult;
			var model = result.Model as BudgetModel;

			// Assert
			Assert.AreEqual(budgets[2].Id, model.Id);
		}

		[Test]
		public async Task Index_GivenTheBudgetHasGroups_ThenTheyAreBroughtBackAsWell()
		{
			// Arrange
			var budgets = CreateBudgets(1, GetUser("Test@test.co.uk").User).ToList();
			CreateBudgetGroups(3, budgets[0], Repo);

			// Act
			var result = await CreateController().Index(budgets[0].Id) as ViewResult;
			var model = result.Model as BudgetModel;

			// Assert
			Assert.AreEqual(3, model.BudgetGroups.Count());
		}

		#endregion

		#region Open

		[Test]
		public async Task Open_ReturnsAViewResult()
		{
			// Arrange

			// Act
			var result = await _budgetController.Open();

			// Assert
			Assert.IsInstanceOf<ViewResult>(result);
		}

		[Test]
		public async Task Open_ReturnedViewContainsAListOfOwnedBudgets()
		{
			// Arrange

			// Act
			var result = await _budgetController.Open() as ViewResult;

			// Assert
			Assert.IsInstanceOf<IEnumerable<BudgetModel>>(result?.Model);
		}

		[Test]
		public async Task Open_GivenZeroBudgets_ReturnAZeroLengthEnumerable()
		{
			// Arrange
			var result = await _budgetController.Open() as ViewResult;

			// Act
			var model = result?.Model as IEnumerable<BudgetModel>;

			// Assert
			Assert.AreEqual(0, model?.Count());
		}

		[Test]
		public async Task Open_GivenOneBudgets_ReturnAOneLengthEnumerable()
		{
			// Arrange
			CreateBudgets(1, GetUser("Test@test.co.uk").User);
			var result = await _budgetController.Open() as ViewResult;

			// Act
			var model = result?.Model as IEnumerable<BudgetModel>;

			// Assert
			Assert.AreEqual(1, model?.Count());
		}

		[Test]
		public async Task Open_GivenThreeBudgets_ReturnsTheSameBudgets()
		{
			// Arrange
			var data = new List<BudgetModel>
			{
				new BudgetModel {Name = "Nice Budget"},
				new BudgetModel {Name = "Mean Budget"},
				new BudgetModel {Name = "Weird Budget"}
			};
			data = CreateBudgets(data, GetUser("Test@test.co.uk").User).ToList();
			var result = await _budgetController.Open() as ViewResult;

			// Act
			var model = result?.Model as IEnumerable<BudgetModel>;

			// Assert
			Assert.IsTrue(model?.Any(x => x.Id == data[0].Id));
			Assert.IsTrue(model?.Any(x => x.Id == data[1].Id));
			Assert.IsTrue(model?.Any(x => x.Id == data[2].Id));
		}

		[Test]
		public async Task Open_GivenThreeBudgets_BudgetsAreOrderedByDateLastOpened()
		{
			// Arrange
			var list = new List<BudgetModel>
			{
				new BudgetModel {Name = "Budget 1", LastUsed = DateTime.Now - TimeSpan.FromDays(5)},
				new BudgetModel {Name = "Budget 1", LastUsed = DateTime.Now - TimeSpan.FromDays(4)},
				new BudgetModel {Name = "Budget 1", LastUsed = DateTime.Now - TimeSpan.FromDays(30)}
			};
			var budgetFactory = new BudgetAccessFactory(Repo);
			var budgetAccess = budgetFactory.Make(GetUser("Test@test.co.uk").User);
			var entityList = new List<BudgetEntity>();
			foreach (var budget in list)
			{
				var entity = Transform.ToBudgetEntity(budget);
				entityList.Add(budgetAccess.Insert(entity));
			}
			list = Transform.ToBudgetModels(entityList).ToList();


			// Act
			var result = await _budgetController.Open() as ViewResult;
			var model = result?.Model as IEnumerable<BudgetModel>;

			// Assert
			Assert.AreEqual(list[0].Id, model?.ElementAt(1).Id);
			Assert.AreEqual(list[1].Id, model?.ElementAt(0).Id);
			Assert.AreEqual(list[2].Id, model?.ElementAt(2).Id);
		}

		[Test]
		public async Task Open_Given11Budgets_OnlyTheFirst10AreReturned()
		{
			// Arrange
			var data = new List<BudgetModel>
			{
				new BudgetModel {Name = "Budget 1", LastUsed = DateTime.Now - TimeSpan.FromDays(5)},
				new BudgetModel {Name = "Budget 2", LastUsed = DateTime.Now - TimeSpan.FromDays(4)},
				new BudgetModel {Name = "Budget 3", LastUsed = DateTime.Now - TimeSpan.FromDays(30)},
				new BudgetModel {Name = "Budget 4", LastUsed = DateTime.Now - TimeSpan.FromDays(5)},
				new BudgetModel {Name = "Budget 5", LastUsed = DateTime.Now - TimeSpan.FromDays(4)},
				new BudgetModel {Name = "Budget 6", LastUsed = DateTime.Now - TimeSpan.FromDays(30)},
				new BudgetModel {Name = "Budget 7", LastUsed = DateTime.Now - TimeSpan.FromDays(5)},
				new BudgetModel {Name = "Budget 8", LastUsed = DateTime.Now - TimeSpan.FromDays(4)},
				new BudgetModel {Name = "Budget 9", LastUsed = DateTime.Now - TimeSpan.FromDays(30)},
				new BudgetModel {Name = "Budget 10", LastUsed = DateTime.Now - TimeSpan.FromDays(5)},
				new BudgetModel {Name = "Budget 11", LastUsed = DateTime.Now - TimeSpan.FromDays(4)}
			};
			data = CreateBudgets(data, GetUser("Test@test.co.uk").User).ToList();
			var result = await _budgetController.Open() as ViewResult;

			// Act
			var model = result?.Model as IEnumerable<BudgetModel>;

			// Assert
			Assert.AreEqual(10, model.Count());
		}

		[Test]
		public async Task Open_Given11Budgets_ViewBagIsPopulatedWithCountOf11()
		{
			// Arrange
			var data = new List<BudgetModel>
			{
				new BudgetModel {Name = "Budget 1", LastUsed = DateTime.Now - TimeSpan.FromDays(5)},
				new BudgetModel {Name = "Budget 2", LastUsed = DateTime.Now - TimeSpan.FromDays(4)},
				new BudgetModel {Name = "Budget 3", LastUsed = DateTime.Now - TimeSpan.FromDays(30)},
				new BudgetModel {Name = "Budget 4", LastUsed = DateTime.Now - TimeSpan.FromDays(5)},
				new BudgetModel {Name = "Budget 5", LastUsed = DateTime.Now - TimeSpan.FromDays(4)},
				new BudgetModel {Name = "Budget 6", LastUsed = DateTime.Now - TimeSpan.FromDays(30)},
				new BudgetModel {Name = "Budget 7", LastUsed = DateTime.Now - TimeSpan.FromDays(5)},
				new BudgetModel {Name = "Budget 8", LastUsed = DateTime.Now - TimeSpan.FromDays(4)},
				new BudgetModel {Name = "Budget 9", LastUsed = DateTime.Now - TimeSpan.FromDays(30)},
				new BudgetModel {Name = "Budget 10", LastUsed = DateTime.Now - TimeSpan.FromDays(5)},
				new BudgetModel {Name = "Budget 11", LastUsed = DateTime.Now - TimeSpan.FromDays(4)}
			};
			CreateBudgets(data, GetUser("Test@test.co.uk").User);
			var result = await _budgetController.Open() as ViewResult;

			// Act
			var count = result?.ViewBag.BudgetCount;

			// Assert
			Assert.AreEqual(11, count);
		}

		#endregion

		#region Delete

		[Test]
		public void Delete_GivenInvalidBudgetId_ThrowInvalidOperationException()
		{
			// Arrange

			// Act
			AsyncTestDelegate del = async () => await _budgetController.Delete(15);

			// Assert
			Assert.ThrowsAsync<ArgumentException>(del);
		}

		[Test]
		public async Task Delete_GivenValidBudgetId_BudgetIsDeleted()
		{
			// Arrange
			var budget = CreateBudgets(1, GetUser("Test@test.co.uk").User).ElementAt(0);

			// Act
			await _budgetController.Delete(budget.Id);

			// Assert
			var entity = Repo.Budgets.Find(budget.Id);
			Assert.IsNull(entity);
		}

		#endregion

		#region UpdateName

		[Test]
		public async Task UpdateName_GivenTheUserHasABudgetItsNameCanBeEdited()
		{
			// Arrange
			var budget = CreateBudgets(1, GetUser("Test@test.co.uk").User).ElementAt(0);

			// Act
			var result = await _budgetController.UpdateName(budget.Id, "New Name");

			// Assert
			Repo = new TestDbContext();
			var name = Repo.Budgets.Find(budget.Id).Name;
			Assert.AreEqual("New Name", name);
		}

		[Test]
		public void UpdateName_GivenTheUserDoesNotHaveAMatchingBudget_ThrowException()
		{
			// Arrange

			// Act
			AsyncTestDelegate del = async () => await _budgetController.UpdateName(675, "New Name");

			// Assert
			Assert.ThrowsAsync<InvalidOperationException>(del);
		}

		[TestCase(null)]
		[TestCase("")]
		public void UpdateName_GivenTheUserSendsANullOrEmptyString_ThrowException(string name)
		{
			// Arrange
			var budget = CreateBudgets(1, GetUser("Test@test.co.uk").User).ElementAt(0);

			// Act
			AsyncTestDelegate del = async () => await _budgetController.UpdateName(budget.Id, name);

			// Assert
			Assert.ThrowsAsync<ModelValidationException>(del);
		}

		[Test]
		public async Task UpdateName_GivenTheUserUpdatesABudgetsName_ReturnNameInJson()
		{
			// Arrange
			var budget = CreateBudgets(1, GetUser("Test@test.co.uk").User).ElementAt(0);

			// Act
			var result = await _budgetController.UpdateName(budget.Id, "New Name") as JsonResult;

			// Assert
			var data = ControllerHelper.GetJsonData<string>(result, "Name");
			Assert.AreEqual("New Name", data);
		}

		#endregion

		private BudgetController CreateController()
		{
			Repo = NinjectModule.Maker.Get<MoneyManagerDbContext>();

			// Users
			var userMan = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(Repo));
			var appUser = userMan.FindByEmail("Test@test.co.uk");

			// Budget Manager
			var budgetfactory = NinjectModule.Maker.Get<IFactory<IBudgetManager, UserEntity>>();

			// Budget Sorting
			var sorter = new BudgetLastUsedSortingService();

			// Url Creator
			var urlCreator = Substitute.For<IActionUrlCreatorService>();
			urlCreator.Excecute(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<IDictionary<string, object>>())
				.Returns(x => x.ArgAt<IDictionary<string, object>>(2).Values.ElementAt(0).ToString());
			var urlCreatorFactory = Substitute.For<IFactory<IActionUrlCreatorService, RequestContext>>();
			urlCreatorFactory.Make(Arg.Any<RequestContext>())
				.Returns(x => urlCreator);

			// Controller
			var controller = NinjectModule.Maker.Get<BudgetController>(); //new BudgetController(budgetfactory, sorter, urlCreatorFactory, userMan);

			// Controller Mock
			_userMock = Substitute.For<IPrincipal>();
			_httpContextMock = Substitute.For<HttpContextBase>();
			var identityMock = Substitute.For<IIdentity>();
			var controllerContextMock = Substitute.For<ControllerContext>();

			_userMock.Identity.Returns(identityMock);
			_httpContextMock.User.Returns(_userMock);
			identityMock.Name.Returns(appUser.UserName);
			controllerContextMock.HttpContext.Returns(_httpContextMock);
			controller.ControllerContext = controllerContextMock;

			return controller;
		}
	}
}
