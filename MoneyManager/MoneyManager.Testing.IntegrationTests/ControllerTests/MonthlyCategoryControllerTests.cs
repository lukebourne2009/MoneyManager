﻿using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MoneyManager.Common.Factories;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Models;
using MoneyManager.Core.ViewModels;
using MoneyManager.Data.Database;
using MoneyManager.Testing.TestHelper;
using MoneyManager.Testing.TestHelper.Mocks.Databases;
using MoneyManager.Web.Controllers;
using NSubstitute;
using NUnit.Framework;

namespace MoneyManager.Testing.IntegrationTests.ControllerTests
{
    public class MonthlyCategoryControllerTests : ControllerHelper
    {
        private MoneyManagerDbContext Repo { get; set; }
        private MonthlyCategoryController MonthlyCategoryController { get; set; }
        private ApplicationUser AppUser { get; set; }
        private BudgetCategoryEntity Category { get; set; }

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            // DB
            Repo = new TestDbContext();
            Repo.Database.ExecuteSqlCommand("UPDATE Users SET DefaultBudget_Id = NULL;" +
                                            "DELETE FROM Transactions;" +
                                            "DELETE FROM Accounts;" +
                                            "DELETE FROM BudgetCategories;" +
                                            "DELETE FROM BudgetGroups;" +
                                            "DELETE FROM Budgets;");

            // Users
            var userMan = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(Repo));
            AppUser = userMan.FindByEmail("Test@test.co.uk");
            var budget = Repo.Budgets.Add(new BudgetEntity {Name = "dfhdf", Parent = AppUser.User});
            Repo.SaveChanges();
            var group = Repo.BudgetGroups.Add(new BudgetGroupEntity {Name = "dfjdfj", ParentId = budget.Id});
            Repo.SaveChanges();
            Category = Repo.BudgetCategories.Add(new BudgetCategoryEntity {CategoryOrder = 0, Name = "djdj", ParentId = group.Id});
            Repo.SaveChanges();
        }

        [SetUp]
        public void Setup()
        {
            // DB
            Repo = new TestDbContext();
            Repo.Database.ExecuteSqlCommand("DELETE FROM MonthlyCategories;");

            // Users
            var userMan = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(Repo));
            AppUser = userMan.FindByEmail("Test@test.co.uk");

            // Monthly Manager Factory
            var factory = new MonthlyManagerFactory(Repo);

            // Controller
            MonthlyCategoryController = new MonthlyCategoryController(factory);

            // Controller Mock
            var userMock = Substitute.For<IPrincipal>();
            var httpContextMock = Substitute.For<HttpContextBase>();
            var identityMock = Substitute.For<IIdentity>();
            var controllerContextMock = Substitute.For<ControllerContext>();

            userMock.Identity.Returns(identityMock);
            httpContextMock.User.Returns(userMock);
            identityMock.Name.Returns(AppUser.UserName);
            controllerContextMock.HttpContext.Returns(httpContextMock);
            MonthlyCategoryController.ControllerContext = controllerContextMock;
        }

        [Test]
        public void Create_DoesNotReturnNull()
        {
            // Arrange
            var viewModel = new MonthlyCategoryViewModel
            {
                Id = 0,
                BudgetCategoryId = Category.Id,
                BudgetedValue = 400,
                Date = "December 2000"
            };

            // Act
            var result = MonthlyCategoryController.Create(viewModel);

            // Assert
            Assert.IsNotNull(result);
        }

        [Test]
        public void Create_GivenValidModel_InsertsEntityIntoDB()
        {
            // Arrange
            var viewModel = new MonthlyCategoryViewModel
            {
                Id = 0,
                BudgetCategoryId = Category.Id,
                BudgetedValue = 400,
                Date = "December 2012"
            };
            AttachModelStateToController(viewModel, MonthlyCategoryController);

            // Act
            var result = MonthlyCategoryController.Create(viewModel) as JsonResult;

            // Assert
            var data = Repo.MonthlyCategories.FirstOrDefault(x => x.Id == ((MonthlyCategoryViewModel) result.Data).Id);
            Assert.IsNotNull(data);
            Assert.AreEqual(400, data.BudgetedValue);
            Assert.AreEqual(Category.Id, data.ParentId);
            Assert.AreEqual("12-2012",data.Date);
        }
    }
}