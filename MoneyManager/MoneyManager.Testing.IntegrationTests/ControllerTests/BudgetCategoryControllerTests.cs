﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MoneyManager.Core.Entities.Accounts;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Interfaces.Repositories;
using MoneyManager.Core.Mappers;
using MoneyManager.Core.Models;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Requests;
using MoneyManager.Data.Database;
using MoneyManager.Data.Repositories;
using MoneyManager.Web.Controllers;
using MoneyManager.Testing.TestHelper;
using MoneyManager.Testing.TestHelper.Mocks.Databases;
using MoneyManager.Testing.TestHelper.PropertyHelpers;
using Ninject;
using NSubstitute;
using NUnit.Framework;

namespace MoneyManager.Testing.IntegrationTests.ControllerTests
{
	public class BudgetCategoryControllerTests
	{
		private MoneyManagerDbContext Repo { get; set; }
		private BudgetCategoryController CatController { get; set; }
		private ApplicationUser AppUser { get; set; }
		private BudgetGroupModel BudgetGroup { get; set; }
		public DataGenerationHelper Helper { get; set; }

		[OneTimeSetUp]
		public void OneTimeSetup()
		{
			// DB
			Repo = new TestDbContext();
			Repo.Database.ExecuteSqlCommand("UPDATE Users SET DefaultBudget_Id = NULL;" +
			                                "DELETE FROM Transactions;" +
			                                "DELETE FROM Accounts;" +
			                                "DELETE FROM MonthlyCategories;" +
			                                "DELETE FROM BudgetCategories;" +
			                                "DELETE FROM BudgetGroups;" +
			                                "DELETE FROM Budgets;");

			// Users
			var userMan = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(DbHelper.Repo));
			AppUser = userMan.FindByEmail("Test@test.co.uk");
			var budget = new DataGenerationHelper().CreateBudgets(1, AppUser.User).ElementAt(0);
			BudgetGroup = new DataGenerationHelper().CreateBudgetGroups(1, budget, Repo).ElementAt(0);
		}

		[SetUp]
		public void Setup()
		{
			// DB
			Repo = new TestDbContext();
			Repo.Database.ExecuteSqlCommand("DELETE FROM BudgetCategories;");

			// Users
			var userMan = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(Repo));
			AppUser = userMan.FindByEmail("Test@test.co.uk");

			// Group Manager Factory
			//        IFactory<IAccess<BudgetCategoryEntity>, int> bgaFact = new BudgetCategoryAccessFactory(Repo);
			//        IFactory<BudgetCategoryManager,int> bgmFact = new BudgetCategoryManagerFactory(bgaFact, 
			//            new BudgetCategoryEntityMapper(new TransactionSummingService(), new MonthlyCategoryChooserService()),
			//new BudgetCategoryModelToBudgetCategoryEntity());

			// Controller
			CatController = new BudgetCategoryController(NinjectModule.Maker
				.Get<IRequest<BudgetCategoryModel, BudgetCategoryModel>>());

			// Controller Mock
			var userMock = Substitute.For<IPrincipal>();
			var httpContextMock = Substitute.For<HttpContextBase>();
			var identityMock = Substitute.For<IIdentity>();
			var controllerContextMock = Substitute.For<ControllerContext>();

			userMock.Identity.Returns(identityMock);
			httpContextMock.User.Returns(userMock);
			identityMock.Name.Returns(AppUser.UserName);
			controllerContextMock.HttpContext.Returns(httpContextMock);
			CatController.ControllerContext = controllerContextMock;
		}

		#region Create

		[Test]
		public async Task Create_GivenInvalidModel_ReturnHttpStatus422()
		{
			// Arrange
			var model = new BudgetCategoryModel();
			ControllerHelper.AttachModelStateToController(model, CatController);

			// Act
			var result = await CatController.Create(model);

			// Assert
			Assert.IsInstanceOf<HttpStatusCodeResult>(result);
			Assert.AreEqual(422, ((HttpStatusCodeResult) result).StatusCode);
		}

		[Test]
		public async Task Create_GivenValidModel_ReturnModelWithId()
		{
			// Arrange
			var model = new BudgetCategoryModel
			{
				BudgetGroupId = BudgetGroup.Id,
				Name = "Name"
			};
			ControllerHelper.AttachModelStateToController(model, CatController);

			// Act
			var result = await CatController.Create(model) as JsonResult;

			// Assert
			var data = (BudgetCategoryModel) result.Data;
			Assert.AreNotEqual(0, data.Id);
		}

		[Test]
		public async Task Create_GivenValidModel_ModelIsStoredCorrectly()
		{
			// Arrange
			var model = new BudgetCategoryModel
			{
				BudgetGroupId = BudgetGroup.Id,
				Name = "Name",
				CategoryOrder = 3
			};
			ControllerHelper.AttachModelStateToController(model, CatController);

			// Act
			var result = await CatController.Create(model) as JsonResult;

			// Assert
			var data = (BudgetCategoryModel) result.Data;
			var access = new BudgetCategoryAccess(BudgetGroup.Id, new TestDbContext());
			var actual = access.GetById(data.Id).FirstOrDefault();

			Assert.AreEqual(data.Name, actual.Name);
			Assert.AreEqual(data.CategoryOrder, actual.CategoryOrder);
		}

		#endregion


	}
}