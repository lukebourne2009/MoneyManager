﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MoneyManager.Core.Entities.Budgets;
using MoneyManager.Core.Interfaces.Factories;
using MoneyManager.Core.Interfaces.Managers;
using MoneyManager.Core.Models;
using MoneyManager.Core.Models.Budgets;
using MoneyManager.Core.Requests;
using MoneyManager.Data.Database;
using MoneyManager.Testing.TestHelper;
using MoneyManager.Testing.TestHelper.Mocks.Databases;
using MoneyManager.Testing.TestHelper.PropertyHelpers;
using MoneyManager.Web.Controllers;
using Ninject;
using NSubstitute;
using NUnit.Framework;

namespace MoneyManager.Testing.IntegrationTests.ControllerTests
{
	[TestFixture]
	public class BudgetGroupControllerTests
	{
		private MoneyManagerDbContext Repo { get; set; }
		private BudgetGroupController GroupController { get; set; }
		private ApplicationUser AppUser { get; set; }
		private BudgetModel Budget { get; set; }

		[OneTimeSetUp]
		public void OneTimeSetup()
		{
			// DB
			Repo = new TestDbContext();
			Repo.Database.ExecuteSqlCommand("UPDATE Users SET DefaultBudget_Id = NULL;" +
			                                "DELETE FROM Transactions;" +
			                                "DELETE FROM Accounts;" +
			                                "DELETE FROM BudgetCategories;" +
			                                "DELETE FROM BudgetGroups;" +
			                                "DELETE FROM Budgets;");

			// Users
			var userMan = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(DbHelper.Repo));
			AppUser = userMan.FindByEmail("Test@test.co.uk");
			Budget = new DataGenerationHelper().CreateBudgets(1, AppUser.User).ElementAt(0);
		}

		[SetUp]
		public void Setup()
		{
			// DB
			Repo = new TestDbContext();
			Repo.Database.ExecuteSqlCommand("DELETE FROM Transactions;" +
			                                "DELETE FROM Accounts;" +
			                                "DELETE FROM MonthlyCategories;" +
			                                "DELETE FROM BudgetCategories;" +
			                                "DELETE FROM BudgetGroups");

			// Users
			var userMan = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(Repo));
			AppUser = userMan.FindByEmail("Test@test.co.uk");

			// Controller
			GroupController = new BudgetGroupController(NinjectModule.Maker.Get<IRequest<BudgetGroupModel, BudgetGroupModel>>());

			// Controller Mock
			var userMock = Substitute.For<IPrincipal>();
			var httpContextMock = Substitute.For<HttpContextBase>();
			var identityMock = Substitute.For<IIdentity>();
			var controllerContextMock = Substitute.For<ControllerContext>();

			userMock.Identity.Returns(identityMock);
			httpContextMock.User.Returns(userMock);
			identityMock.Name.Returns(AppUser.UserName);
			controllerContextMock.HttpContext.Returns(httpContextMock);
			GroupController.ControllerContext = controllerContextMock;
		}

		#region Create

		[Test]
		public void Create_GivenAValidModel_ReturnNotNull()
		{
			// Arrange
			var groupModel = new BudgetGroupModel
			{
				BudgetId = Budget.Id,
				Name = "Name"
			};
			ControllerHelper.AttachModelStateToController(groupModel, GroupController);

			// Act
			var result = GroupController.Create(groupModel);

			// Assert
			Assert.IsNotNull(result);
		}

		[Test]
		public void Create_GivenAValidModel_ReturnJsonResult()
		{
			// Arrange
			var groupModel = new BudgetGroupModel
			{
				BudgetId = Budget.Id,
				Name = "Name"
			};
			ControllerHelper.AttachModelStateToController(groupModel, GroupController);

			// Act
			var result = GroupController.Create(groupModel);

			// Assert
			Assert.IsInstanceOf<JsonResult>(result);
		}

		[Test]
		public void Create_GivenAnInValidModel_ReturnHttpStatusCodeResult()
		{
			// Arrange
			var groupModel = new BudgetGroupModel();
			ControllerHelper.AttachModelStateToController(groupModel, GroupController);

			// Act
			var result = GroupController.Create(groupModel);

			// Assert
			Assert.IsInstanceOf<HttpStatusCodeResult>(result);
		}

		[Test]
		public async Task Create_GivenAValidModel_ModelIsStoredInTheDB()
		{
			// Arrange
			var groupModel = new BudgetGroupModel
			{
				Name = "name",
				BudgetId = Budget.Id
			};
			ControllerHelper.AttachModelStateToController(groupModel, GroupController);

			// Act
			var result = await GroupController.Create(groupModel);

			// Assert
			var id = ControllerHelper.GetJsonData<int>(result as JsonResult, "Id");
			var entity = ControllerHelper.ReloadDb(Repo).BudgetGroups.First(x => x.Id == id);
			Assert.IsNotNull(entity);
		}

		#endregion
	}
}